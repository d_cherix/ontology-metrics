package ontologymetrics.clustering;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import ontologymetrics.core.Config;
import ontologymetrics.core.data.ILoader;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.RawData;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.genetics.AbstractFitnessFunction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.jgap.Chromosome;
import org.jgap.Gene;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.BooleanGene;
import org.jgap.impl.DefaultConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class AUCClustering extends AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(AUCClustering.class);

    @Autowired
    private ApplicationContext appContext;

    @Autowired
    private ClusteringResultDao dao;
    private RawData instances;

    @Autowired
    private AbstractFitnessFunction fitness;
    @Autowired
    private ILoader loader;
    private DefaultConfiguration conf;

    private String description = "";

    private boolean[] weights;

    private String arffFilePath;

    private String outPath;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        Config config = (Config) this.data.getData(Type.CONFIG);
        if (config.getConfigValue(this.name + ".arffFilePath") == null
                || config.getConfigValue(this.name + ".outPath") == null) {
            throw new StageInitException();
        }
        arffFilePath = config.getConfigValue(this.name + ".arffFilePath");
        outPath = config.getConfigValue(this.name + ".outPath");
        return true;
    }

    @Override
    protected void process() {
        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(this.outPath, false));
            writer.append("dist\tuntrusted\n");
            writer.flush();
        } catch (IOException e) {
            logger.error("Error on wrtiting roc curve file", e);
            return;
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                logger.error("Error on closing writer", e);
            }
        }
        instances = loader.load(arffFilePath);
        logger.info("{} instances loaded", instances.getInstances().size());

        for (int alpha = 0; alpha < 100; alpha++) {
            logger.info("begin {} step", alpha + 1);
            fitness.setInstances(instances);
            fitness.setMaxValues(loader.getMaxValues());
            int trustedNb = (instances.getInstances().size() - instances.getUntrusteds()) / 10;
            int untrustedNb = instances.getUntrusteds() / 10;

            List<Instance> tmp = new ArrayList<Instance>(trustedNb + untrustedNb);
            List<Integer> positions = new ArrayList<Integer>(trustedNb + untrustedNb);
            Set<Integer> posPos = new HashSet<Integer>();
            Set<Integer> posNeg = new HashSet<Integer>();
            Random r = new Random();
            while (posPos.size() < trustedNb || posNeg.size() < untrustedNb) {
                int pos = r.nextInt(instances.getInstances().size());
                if (!instances.getInstances().get(pos).isUntrusted() && posPos.size() < trustedNb) {
                    posPos.add(pos);
                } else if (instances.getInstances().get(pos).isUntrusted() && posNeg.size() < untrustedNb) {
                    posNeg.add(pos);
                }
            }
            positions.addAll(posNeg);
            positions.addAll(posPos);
            Collections.sort(positions);
            for (int i = 0; i < positions.size(); i++) {
                tmp.add(instances.getInstances().remove(positions.get(i) - i));
            }
            IClusterer clusterer = null;
            logger.debug("Clustering Nb of Instances{}", instances.getInstances().size());
            try {
                clusterer = this.cluster(instances);
            } catch (InvalidConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            this.validate(tmp, clusterer, alpha);
            for (Instance i : tmp) {
                instances.getInstances().add(i);
            }
            logger.info("{} step achieved", alpha + 1);
        }

    }

    private IClusterer cluster(RawData instances) throws InvalidConfigurationException {
        IClusterer clusterer = (IClusterer) appContext.getBean("clusterer");
        conf.reset();
        conf = new DefaultConfiguration();
        Gene[] sampleGenes = new Gene[instances.getInstances().get(0).getPoint().length];
        for (int i = 0; i < sampleGenes.length; i++) {
            sampleGenes[i] = new BooleanGene(conf);
            sampleGenes[i].setAllele(weights[i]);
        }

        Chromosome chromosome = new Chromosome(conf, sampleGenes);
        try {
            clusterer.cluster(chromosome, instances, instances.getMaxValues());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        chromosome.setApplicationData(clusterer);
        return clusterer;
    }

    private void validate(List<Instance> tmp, IClusterer clusterer, int index) {
        double[] dist = new double[tmp.size()];
        boolean[] untrusted = new boolean[tmp.size()];
        for (int i = 0; i < tmp.size(); i++) {
            dist[i] = clusterer.getDistanceMeasure().compute(
                    ((CentroidCluster<Instance>) clusterer.getUntrustedCluster()).getCenter().getPoint(),
                    tmp.get(i).getPoint());
            untrusted[i] = tmp.get(i).isUntrusted();
        }
        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(this.outPath, true));
            for (int i = 0; i < tmp.size(); i++) {
                writer.append(dist[i] + "\t" + untrusted[i] + "\n");
            }
            writer.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    private double getFMeasure(double precision, double recall, double beta) {
        return (1 + Math.pow(beta, 2)) * ((precision * recall) / ((Math.pow(beta, 2) * precision) + recall));
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean[] getWeights() {
        return weights;
    }

    public void setWeights(boolean[] weights) {
        this.weights = weights;
    }
}
