package ontologymetrics.clustering;
/**
 * Exception throwed while clustering
 * @author d.cherix
 *
 */
public class ClusteringException extends Exception {

    public ClusteringException() {
        super();
    }

    public ClusteringException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ClusteringException(String arg0) {
        super(arg0);
    }

    public ClusteringException(Throwable arg0) {
        super(arg0);
    }

}
