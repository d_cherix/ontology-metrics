package ontologymetrics.clustering;

import java.util.List;
/**
 * 
 * @author d.cherix
 *
 */
public class ClusteringResult {
    private int chromosome;
    private String chromosomeString;
    private List<String> instances;
    private String description;
    private double precision;
    private double recall;
    private double fitness;

    /**
     * 
     * @param chromosome
     * @param chromosomeString
     * @param instances
     * @param description
     */
    public ClusteringResult(int chromosome, String chromosomeString, List<String> instances, String description) {
        super();
        this.chromosome = chromosome;
        this.chromosomeString = chromosomeString;
        this.instances = instances;
        this.description=description;
    }
    
    /**
     * 
     * @param chromosome
     * @param chromosomeString
     * @param description
     */
    public ClusteringResult(int chromosome, String chromosomeString, String description) {
        super();
        this.chromosome = chromosome;
        this.chromosomeString = chromosomeString;
        this.description = description;
    }

    public int getChromosome() {
        return chromosome;
    }
    public void setChromosome(int chromosome) {
        this.chromosome = chromosome;
    }
    public String getChromosomeString() {
        return chromosomeString;
    }
    public void setChromosomeString(String chromosomeString) {
        this.chromosomeString = chromosomeString;
    }
    public List<String> getInstances() {
        return instances;
    }
    public void setInstances(List<String> instances) {
        this.instances = instances;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrecision() {
        return precision;
    }

    public void setPrecision(double precision) {
        this.precision = precision;
    }

    public double getRecall() {
        return recall;
    }

    public void setRecall(double recall) {
        this.recall = recall;
    }
    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }
    
}
