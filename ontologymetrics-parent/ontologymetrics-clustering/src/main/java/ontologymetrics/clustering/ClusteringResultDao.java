package ontologymetrics.clustering;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.jgap.Gene;
import org.jgap.IChromosome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * DAO for the clustering result.
 * 
 * @author d.cherix
 * 
 */
public class ClusteringResultDao {

    private static final Logger logger = LoggerFactory.getLogger(ClusteringResultDao.class);

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private final Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
    private String path;

    @Autowired
    public void init(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        jdbcTemplate
                .execute("CREATE TABLE IF NOT EXISTS `cluster_results` (" +
                        "`id` int(11) NOT NULL AUTO_INCREMENT," +
                        "`chromosomeHash` INTEGER," +
                        "`precision` DOUBLE," +
                        "`recall` DOUBLE," +
                        "`fitness` DOUBLE," +
                        "`chromosomeString` VARCHAR(500)," +
                        "`timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
                        "`testDescription` VARCHAR(500)," +
                        "PRIMARY KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
        // jdbcTemplate
        // .execute("CREATE TABLE IF NOT EXISTS `cluster_coresp` (" +
        // "`id` int(11) NOT NULL AUTO_INCREMENT," +
        // "`chromosomeHash` INTEGER," +
        // "`uri` VARCHAR(500)," +
        // "PRIMARY KEY (`id`)" +
        // ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
    }

    /**
     * Save a clustering result
     * This mehtod is asynchron.
     * 
     * @param clusteringResult
     *            the clustering result to save.
     */
    public void save(final ClusteringResult clusteringResult) {
        SaveTask task = new SaveTask(jdbcTemplate, clusteringResult, this.path);
        Thread t = new Thread(task);
        t.start();
    }

    /**
     * Synchron mehtod to save a {@link ClusteringResult} object.
     * 
     * @param clusteringResult
     */
    public void saveDirect(final ClusteringResult clusteringResult) {
        jdbcTemplate
                .update("INSERT INTO cluster_results (`chromosomeHash`, `chromosomeString`, `testDescription`, `precision`, `recall`,`fitness`) VALUES (?,?,?,?,?,?)",
                        new Object[] {
                                clusteringResult.getChromosome(),
                                clusteringResult.getChromosomeString(),
                                clusteringResult.getDescription(),
                                clusteringResult.getPrecision(), clusteringResult.getRecall(),
                                clusteringResult.getFitness()
                        });
    }

    public void updateResult(final int hashcode, final double fitness, final String chromosomeString) {
        jdbcTemplate.update("UPDATE cluster_results set fitness=?, chromosomeString=? WHERE chromosomeHash=?;",
                new PreparedStatementSetter() {

                    @Override
                    public void setValues(PreparedStatement ps) throws SQLException {
                        ps.setDouble(1, fitness);
                        ps.setString(2, chromosomeString);
                        ps.setInt(3, hashcode);
                    }

                });
    }

    public ClusteringResult getResult(final IChromosome chromosome) {
        Gene[] genes = chromosome.getGenes();
        final Object[] alleleValues = new Object[genes.length];
        for (int i = 0; i < genes.length; i++) {
            alleleValues[i] = genes[i].getAllele();
        }
        return jdbcTemplate.query("SELECT * from cluster_results WHERE chromosome=?", new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement) throws SQLException {
                statement.setString(1, gson.toJson(alleleValues));
            }

        }, new RowMapper<ClusteringResult>() {

            @Override
            public ClusteringResult mapRow(ResultSet rs, int arg1) throws SQLException {
                List<String> instances = new LinkedList<String>();
                for (String i : rs.getString("result").split(",")) {
                    instances.add(i);
                }
                ClusteringResult cr = new ClusteringResult(rs.getInt("chromosomeHash"), rs
                        .getString("chromosomeString"), rs.getString("description"));
                return cr;
            }
        }).get(0);
    }

    public String getPath() {
        return path;
    }

    @Required
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Helper to save the clustering result.
     * 
     * @author d.cherix
     * 
     */
    class SaveTask implements Runnable {

        private JdbcTemplate jdbcTemplate;
        private ClusteringResult cr;
        private String path;

        public SaveTask(JdbcTemplate jdbcTemplate, ClusteringResult cr, String path) {
            super();
            this.jdbcTemplate = jdbcTemplate;
            this.cr = cr;
            this.path = path;
        }

        @Override
        public void run() {
            jdbcTemplate
                    .update("INSERT INTO cluster_results (`chromosomeHash`, `chromosomeString`, `testDescription`, `precision`, `recall`,`fitness`) VALUES (?,?,?,?,?,?)",
                            new Object[] {
                                    cr.getChromosome(), cr.getChromosomeString(), cr.getDescription(),
                                    cr.getPrecision(), cr.getRecall(), cr.getFitness()
                            });
            // jdbcTemplate.batchUpdate("INSERT INTO cluster_coresp (chromosomeHash,uri) VALUES (?,?) ",
            // new BatchPreparedStatementSetter() {
            //
            // @Override
            // public int getBatchSize() {
            // return cr.getInstances().size();
            // }
            //
            // @Override
            // public void setValues(PreparedStatement ps, int i) throws SQLException {
            // ps.setInt(1,cr.getChromosome());
            // ps.setString(2,cr.getInstances().get(i));
            // }
            //
            // });
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(path + "/" + cr.getChromosome()));
                for (int i = 0; i < cr.getInstances().size(); i++) {
                    writer.append(cr.getInstances().get(i) + "\n");
                    if (i % 1000 == 0) {
                        writer.flush();
                    }
                }
            } catch (IOException e) {
                logger.warn("Exception on writing results", e);
            } finally {
                try {
                    writer.close();
                } catch (IOException e) {
                    logger.warn("Exception on writing results", e);
                }
            }
        }

    }

}
