package ontologymetrics.clustering;

import ontologymetrics.core.Config;
import ontologymetrics.core.data.ILoader;
import ontologymetrics.core.data.RawData;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.genetics.AbstractFitnessFunction;
import ontologymetrics.genetics.GeneticsConfiguraton;

import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.BooleanGene;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
/**
 * Stage for clustering.
 * @author d.cherix
 *
 */
public class ClusteringStage extends AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(ClusteringStage.class);

    @Autowired
    private GeneticsConfiguraton gConf;
    @Autowired
    private ClusteringResultDao dao;
    private RawData instances;

    private int sizeOfPopulation;
    @Autowired
    private AbstractFitnessFunction fitness;
    @Autowired
    private ILoader loader;
    private double threshold;
    private String arffFilePath;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        Config config = (Config)this.data.getData(Type.CONFIG);
        if(config.getConfigValue(this.name+".arffFilePath")==null){
            throw new StageInitException();
        }
        arffFilePath=config.getConfigValue(this.name+".arffFilePath");
        return true;
    }

    @Override
    protected void process() {
        instances = loader.load(arffFilePath);
        logger.info("{} instances loaded",instances.getInstances().size());
        fitness.setInstances(instances);
        fitness.setMaxValues(loader.getMaxValues());
        gConf.setFitness(fitness);
        Gene[] sampleGenes = new Gene[instances.getInstances().get(0).getPoint().length];

        try {
            for (int i = 0; i < sampleGenes.length; i++) {
                sampleGenes[i] = new BooleanGene(gConf.getConfiguration());
            }
            ExtendedChromosome chromosome = new ExtendedChromosome(gConf.getConfiguration(), sampleGenes);
            gConf.getConfiguration().setSampleChromosome(chromosome);
            gConf.getConfiguration().setPopulationSize(sizeOfPopulation);
            Genotype population = Genotype.randomInitialGenotype(gConf.getConfiguration());
            int tryes = 0;
            double maximalPossibilities = Math.pow(2, sampleGenes.length);
            logger.info("Begin to cluster");
            for(int i=0; i<population.getPopulation().getChromosomes().size();i++){
                IChromosome cr = population.getPopulation().getChromosome(i);
                dao.updateResult(cr.hashCode(), cr.getFitnessValueDirectly(),cr.toString());
                logger.debug("fitness {} for {} saved",cr.getFitnessValue(),cr.hashCode());
            }
            while (population.getFittestChromosome().getFitnessValue() < threshold
                    && tryes < maximalPossibilities) {
                population.evolve();
                tryes++;
                logger.info("try {} fittest value", tryes,population.getFittestChromosome().getFitnessValue());
                for(int i=0; i<population.getPopulation().getChromosomes().size();i++){
                    IChromosome cr = population.getPopulation().getChromosome(i);
                    dao.updateResult(cr.hashCode(), cr.getFitnessValue(),cr.toString());
                    logger.debug("fitness {} for {} saved",cr.hashCode(),cr.getFitnessValueDirectly());
                }
            }
            logger.info("fittest {}: {}", population.getFittestChromosome().hashCode(),population.getFittestChromosome());
        } catch (InvalidConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    @Required
    /**
     * Setter for the sie of the population
     * @param sizeOfPopulation the size to set
     */
    public void setSizeOfPopulation(int sizeOfPopulation) {
        this.sizeOfPopulation = sizeOfPopulation;
    }

    @Required
    /**
     * Setter to fix the threshold to reach for the fitness value.
     * @param threshold the threshold to set
     */
    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }
}
