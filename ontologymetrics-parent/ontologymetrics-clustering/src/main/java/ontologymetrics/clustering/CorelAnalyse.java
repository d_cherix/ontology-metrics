package ontologymetrics.clustering;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Setter;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

public class CorelAnalyse extends AbstractPipelineStage {
    
    
    private static final Logger logger = LoggerFactory.getLogger(CorelAnalyse.class);

    private Instances instances;
    @Setter
    private String filePath=null;
    @Setter
    private String untrustedPath=null;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        instances = (Instances) this.data.getData(Type.WEKAINSTANCES);
        if (instances == null) {
            throw new StageInitException("The Corelanalyse needs weka instances");
        }
        if (instances.attribute("cluster") == null) {
            throw new StageInitException("The Corelanalyse needs weka instances with a cluster attribute");
        }
        if(untrustedPath==null){
            try {
                this.setUntrustedPath("http://example.org/resource/u");
            } catch (UnsupportedEncodingException e) {
                throw new StageInitException(e);
            }
        }
        return true;
    }

    @Override
    protected void process() throws StageProcessingException {
        
        Attribute attrName = instances.attribute("name");
        Attribute attrCluster = instances.attribute("cluster");
        Instance instance;
        String untrustedCluster = this.data.getStringValue("untrustedClusterName");
        double tp = 0;
        double tn = 0;
        double fp = 0;
        double fn = 0;
        for (int i = 0; i < instances.numInstances(); i++) {
            instance = instances.instance(i);
            String clusterName = attrCluster.value((int) instance.value(attrCluster));
            if (attrName.value((int) instance.value(attrName)).startsWith(untrustedPath)) {
                if (clusterName.equals(untrustedCluster)) {
                    tp++;
                } else {
                    fn++;
                }
            } else {

                if (clusterName.equals(untrustedCluster)) {
                    fp++;
                } else {
                    tn++;
                }
            }
        }
        logger.debug("tp: {}\tfp: {}\ttn: {}\tfn: {}",new Object[]{tp,fp,tn,fn});
        BufferedWriter writer=null;
        double precision=tp/(tp+fp);
        double recall=tp/(tp+fn);
        try {
            writer = new BufferedWriter(new FileWriter(filePath,true));
            writer.append(String.valueOf(2.0*(precision*recall)/(precision+recall)));
            writer.append("\n");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    public void setUntrustedPath(String untrustedPath) throws UnsupportedEncodingException {
        this.untrustedPath = URLEncoder.encode(untrustedPath,"utf8");
    }

}
