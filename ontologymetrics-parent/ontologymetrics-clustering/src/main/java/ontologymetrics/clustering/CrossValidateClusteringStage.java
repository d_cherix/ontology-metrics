package ontologymetrics.clustering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import ontologymetrics.core.Config;
import ontologymetrics.core.data.ILoader;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.RawData;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.genetics.AbstractFitnessFunction;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.jgap.BaseChromosome;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.Population;
import org.jgap.impl.BooleanGene;
import org.jgap.impl.DefaultConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

public class CrossValidateClusteringStage extends AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(CrossValidateClusteringStage.class);

    @Autowired
    private ClusteringResultDao dao;
    private RawData instances;

    private int sizeOfPopulation;
    @Autowired
    private AbstractFitnessFunction fitness;
    @Autowired
    private ILoader loader;
    private double threshold;

    private BaseChromosome gConf;

    private DefaultConfiguration conf;

    private String description = "";

    private String arffFilePath;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        Config config = (Config)this.data.getData(Type.CONFIG);
        if(config.getConfigValue(this.name+".arffFilePath")==null){
            throw new StageInitException();
        }
        arffFilePath=config.getConfigValue(this.name+".arffFilePath");
        return true;
    }

    @Override
    protected void process() {
        instances = loader.load(arffFilePath);
        logger.info("{} instances loaded", instances.getInstances().size());
        fitness.setInstances(instances);
        fitness.setMaxValues(loader.getMaxValues());

        int trustedNb = (instances.getInstances().size() - instances.getUntrusteds()) / 10;
        int untrustedNb = instances.getUntrusteds() / 10;
        for (int index = 0; index < 10; index++) {
            List<Instance> tmp = new ArrayList<Instance>(trustedNb + untrustedNb);
            List<Integer> positions = new ArrayList<Integer>(trustedNb + untrustedNb);
            Set<Integer> posPos = new HashSet<Integer>();
            Set<Integer> posNeg = new HashSet<Integer>();
            Random r = new Random();
            while (posPos.size() < trustedNb || posNeg.size() < untrustedNb) {
                int pos = r.nextInt(instances.getInstances().size());
                if (!instances.getInstances().get(pos).isUntrusted()&& posPos.size() < trustedNb) {
                    posPos.add(pos);
                } else if (instances.getInstances().get(pos).isUntrusted()&& posNeg.size() < untrustedNb) {
                    posNeg.add(pos);
                }
            }
            positions.addAll(posNeg);
            positions.addAll(posPos);
            Collections.sort(positions);
            for(int i=0; i<positions.size();i++){
                tmp.add(instances.getInstances().remove(positions.get(i)-i));
            }
            IChromosome chromosome = null;
            logger.debug("Clustering Nb of Instances{}", instances.getInstances().size());
            try {
                chromosome = this.cluster(instances);
            } catch (InvalidConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            logger.debug("Validating Nb of Instances{}", tmp.size());
            ClusteringResult cr = this.validate(tmp, chromosome);
            cr.setDescription(cr.getDescription() + " try: " + index);
            dao.saveDirect(cr);
            for (int i = 0; i < tmp.size(); i++) {
                instances.getInstances().add(positions.get(i), tmp.get(i));
            }
        }
    }

    private IChromosome cluster(RawData instances) throws InvalidConfigurationException {
        Configuration.reset();
        conf = new DefaultConfiguration();
        Gene[] sampleGenes = new Gene[instances.getInstances().get(0).getPoint().length];
        for (int i = 0; i < sampleGenes.length; i++) {
            sampleGenes[i] = new BooleanGene(conf);
            // sampleGenes[i].setAllele(true);
        }

        Chromosome chromosome = new Chromosome(conf, sampleGenes);
        conf.setFitnessFunction(fitness);
        conf.setSampleChromosome(chromosome);
        conf.setPopulationSize(sizeOfPopulation);
        Genotype genotype = Genotype.randomInitialGenotype(conf);
        double maximalPossibilities = Math.pow(2, sampleGenes.length);
        logger.info("Begin to cluster with");
        int tryes=0;
        while (genotype.getFittestChromosome().getFitnessValue() < threshold
                && tryes*genotype.getPopulation().getChromosomes().size() < maximalPossibilities) {
            genotype.evolve();
            tryes++;
            logger.info("tested chromosomes {} fittest value {}", tryes*genotype.getPopulation().getChromosomes().size(), genotype.getFittestChromosome().getFitnessValue());
        }
        return genotype.getFittestChromosome();
    }

    private ClusteringResult validate(List<Instance> tmp, IChromosome chromosome) {
        IClusterer clusterer = (IClusterer) chromosome.getApplicationData();
        int fn = 0, fp = 0, tn = 0, tp = 0;
        for (Instance instance : tmp) {
            Cluster<Instance> cluster = null;
            try {
                cluster = clusterer.evaluate(instance);
            } catch (ClusteringException e) {
                logger.error("evaluation error", e);
            }

            if (instance.isUntrusted()) {
                if (cluster == clusterer.getUntrustedCluster()) {
                    tp++;
                } else {
                    fn++;
                }
            } else {
                if (cluster == clusterer.getUntrustedCluster()) {
                    fp++;
                } else {
                    tn++;
                }
            }
        }
        logger.debug("tp {} tn {} fp {} fn {}", new Object[] { tp, tn, fp, fn });
        double precision = (double) tp / ((double) tp + fp);
        precision = (Double.isNaN(precision)) ? 0 : precision;
        double recall = (double) tp / ((double) tp + fn);
        recall = (Double.isNaN(recall)) ? 0 : recall;
        double fmeasure = this.getFMeasure(precision, recall, 2);
        fmeasure = (Double.isNaN(fmeasure)) ? 0 : fmeasure;
        ClusteringResult cr = new ClusteringResult(chromosome.hashCode(), chromosome.toString(), description);
        cr.setFitness(fmeasure);
        cr.setPrecision(precision);
        cr.setRecall(recall);
        logger.debug("Evaluation: precision {} recall {} fitness {}", precision, recall, fmeasure);
        return cr;
    }

    private double getFMeasure(double precision, double recall, double beta) {
        return (1 + Math.pow(beta, 2)) * ((precision * recall) / ((Math.pow(beta, 2) * precision) + recall));
    }


    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    @Required
    public void setSizeOfPopulation(int sizeOfPopulation) {
        this.sizeOfPopulation = sizeOfPopulation;
    }

    @Required
    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
