package ontologymetrics.clustering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ontologymetrics.core.Config;
import ontologymetrics.core.data.ILoader;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.RawData;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.genetics.AbstractFitnessFunction;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.jgap.Chromosome;
import org.jgap.Gene;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.BooleanGene;
import org.jgap.impl.DefaultConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;

public class CrossValidateForChromosome extends AbstractPipelineStage{


        private static final Logger logger = LoggerFactory.getLogger(CrossValidateClusteringStage.class);
        
        @Autowired
        private ApplicationContext appContext;

        @Autowired
        private ClusteringResultDao dao;
        private RawData instances;

        @Autowired
        private AbstractFitnessFunction fitness;
        @Autowired
        private ILoader loader;
        private DefaultConfiguration conf;

        private String description = "";
        
        private boolean[] weights;

        private String arffFilePath;

        @Override
        protected boolean prerequisitesFulfilled() throws StageInitException {
            Config config = (Config)this.data.getData(Type.CONFIG);
            if(config.getConfigValue(this.name+".arffFilePath")==null){
                throw new StageInitException();
            }
            arffFilePath=config.getConfigValue(this.name+".arffFilePath");
            return true;
        }

        @Override
        protected void process() {
            instances = loader.load(arffFilePath);
            logger.info("{} instances loaded", instances.getInstances().size());
            fitness.setInstances(instances);
            fitness.setMaxValues(loader.getMaxValues());

            int trustedNb = (instances.getInstances().size() - instances.getUntrusteds()) / 10;
            int untrustedNb = instances.getUntrusteds() / 10;
            int untrustedStart = instances.getInstances().size() - instances.getUntrusteds() - 1;
            for (int index = 0; index < 10; index++) {
                List<Instance> tmp = new ArrayList<Instance>(trustedNb + untrustedNb);
                List<Integer> positions = new ArrayList<Integer>(trustedNb + untrustedNb);
                for (int i = index * trustedNb; i < (index + 1) * trustedNb; i++) {
                    positions.add(i);
                }
                for (int i = untrustedStart+(index*untrustedNb); i<untrustedStart+((index+1)*untrustedNb);i++) {
                    positions.add(i);
                }
                for(int i=0; i<positions.size();i++){
                    tmp.add(instances.getInstances().remove(positions.get(i)-i));
                }
                logger.debug("TMP: {}",positions);
                IClusterer clusterer=null;
                logger.debug("Clustering Nb of Instances{}", instances.getInstances().size());
                try {
                    clusterer = this.cluster(instances);
                } catch (InvalidConfigurationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                for (int i = 0; i < tmp.size(); i++) {
                    instances.getInstances().add(positions.get(i), tmp.get(i));
                }
                logger.debug("Validating Nb of Instances{}", tmp.size());
                ClusteringResult cr = this.validate(tmp, clusterer);
                cr.setDescription(cr.getDescription() + " try: " + index);
                dao.saveDirect(cr);
            }
        }

        private IClusterer cluster(RawData instances) throws InvalidConfigurationException {
            IClusterer clusterer = (IClusterer) appContext.getBean("clusterer");
            System.out.println(clusterer);
            conf.reset();
            conf = new DefaultConfiguration();
            Gene[] sampleGenes = new Gene[instances.getInstances().get(0).getPoint().length];
            for (int i = 0; i < sampleGenes.length; i++) {
                sampleGenes[i] = new BooleanGene(conf);
                sampleGenes[i].setAllele(weights[i]);
            }

            Chromosome chromosome = new Chromosome(conf, sampleGenes);
            try {
                clusterer.cluster(chromosome, instances, instances.getMaxValues());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            chromosome.setApplicationData(clusterer);
            return clusterer;
        }

        private ClusteringResult validate(List<Instance> tmp, IClusterer clusterer) {
            int fn = 0, fp = 0, tn = 0, tp = 0;
            for (Instance instance : tmp) {
                Cluster<Instance> cluster = null;
                try {
                    cluster = clusterer.evaluate(instance);
                } catch (ClusteringException e) {
                    ClusteringResult cr = new ClusteringResult(clusterer.hashCode(), "Fitness: "+getFMeasure(clusterer.getPrecision(), clusterer.getRecall(), 2)+" precision: "+clusterer.getPrecision()+" recall: "+clusterer.getRecall()+" weights:"+Arrays.toString(weights), description);
                    cr.setFitness(0);
                    cr.setPrecision(0);
                    cr.setRecall(0);
                    logger.debug("Evaluation: precision {} recall {} fitness {}", 0, 0, 0);
                    return cr;
                }
                
                if(clusterer.getUntrustedCluster()==null){
                    ClusteringResult cr = new ClusteringResult(clusterer.hashCode(), "Fitness: "+getFMeasure(clusterer.getPrecision(), clusterer.getRecall(), 2)+" precision: "+clusterer.getPrecision()+" recall: "+clusterer.getRecall()+" weights:"+Arrays.toString(weights), description);
                    cr.setFitness(0);
                    cr.setPrecision(0);
                    cr.setRecall(0);
                    logger.debug("Evaluation: precision {} recall {} fitness {}", 0, 0, 0);
                    return cr;
                }
                
                if (instance.isUntrusted()) {
                    if (cluster == clusterer.getUntrustedCluster()) {
                        tp++;
                    } else {
                        fn++;
                    }
                } else {
                    if (cluster == clusterer.getUntrustedCluster()) {
                        fp++;
                    } else {
                        tn++;
                    }
                }
            }
            logger.debug("tp {} tn {} fp {} fn {}", new Object[] { tp, tn, fp, fn });
            double precision = (double) tp / ((double) tp + fp);
            precision = (Double.isNaN(precision)) ? 0 : precision;
            double recall = (double) tp / ((double) tp + fn);
            recall = (Double.isNaN(recall)) ? 0 : recall;
            double fmeasure = this.getFMeasure(precision, recall, 2);
            fmeasure = (Double.isNaN(fmeasure)) ? 0 : fmeasure;
            ClusteringResult cr = new ClusteringResult(clusterer.hashCode(), "Fitness: "+getFMeasure(clusterer.getPrecision(), clusterer.getRecall(), 2)+" precision: "+clusterer.getPrecision()+" recall: "+clusterer.getRecall()+" weights:"+Arrays.toString(weights), description);
            cr.setFitness(fmeasure);
            cr.setPrecision(precision);
            cr.setRecall(recall);
            logger.debug("Evaluation: precision {} recall {} fitness {}", precision, recall, fmeasure);
            return cr;
        }

        private double getFMeasure(double precision, double recall, double beta) {
            return (1 + Math.pow(beta, 2)) * ((precision * recall) / ((Math.pow(beta, 2) * precision) + recall));
        }


        @Override
        protected void finalise() {
            // TODO Auto-generated method stub

        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean[] getWeights() {
            return weights;
        }

        public void setWeights(boolean[] weights) {
            this.weights = weights;
        }
    }

