package ontologymetrics.clustering;

import java.util.LinkedList;
import java.util.List;

import ontologymetrics.core.Beta;
import ontologymetrics.core.Beta.Cause;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.RawData;

import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.jgap.Gene;
import org.jgap.IChromosome;
import org.jgap.impl.DoubleGene;
import org.jgap.impl.IntegerGene;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

public class DBSCANClustering implements IClusterer {

    private int minPts;
    private double eps;
    private double precision;
    private double recall;
    private List<Cluster<Instance>> clusters;
    private Cluster<Instance> untrustedCluster;
    private ExtendedDistanceMeasure distanceMeasure;

    private static final Logger logger = LoggerFactory.getLogger(DBSCANClustering.class);

    @Autowired
    private ClusteringResultDao dao;
    private String description;

    @Override
    public double getPrecision() {
        return precision;
    }

    @Override
    public double getRecall() {
        return recall;
    }

    @Override
    public void cluster(IChromosome chromosome, RawData instances, double[] maxValues) throws ClusteringException {
        double[] weights = new double[chromosome.getGenes().length];
        for (int i = 0; i < chromosome.getGenes().length; i++) {
            Gene g = chromosome.getGenes()[i];
            if (g instanceof IntegerGene) {
                double value = ((Integer) g.getAllele()).doubleValue();
                weights[i] = value;
            } else if (g instanceof DoubleGene) {
                double value = ((Double) g.getAllele());
                weights[i] = value;
            }
        }
        instances.setWeights(weights);
        this.distanceMeasure = new ExtendedDistanceMeasure(instances);
        DBSCANClusterer<Instance> clusterer = new DBSCANClusterer<Instance>(eps, minPts,
                this.distanceMeasure);
        logger.info("clustering started");
        Monitor mon = MonitorFactory.start("clustering");
        this.clusters = clusterer.cluster(instances.getInstances());
        mon.stop();
        logger.info("clustering finished");
        untrustedCluster = this.evaluateClusters(instances.getUntrusteds());
        ClusteringResult result;

        List<String> instancesNames = new LinkedList<String>();
        if (untrustedCluster != null) {
            for (Instance i : untrustedCluster.getPoints()) {
                instancesNames.add(i.getName());
            }
        } else {
            instancesNames.add("No result");
        }
        result = new ClusteringResult(chromosome.hashCode(), chromosome.toString(), instancesNames, this.description);

        dao.save(result);
    }

    private Cluster<Instance> evaluateClusters(int nbUntrusteds) {
        int maxUntrusteds = 0;
        int clusterUntrusteds;
        Cluster<Instance> cluster = null;
        for (int i = 0; i < this.clusters.size(); i++) {
            clusterUntrusteds = 0;
            for (Instance inst : this.clusters.get(i).getPoints()) {
                if (inst.isUntrusted()) {
                    clusterUntrusteds++;
                }
            }
            if (clusterUntrusteds > maxUntrusteds) {
                clusterUntrusteds = maxUntrusteds;
                cluster = this.clusters.get(i);
            }
        }
        if (cluster != null) {
            this.precision = ((double) maxUntrusteds / (double) cluster.getPoints().size());
            this.recall = ((double) cluster.getPoints().size() / (double) nbUntrusteds);
        } else {
            this.precision = 0;
            this.recall = 0;
        }
        return cluster;
    }

    public int getMinPts() {
        return minPts;
    }

    public void setMinPts(int minPts) {
        this.minPts = minPts;
    }

    public double getEps() {
        return eps;
    }

    public void setEps(double eps) {
        this.eps = eps;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Beta(cause = Cause.NOTYETIMPLEMETED)
    @Override
    public Cluster<Instance> evaluate(Instance i) throws ClusteringException {
        // TODO Auto-generated method stub
        return null;
    }

    @Beta(cause = Cause.NOTYETIMPLEMETED)
    @Override
    public Cluster<Instance> getUntrustedCluster() {
        return null;
    }

    public ExtendedDistanceMeasure getDistanceMeasure() {
        return distanceMeasure;
    }

    public void setDistanceMeasure(ExtendedDistanceMeasure distanceMeasure) {
        this.distanceMeasure = distanceMeasure;
    }

}
