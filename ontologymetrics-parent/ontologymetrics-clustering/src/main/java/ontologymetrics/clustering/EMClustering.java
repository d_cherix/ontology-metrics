package ontologymetrics.clustering;

import lombok.Setter;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.clusterers.ClusterEvaluation;
import weka.clusterers.EM;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.RemoveType;

public class EMClustering extends AbstractPipelineStage {
    
    
    private static final Logger logger = LoggerFactory.getLogger(EMClustering.class);
    private Instances instances=null;
    @Setter
    private int maxIterations=100;
    @Setter
    private int numClusters=2;
    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        instances=(Instances)this.data.getData(Type.WEKAINSTANCES);
        if(instances==null){
            throw new StageInitException("No weka instances in the pipeline object");
        }
        return true;
    }

    @Override
    protected void process() throws StageProcessingException{
        RemoveType filter = new RemoveType();
 
        EM clusterer = new EM();
        ClusterEvaluation eval = new ClusterEvaluation();
        clusterer.setDebug(logger.isDebugEnabled());
        try {
            filter.setOptions(new String[]{"-T","string"});
            filter.setInputFormat(instances);
            Instances filteredInstances = Filter.useFilter(instances, filter);
            clusterer.setMaxIterations(this.maxIterations);
            clusterer.setNumClusters(this.numClusters);
            clusterer.buildClusterer(filteredInstances);
            eval.setClusterer(clusterer);
            eval.evaluateClusterer(filteredInstances);
            System.out.println(eval.clusterResultsToString());
            double[] assign = eval.getClusterAssignments();
            for(int i=0; i< filteredInstances.numInstances();i++){
                System.out.println(filteredInstances.instance(i).value(0)+"\t->\t"+assign[i]);
            }
        } catch (Exception e) {
            throw new StageProcessingException("Error by building cluster", e);
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub
        
    }
}
