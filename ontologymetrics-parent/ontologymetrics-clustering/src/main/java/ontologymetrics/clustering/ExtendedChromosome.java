package ontologymetrics.clustering;

import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.Gene;
import org.jgap.IGeneConstraintChecker;
import org.jgap.InvalidConfigurationException;
import org.jgap.UnsupportedRepresentationException;

public class ExtendedChromosome extends Chromosome {
    
    private IClusterer clustering;

    public ExtendedChromosome() throws InvalidConfigurationException {
        super();
    }

    public ExtendedChromosome(Configuration a_configuration, Gene a_sampleGene, int a_desiredSize,
            IGeneConstraintChecker a_constraintChecker) throws InvalidConfigurationException {
        super(a_configuration, a_sampleGene, a_desiredSize, a_constraintChecker);
    }

    public ExtendedChromosome(Configuration a_configuration, Gene a_sampleGene, int a_desiredSize)
            throws InvalidConfigurationException {
        super(a_configuration, a_sampleGene, a_desiredSize);
    }

    public ExtendedChromosome(Configuration a_configuration, Gene[] a_initialGenes,
            IGeneConstraintChecker a_constraintChecker) throws InvalidConfigurationException {
        super(a_configuration, a_initialGenes, a_constraintChecker);
    }

    public ExtendedChromosome(Configuration a_configuration, Gene[] a_initialGenes)
            throws InvalidConfigurationException {
        super(a_configuration, a_initialGenes);
    }

    public ExtendedChromosome(Configuration a_configuration, int a_desiredSize) throws InvalidConfigurationException {
        super(a_configuration, a_desiredSize);
    }

    public ExtendedChromosome(Configuration a_configuration, String a_persistentRepresentatuion)
            throws InvalidConfigurationException, UnsupportedRepresentationException {
        super(a_configuration, a_persistentRepresentatuion);
    }

    public ExtendedChromosome(Configuration a_configuration) throws InvalidConfigurationException {
        super(a_configuration);
    }

    public IClusterer getClustering() {
        return clustering;
    }

    public void setClustering(IClusterer clustering) {
        this.clustering = clustering;
    }

}
