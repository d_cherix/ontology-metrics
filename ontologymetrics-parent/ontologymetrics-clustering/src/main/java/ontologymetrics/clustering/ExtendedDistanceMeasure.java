package ontologymetrics.clustering;

import java.util.Arrays;

import ontologymetrics.core.data.IAttribute;
import ontologymetrics.core.data.RawData;

import org.apache.commons.math3.ml.distance.DistanceMeasure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

public class ExtendedDistanceMeasure implements DistanceMeasure {

    /**
     * 
     */
    private static final long serialVersionUID = 7750601418895666142L;
    private RawData instances;
    private boolean normalized=false;
    
    private static final Logger logger = LoggerFactory.getLogger(ExtendedDistanceMeasure.class);

    public ExtendedDistanceMeasure(RawData instances) {
        this.instances = instances;
    }

    @Override
    public double compute(double[] a, double[] b) {
        double max=0;
        double sum = 0;
        double weight;
        double[] maxValues = this.instances.getMaxValues();
        IAttribute attr_a;
        IAttribute attr_b;
        Monitor mon = MonitorFactory.start("distance");
        for (int i = 0; i < a.length; i++) {
            weight = this.instances.getWeights()[i];
            if(weight==.0){
                continue;
            }
            max+=weight;
            attr_a = this.instances.getAttributes().get(((Double) a[i]).intValue());
            attr_b = this.instances.getAttributes().get(((Double) b[i]).intValue());
            sum += weight * Math.pow(attr_a.distance(attr_b, maxValues[i]), 2);
        }
        mon.stop();
        if (logger.isTraceEnabled() && mon.getHits() % 1000000 == 0) {
            logger.trace("{} distances computed", mon.getHits());
        }
        if(normalized){
           return Math.sqrt(sum)/Math.sqrt(max);
        }
        return Math.sqrt(sum);
    }

    public void setNormalized(boolean normalized) {
        this.normalized = normalized;
    }
    
    

}
