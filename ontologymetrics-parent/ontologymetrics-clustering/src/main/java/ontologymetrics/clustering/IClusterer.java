package ontologymetrics.clustering;

import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.RawData;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.distance.DistanceMeasure;
import org.jgap.IChromosome;
/**
 * This interface defines the method than a clusterer need.
 * @author d.cherix
 *
 */
public interface IClusterer  {
    /**
     * Getter for the precision.
     * @return the precision of the cluster result.
     */
    public double getPrecision();
    /**
     * Getter for the recall.
     * @return the recall of the cluster result.
     */
    public double getRecall();
    /**
     * Cluster and create a model of the instances
     * @param chromosome an IChromosome instance to weights the metrics
     * @param instances the Instances to cluster
     * @param maxValues the maximum value for each metric
     * @throws ClusteringException exception while clustering
     */
    public void cluster(IChromosome chromosome, RawData instances, double[] maxValues) throws ClusteringException;
    /**
     * To add a description for this cluster task.
     * @param description the description to set
     */
    public void setDescription(String description);
    /**
     * Evaluate to which cluster an instance should be add
     * @param instance the instance to evaluate
     * @return the cluste to which the instance can be add
     * @throws ClusteringException 
     */
    public Cluster<Instance> evaluate(Instance instance) throws ClusteringException;
    /**
     * 
     * @return the cluster for the untrusteds instances.
     */
    public Cluster<Instance> getUntrustedCluster();
    public DistanceMeasure getDistanceMeasure();
}
