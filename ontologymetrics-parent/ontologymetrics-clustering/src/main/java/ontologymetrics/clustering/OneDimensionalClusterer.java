package ontologymetrics.clustering;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.Vector;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.clusterers.Clusterer;
import weka.clusterers.NumberOfClustersRequestable;
import weka.clusterers.RandomizableClusterer;
import weka.core.Attribute;
import weka.core.Capabilities;
import weka.core.Capabilities.Capability;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Option;
import weka.core.WeightedInstancesHandler;

import com.google.common.collect.Sets;

public class OneDimensionalClusterer extends RandomizableClusterer implements Clusterer, NumberOfClustersRequestable,
        WeightedInstancesHandler {
    @Getter
    private String instancesClustersAttribution;

    @Override
    public Capabilities getCapabilities() {
        Capabilities capabilities = super.getCapabilities();
        capabilities.disableAll();
        capabilities.enable(Capability.NO_CLASS);
        capabilities.enable(Capability.NUMERIC_ATTRIBUTES);
        return capabilities;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("Not implemented yet");
    }

    private static final long serialVersionUID = -4742329115531465236L;

    private static final Logger logger = LoggerFactory.getLogger(OneDimensionalClusterer.class);

    private int numClusters;

    private int[] correspondances;

    @Override
    public void buildClusterer(Instances data) throws Exception {
        data.sort(1);
        DescriptiveStatistics stats = new DescriptiveStatistics();
        TreeSet<Double> highests = Sets.newTreeSet();
        for (int i = 0; i < data.numInstances() - 1; i++) {
            highests.add(data.instance(i + 1).value(1) - data.instance(i).value(1));
            if (highests.size() % 1000 == 0) {
                TreeSet<Double> tmp = Sets.newTreeSet();
                for (int j = 0; j < numClusters - 1; j++) {
                    tmp.add(highests.pollLast());
                }
                highests = tmp;
            }
        }
        TreeSet<Double> tmp = Sets.newTreeSet();
        for (int j = 0; j < numClusters - 1; j++) {
            tmp.add(highests.pollLast());
        }
        highests = tmp;
        StringBuilder builder = new StringBuilder();
        int clusterNumber = 0;
        Instance instance;
        correspondances = new int[data.numInstances()];
        FastVector clusterNames = new FastVector();
        for (int i = 0; i < this.numClusters; i++) {
            clusterNames.addElement("cluster_"+i);
        }
        Attribute clusterAttr = new Attribute("cluster",clusterNames);
        data.insertAttributeAt(clusterAttr, data.numAttributes());
        clusterAttr=data.attribute("cluster");
        for (int i = 0; i < data.numInstances() - 1; i++) {
            instance = data.instance(i);
            correspondances[(int) instance.value(0)] = clusterNumber;
            instance.setValue(clusterAttr, "cluster_"+clusterNumber);
            stats.addValue(instance.value(1));
            builder.append(instance);
            builder.append("\t");
            builder.append(clusterNumber);
            builder.append("\n");
            if (highests.contains(data.instance(i + 1).value(1) - instance.value(1))) {
                clusterNumber++;
            }
        }
        instance = data.lastInstance();
        instance.setValue(clusterAttr, "cluster_"+clusterNumber);
        correspondances[(int) instance.value(0)] = clusterNumber;
//        instance.setValue(clusterAttr, "cluster_"+clusterNumber);
        builder.append(instance);
        builder.append("\t");
        builder.append(clusterNumber);
        builder.append("\n*****\n");
        builder.append(stats.getPercentile(95));
        this.instancesClustersAttribution = builder.toString();
    }

    @Override
    public int numberOfClusters() throws Exception {
        return numClusters;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Enumeration listOptions() {
        Vector<Object> result = new Vector<Object>();

        result.addElement(new Option(
                "\tnumber of clusters.\n"
                        + "\t(default 2).",
                "N", 1, "-N <num>"));
        Enumeration en = super.listOptions();
        while (en.hasMoreElements())
            result.addElement(en.nextElement());
        return result.elements();
    }

    public String getInstancesClustersAttribution() {
        return instancesClustersAttribution;
    }

    public int[] getCorrespondances() {
        return correspondances;
    }

    public void setNumClusters(int numClusters) {
        this.numClusters = numClusters;
    }
}
