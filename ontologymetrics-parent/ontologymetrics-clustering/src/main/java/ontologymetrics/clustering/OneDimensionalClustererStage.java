package ontologymetrics.clustering;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import lombok.Setter;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

public class OneDimensionalClustererStage extends AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(OneDimensionalClustererStage.class);

    private Instances instances = null;
    @Setter
    private int numClusters = 2;
    @Setter
    private String columnIndex = String.valueOf(2);

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        instances = (Instances) this.data.getData(Type.WEKAINSTANCES);
        if (instances == null) {
            throw new StageInitException("No weka instances in the pipeline object");
        }
        return true;
    }

    @Override
    protected void process() throws StageProcessingException {
        Remove filter = new Remove();
        OneDimensionalClusterer clusterer = new OneDimensionalClusterer();
        clusterer.setNumClusters(numClusters);
        try {
            filter.setOptions(new String[] { "-R", "First," + this.columnIndex, "-V" });
            filter.setInputFormat(instances);
            Instances filteredInstances = Filter.useFilter(instances, filter);
            clusterer.buildClusterer(filteredInstances);
            Attribute idAttr = filteredInstances.attribute("ID");
            filteredInstances.sort(idAttr);
            idAttr = instances.attribute("ID");
            filter = new Remove();
            filter.setOptions(new String[] { "-R", "Last", "-V" });
            filter.setInputFormat(filteredInstances);
            this.data.addData(Type.WEKAINSTANCES,Instances.mergeInstances(instances, Filter.useFilter(filteredInstances, filter)));
            System.out.println(this.data.getData(Type.WEKAINSTANCES));
        } catch (Exception e) {
            logger.error("", e);
            throw new StageProcessingException(e);
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
