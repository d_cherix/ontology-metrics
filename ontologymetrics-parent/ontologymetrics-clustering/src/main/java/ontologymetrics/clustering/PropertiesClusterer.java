package ontologymetrics.clustering;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import lombok.Setter;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import cern.colt.Arrays;
import weka.clusterers.Clusterer;
import weka.clusterers.RandomizableClusterer;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import au.com.bytecode.opencsv.CSVWriter;

public class PropertiesClusterer extends RandomizableClusterer implements Clusterer {

    private static final Logger logger = LoggerFactory.getLogger(PropertiesClusterer.class);
    @Autowired
    private SparqlPrefixes prefixes;

    private String path;

    @Override
    public void buildClusterer(Instances data) {
        Enumeration<Attribute> enumeration = data.enumerateAttributes();
        Instance instance;
        Map<Double, Integer>[] values = new HashMap[data.numAttributes() - 1];
        for (int i = 0; i < data.numInstances(); i++) {
            instance = data.instance(i);
            for (int j = 0; j < data.numAttributes() - 1; j++) {
                if (i == 0) {
                    values[j] = new HashMap<Double, Integer>();
                }
                double key = instance.value(j + 1);
                if (values[j].containsKey(key)) {
                    values[j].put(key, values[j].get(key) + 1);
                } else {
                    values[j].put(key, 1);
                }
            }
        }
        System.out.println(this.path + prefixes.shortForm(data.relationName()) + ".csv");

        CSVWriter writer = null;

        try {
            writer = new CSVWriter(new FileWriter(this.path + prefixes.shortForm(data.relationName()) + ".csv"), ',',
                    CSVWriter.NO_QUOTE_CHARACTER);
            writer.writeNext(new String[] { "Property", "Value", "Percent" });
            for (int i = 0; i < values.length; i++) {
                for (Double key : values[i].keySet()) {
                    long percent = Math.round(((double) values[i].get(key) / (double) data.numInstances()) * 10000);
                    writer.writeNext(new String[] { data.attribute(i + 1).name(), key.toString(),
                            String.valueOf((double) percent / 100.0) });
                }
            }
        } catch (IOException e) {
            logger.error("Error on writing results", e);
        } finally {
            try {
                writer.close();
            } catch (Exception e) {
                logger.error("Error on writing results", e);
            }
        }
    }

    @Override
    public int numberOfClusters() throws Exception {
        // TODO Auto-generated method stub
        return 0;
    }

    public void setPrefixes(SparqlPrefixes prefixes) {
        this.prefixes = prefixes;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
