package ontologymetrics.clustering;

import lombok.Setter;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.RemoveType;
import weka.filters.unsupervised.attribute.RemoveUseless;

public class PropertiesClusteringStage extends AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(PropertiesClusteringStage.class);

    private Instances instances;
    @Setter
    private String path="";
    @Autowired
    private SparqlPrefixes prefixes;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        instances = (Instances) this.data.getData(Type.WEKAINSTANCES);
        if (instances == null) {
            throw new StageInitException("No weka instances in the pipeline object");
        }
        return true;
    }

    @Override
    protected void process() throws StageProcessingException {
        Filter filter = new RemoveType();
        try {
            filter.setInputFormat(instances);
            Instances filteredInstances = Filter.useFilter(instances, filter);
            filter = new RemoveUseless();
            filter.setInputFormat(filteredInstances);
            Instances filteredInstances2 = Filter.useFilter(filteredInstances, filter);
            PropertiesClusterer clusterer = new PropertiesClusterer();
            clusterer.setPath(path);
            clusterer.setPrefixes(prefixes);
            clusterer.buildClusterer(filteredInstances2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
