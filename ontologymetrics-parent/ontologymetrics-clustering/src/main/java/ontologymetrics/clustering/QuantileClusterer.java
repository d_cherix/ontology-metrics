package ontologymetrics.clustering;

import java.util.Enumeration;

import lombok.Setter;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.clusterers.Clusterer;
import weka.clusterers.RandomizableClusterer;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class QuantileClusterer extends RandomizableClusterer implements Clusterer {

    private static final Logger logger = LoggerFactory.getLogger(QuantileClusterer.class);
    /**
     * 
     */
    private static final long serialVersionUID = -3667586427300350599L;

    private double percentile;

    @Override
    public void buildClusterer(Instances data) throws Exception {
        DescriptiveStatistics descStats = new DescriptiveStatistics(data.attributeToDoubleArray(1));
        double threshold = descStats.getPercentile(percentile);
        logger.debug("Threshold: {}",threshold);
        Instance instance;
        FastVector clusterNames = new FastVector();
        clusterNames.addElement("trusted");
        clusterNames.addElement("untrusted");
        Attribute clustAttr = new Attribute("cluster", clusterNames);
        data.insertAttributeAt(clustAttr, data.numAttributes());
        clustAttr = data.attribute("cluster");
        for (Enumeration<Instance> enumeration = data.enumerateInstances(); enumeration.hasMoreElements();) {
            instance = enumeration.nextElement();
            if (instance.value(1) <= threshold) {
                instance.setValue(clustAttr, "trusted");
            } else {
                instance.setValue(clustAttr, "untrusted");
            }
        }
    }

    @Override
    public int numberOfClusters() throws Exception {
        return 2;
    }

    public void setPercentile(double percentile) {
        this.percentile = percentile;
    }
}
