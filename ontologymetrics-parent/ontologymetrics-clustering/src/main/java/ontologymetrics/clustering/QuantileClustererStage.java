package ontologymetrics.clustering;

import lombok.Setter;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.core.Attribute;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

public class QuantileClustererStage extends AbstractPipelineStage {
    
    
    private static final Logger logger = LoggerFactory.getLogger(QuantileClustererStage.class);

    private Instances instances;

    private String columnIndex;

    private double quantile=-1;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        instances = (Instances) this.data.getData(Type.WEKAINSTANCES);
        if (instances == null) {
            throw new StageInitException("No weka instances in the pipeline object");
        }
        if (quantile==-1){
            throw new StageInitException("A quantile value is needed from stage "+this.name);
        }
        return true;
    }

    @Override
    protected void process() throws StageProcessingException {
        Remove filter = new Remove();
        QuantileClusterer clusterer = new QuantileClusterer();
        clusterer.setPercentile(quantile);
        try {
            filter.setOptions(new String[] { "-R", "First," + this.columnIndex, "-V" });
            filter.setInputFormat(instances);
            Instances filteredInstances = Filter.useFilter(instances, filter);
            clusterer.buildClusterer(filteredInstances);
            Attribute idAttr = filteredInstances.attribute("ID");
            filteredInstances.sort(idAttr);
            idAttr = instances.attribute("ID");
            filter = new Remove();
            filter.setOptions(new String[] { "-R", "Last", "-V" });
            filter.setInputFormat(filteredInstances);
            this.data.addData(Type.WEKAINSTANCES,
                    Instances.mergeInstances(instances, Filter.useFilter(filteredInstances, filter)));
            this.data.addKeyValue("untrustedClusterName", "untrusted");
        } catch (Exception e) {
            logger.error("", e);
            throw new StageProcessingException(e);
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    public void setColumnIndex(String columnIndex) {
        this.columnIndex = columnIndex;
    }

    public void setQuantile(double quantile) {
        this.quantile = quantile;
    }

}
