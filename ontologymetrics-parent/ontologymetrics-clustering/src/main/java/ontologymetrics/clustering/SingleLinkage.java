package ontologymetrics.clustering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ontologymetrics.core.data.SymmetricMatrix;

import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.Clusterer;
import org.apache.commons.math3.ml.distance.DistanceMeasure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SingleLinkage<T extends Clusterable> extends Clusterer<T> {

    
    private static final Logger logger = LoggerFactory.getLogger(SingleLinkage.class);
    private int k;

    public SingleLinkage(int k, DistanceMeasure measure) {
        super(measure);
        this.k = k;
    }

    @Override
    public List<Cluster<T>> cluster(Collection<T> points) throws MathIllegalArgumentException,
            ConvergenceException {
        ArrayList<T> data;
        if (!(points instanceof ArrayList)) {
            data = new ArrayList<T>();
            data.addAll(points);
        } else {
            data = (ArrayList<T>) points;
        }
        logger.debug("Start distance computing");
        SymmetricMatrix<Double> distMatrix = new SymmetricMatrix<Double>(data.size());
        Cluster<T>[] clusters = new Cluster[data.size()];
        int clusterNumber = data.size();
        for (int i = 0; i < data.size(); i++) {
            for (int j = i + 1; j < data.size(); j++) {
                distMatrix.add(i, j, this.getDistanceMeasure().compute(data.get(i).getPoint(), data.get(j).getPoint()));
            }
            logger.debug("{} distances computed", (i+1)*data.size()-i);
        }
        logger.debug("distance computing finished");
        while (clusterNumber > this.k) {
            int[] value = distMatrix.getSmallest();
            if (clusters[value[0]] == null && clusters[value[1]] == null) {
                Cluster<T> cluster = new Cluster<T>();
                cluster.addPoint(data.get(value[0]));
                cluster.addPoint(data.get(value[1]));
                clusters[value[0]] = cluster;
                clusters[value[1]] = cluster;
            } else if (clusters[value[0]] != null && clusters[value[1]] == null) {
                clusters[value[0]].addPoint(data.get(value[1]));
                clusters[value[1]] = clusters[value[0]];
            } else if (clusters[value[0]] == null && clusters[value[1]] != null) {
                clusters[value[1]].addPoint(data.get(value[0]));
                clusters[value[0]] = clusters[value[1]];
            } else {
                for (T point : clusters[value[0]].getPoints()) {
                    clusters[value[1]].addPoint(point);
                }
                clusters[value[0]] = clusters[value[1]];
            }
            clusterNumber--;
            distMatrix.add(value[0], value[1], Double.MAX_VALUE);
            if(clusterNumber%1000==0){
                logger.debug("number of cluster {}",clusterNumber);
            }
        }
        distMatrix = null;
        List<Cluster<T>> cl = new LinkedList<Cluster<T>>();
        for (int i = 0; i < clusters.length; i++) {
            if (clusters[i] == null) {
                Cluster<T> c = new Cluster<T>();
                c.addPoint(data.get(i));
                cl.add(c);
            } else {
                if (!cl.contains(clusters[i])) {
                    cl.add(clusters[i]);
                }
            }
        }
        return cl;
    }

}
