package ontologymetrics.clustering;

import java.util.LinkedList;
import java.util.List;

import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.RawData;

import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.jgap.Gene;
import org.jgap.IChromosome;
import org.jgap.impl.BooleanGene;
import org.jgap.impl.DoubleGene;
import org.jgap.impl.IntegerGene;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

public class SingleLinkageClusterer implements IClusterer {

    private double precision;
    private double recall;

    private static final Logger logger = LoggerFactory.getLogger(KMeansPlusPlus.class);

    @Autowired
    private ClusteringResultDao dao;
    private String description;
    private int clusterNumber;
  
    private boolean saveToDB = true;
    private List<Cluster<Instance>> clusters;
    private Cluster<Instance> untrustedCluster;
    private ExtendedDistanceMeasure distanceMeasure;

    @Override
    public double getPrecision() {
        return precision;
    }

    @Override
    public double getRecall() {
        return recall;
    }

    @Override
    public void cluster(IChromosome chromosome, RawData instances, double[] maxValues) throws ClusteringException {
        clusters=null;
        double[] weights = new double[chromosome.getGenes().length];
        for (int i = 0; i < chromosome.getGenes().length; i++) {
            Gene g = chromosome.getGenes()[i];
            if (g instanceof IntegerGene) {
                double value = ((Integer) g.getAllele()).doubleValue();
                weights[i] = value;
            } else if (g instanceof DoubleGene) {
                double value = ((Double) g.getAllele());
                weights[i] = value;
            } else if (g instanceof BooleanGene) {
                boolean value = ((Boolean) g.getAllele());
                if (value) {
                    weights[i] = 1;
                } else {
                    weights[i] = 0;
                }
            }
        }
        instances.setWeights(weights);
        this.distanceMeasure = new ExtendedDistanceMeasure(instances);
        SingleLinkage<Instance> clusterer = new SingleLinkage<Instance>(this.clusterNumber,
                distanceMeasure);
        logger.info("clustering started {}", Thread.currentThread().getName());
        Monitor mon = MonitorFactory.start("clustering");
        try {
           clusters=clusterer.cluster(instances.getInstances());
        } catch (ConvergenceException e) {
            this.precision = 0;
            this.recall = 0;
            MonitorFactory.add("Clustering exception","Exception", 1);
            mon.stop();
            logger.debug("{}", mon);
            logger.info("clustering finished without results");
            return;
        }
        mon.stop();
        logger.debug("{}", mon);
        logger.info("clustering finished");
        Cluster<Instance> cluster = this.evaluateClusters(clusters, instances.getUntrusteds());
        ClusteringResult result;

        List<String> instancesNames = new LinkedList<String>();
        if (cluster != null) {
            for (Instance i : cluster.getPoints()) {
                String name = i.getName();
                if (i.isUntrusted()) {
                    name += ",tp";
                } else {
                    name += ",fp";
                }
                instancesNames.add(name);
            }
        } else {
            instancesNames.add("No result");
        }
        result = new ClusteringResult(chromosome.hashCode(), chromosome.toString(), instancesNames, this.description);
        result.setPrecision(precision);
        result.setRecall(recall);
        if (saveToDB) {
            dao.save(result);
        }
    }

    private Cluster<Instance> evaluateClusters(List<Cluster<Instance>> clusters, int nbUntrusteds) {
        int maxUntrusteds = 0;
        int clusterUntrusteds;
        this.untrustedCluster = null;
        for (int i = 0; i < clusters.size(); i++) {
            clusterUntrusteds = 0;
            for (Instance inst : clusters.get(i).getPoints()) {
                if (inst.isUntrusted()) {
                    clusterUntrusteds++;
                }
            }
            if (clusterUntrusteds > maxUntrusteds) {
                maxUntrusteds = clusterUntrusteds;
                this.untrustedCluster = clusters.get(i);
            }
        }
        if (untrustedCluster != null) {
            this.precision = ((double) maxUntrusteds / (double) untrustedCluster.getPoints().size());
            this.recall = ((double) maxUntrusteds / (double) nbUntrusteds);
        } else {
            this.precision = 0;
            this.recall = 0;
        }
        logger.debug("precision {}", precision);
        logger.debug("recall {}", recall);
        return this.untrustedCluster;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    public int getClusterNumber() {
        return clusterNumber;
    }

    @Required
    public void setClusterNumber(int clusterNumber) {
        this.clusterNumber = clusterNumber;
    }

    public boolean isSaveToDB() {
        return saveToDB;
    }

    public void setSaveToDB(boolean saveToDB) {
        this.saveToDB = saveToDB;
    }

    @Override
    public Cluster<Instance> evaluate(Instance i) throws ClusteringException {
        if (this.distanceMeasure == null || this.clusters == null) {
            throw new ClusteringException("There is no model, please cluster some test data first");
        }
        double minDistance=Double.MAX_VALUE;
        Cluster<Instance> c=null;
        for (Cluster<Instance> cluster : this.clusters) {
            double dist = this.distCluster(i, cluster);
            if(dist<minDistance){
                minDistance=dist;
                c=cluster;
            }
        }
        return c;
    }
    
    private double distCluster(Instance i, Cluster<Instance> cluster){
        double minDist=Double.MAX_VALUE;
        for(Instance other : cluster.getPoints()){
             double dist = this.distanceMeasure.compute(i.getPoint(),other.getPoint());
             if(dist<minDist){
                 minDist=dist;
             }
        }
        return minDist;
    }

    private Object distanceMeasure(double[] point, double[] point2) {
        // TODO Auto-generated method stub
        return null;
    }

    public Cluster<Instance> getUntrustedCluster() {
        return untrustedCluster;
    }

    public ExtendedDistanceMeasure getDistanceMeasure() {
        return distanceMeasure;
    }

    public void setDistanceMeasure(ExtendedDistanceMeasure distanceMeasure) {
        this.distanceMeasure = distanceMeasure;
    }

}
