package ontologymetrics.genetics;

import ontologymetrics.clustering.IClusterer;
import ontologymetrics.core.data.RawData;

import org.jgap.FitnessFunction;
/**
 * Definition for fitness functions.
 * @author d.cherix
 *
 */
public abstract class AbstractFitnessFunction extends FitnessFunction {
    
    private static final long serialVersionUID = -9139368009827706986L;
    protected double[] maxValues;

    /**
     * setter for the {@ling Instancexs} object to use.
     * 
     * @param instances
     */
    public abstract void setInstances(RawData instances);

    /**
     * Setter for the maxvalues.
     * @param maxValues
     */
    public void setMaxValues(double[] maxValues) {
        this.maxValues = maxValues;
    }

}