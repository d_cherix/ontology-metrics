package ontologymetrics.genetics;

import ontologymetrics.clustering.IClusterer;
import ontologymetrics.core.data.RawData;

import org.jgap.IChromosome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * Fitness function based on the \(F_2\)-Measure.</br>
 * \(F_2=5 \cdot \frac{\text{precision}\cdot\text{recall}}{(4\cdot\text{precision})+\text{recall}}\) 
 * @author d.cherix
 *
 */
public class FMeasureFitnessFunction extends AbstractFitnessFunction {


    private static final long serialVersionUID = 5317177578908498441L;

    private static final Logger logger = LoggerFactory.getLogger(FMeasureFitnessFunction.class);

    @Autowired
    private ApplicationContext appContext;
    private String instancesFile;
    private RawData instances;

    @Override
    protected double evaluate(IChromosome chromosome) {
        IClusterer clusterer = (IClusterer) appContext.getBean("clusterer");
        logger.debug("chromosome {}",chromosome);
        try {
            clusterer.cluster(chromosome, instances, maxValues);
        } catch (Exception e) {
            logger.error("No clusters created", e);
            return 0;
        }
        double measure = 0;
        chromosome.setApplicationData(clusterer);
        return (Double.isNaN(measure = this.getFMeasure(clusterer.getPrecision(), clusterer.getRecall(), 2))) ? .0
                : measure;
    }

    public RawData getInstances() {
        return instances;
    }

    /**
     * Calculate an return the F-Measure.</br>
     *  \(F_{\beta}=(1+\beta) \cdot \frac{\text{precision}\cdot\text{recall}}{(\beta^2\cdot\text{precision})+\text{recall}}\) 
     * @param precision the precision
     * @param recall the recall
     * @param beta \(\beta\)
     * @return the \(F_{\beta}\)-Measure.
     */
    public double getFMeasure(double precision, double recall, double beta) {
        return (1 + Math.pow(beta, 2)) * ((precision * recall) / ((Math.pow(beta, 2) * precision) + recall));
    }

    @Override
    public void setInstances(RawData instances) {
        this.instances = instances;
    }
}
