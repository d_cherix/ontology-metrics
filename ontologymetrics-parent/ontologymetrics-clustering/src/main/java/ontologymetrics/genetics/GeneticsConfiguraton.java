package ontologymetrics.genetics;



import org.jgap.Configuration;
import org.jgap.FitnessFunction;
import org.jgap.InvalidConfigurationException;
import org.jgap.audit.PermutingConfiguration;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.impl.DefaultConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;


public class GeneticsConfiguraton {
    
    
    private static final Logger logger = LoggerFactory.getLogger(GeneticsConfiguraton.class);
    
    public enum ConfType{
        DEFAULT, GP,PERMUTING;
    }

    
    private Configuration conf;

    private FitnessFunction fitness;
    private ConfType confType;

   /**
    * To init
    * @throws InvalidConfigurationException
    */
    public void init() throws InvalidConfigurationException{
        switch (confType){
        case DEFAULT:
            conf= new DefaultConfiguration();
            break;
        case GP:
            conf = new GPConfiguration();
            break;
        case PERMUTING:
            conf=new PermutingConfiguration();
            break;
        }
    }

    /**
     * Getter for a {@link Configuration} object
     * @return
     */
    public Configuration getConfiguration(){
        return this.conf;
    }
    
    @Required
    /**
     * Set the type of Configuration to use
     * @param type
     */
    public void setType(ConfType type){
        this.confType=type;
    }
    
    /**
     * Getter for the fitness value
     * @return the fitness value
     */
    public FitnessFunction getFitness() {
        return fitness;
    }

  /**
   * Set the fitness function to use
   * @param fitnessFunction the fitnessfunction to use
   */
    public void setFitness(FitnessFunction fitnessFunction) {
        this.fitness = fitnessFunction;
        try {
            this.conf.setFitnessFunction(fitnessFunction);
        } catch (InvalidConfigurationException e) {
           logger.warn("Configexception",e);
        }
    }
}
