
/**
 * This package contains all the objects for the gentics part of the algorithm
 * @author d.cherix
 *
 */
package ontologymetrics.genetics;