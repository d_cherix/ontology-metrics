package ontologmetrics.configurator;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ontologymetrics.core.Beta;
import ontologymetrics.core.Beta.Cause;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

@Beta(cause=Cause.NOTYETIMPLEMETED)
public class ClusteringConfiguration implements Configuration {

    
    private static final Logger logger = LoggerFactory.getLogger(ClusteringConfiguration.class);
    
    private boolean genetic;
    private int clusterNumber;
    private int maxIterations;
    private String description;
    private boolean saveToDB;
    private boolean chromosomeTest;
    private String arffFilePath;
    private String descChromosome;
    private boolean[] weights;
    private int populationSize;
    private double threshold;
    private Map<String, String> prefixes;

    public boolean genetic() {
        return this.genetic;
    }

    public String clusterNumber() {
        return String.valueOf(this.clusterNumber);
    }

    public String maxIterations() {
        return String.valueOf(this.maxIterations);
    }

    public String description() {
        return this.description;
    }

    public String saveToDB() {
        return String.valueOf(saveToDB);
    }

    public boolean chromosomeTest() {
        return chromosomeTest;
    }

    public String arffFilePath() {
        return this.arffFilePath;
    }

    public String descChromosome() {
        return this.descChromosome;
    }

    public String weights() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < weights.length; i++) {
            builder.append("<value>");
            builder.append(String.valueOf(weights[i]));
            builder.append("<value>");
        }
        return builder.toString();
    }
  
    public String populationSize() {
        return String.valueOf(this.populationSize);
    }

    public String threshold() {
        return String.valueOf(this.threshold);
    }

    public String prefixes() {
        StringBuilder builder = new StringBuilder();
        for (String key : this.prefixes.keySet()) {
            builder.append("<entry key=\"");
            builder.append(key);
            builder.append("\" value=\"");
            builder.append(prefixes.get(key));
            builder.append("\" />");
        }
        return builder.toString();
    }

    @Override
    public void writeTo(String filePath) {
        MustacheFactory mf = new XMLMustacheFactory();
        Mustache mustache = mf.compile("config-clustering.xml");
        try {
            mustache.execute(new FileWriter(filePath), this).flush();
        } catch (IOException e) {
           logger.error("Exception on writing to file");
        }
    }

    @Override
    public void setOptions(Map<String, Object> options) {

    }

    public boolean isGenetic() {
        return genetic;
    }

    public void setGenetic(boolean genetic) {
        this.genetic = genetic;
    }

    public int getClusterNumber() {
        return clusterNumber;
    }

    public void setClusterNumber(int clusterNumber) {
        this.clusterNumber = clusterNumber;
    }

    public int getMaxIterations() {
        return maxIterations;
    }

    public void setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSaveToDB() {
        return saveToDB;
    }

    public void setSaveToDB(boolean saveToDB) {
        this.saveToDB = saveToDB;
    }

    public boolean isChromosomeTest() {
        return chromosomeTest;
    }

    public void setChromosomeTest(boolean chromosomeTest) {
        this.chromosomeTest = chromosomeTest;
    }

    public String getArffFilePath() {
        return arffFilePath;
    }

    public void setArffFilePath(String arffFilePath) {
        this.arffFilePath = arffFilePath;
    }

    public String getDescChromosome() {
        return descChromosome;
    }

    public void setDescChromosome(String descChromosome) {
        this.descChromosome = descChromosome;
    }

    public boolean[] getWeights() {
        return weights;
    }

    public void setWeights(boolean[] weights) {
        this.weights = weights;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(int populationSize) {
        this.populationSize = populationSize;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public Map<String, String> getPrefixes() {
        return prefixes;
    }

    public void setPrefixes(Map<String, String> prefixes) {
        this.prefixes = prefixes;
    }

    public static void main(String[] args) {
        HashMap<String,String> map = new HashMap<String, String>();
        map.put("a", "aa");
        ClusteringConfiguration config = new ClusteringConfiguration();
        config.setArffFilePath("a");
        config.setChromosomeTest(true);
        config.setClusterNumber(10);
        config.setDescChromosome("desc");
        config.setDescription("d");
        config.setGenetic(false);
        config.setMaxIterations(-1);
        config.setPopulationSize(5);
        config.setPrefixes(map);
        config.setSaveToDB(false);
        config.setWeights(new boolean[]{false,true});
        config.writeTo("test");
    }
    
}
