package ontologmetrics.configurator;

import java.util.Map;

import ontologymetrics.core.Beta;
import ontologymetrics.core.Beta.Cause;
/**
 * 
 * @author d.cherix
 *
 */
@Beta(cause=Cause.NOTYETIMPLEMETED)
public interface Configuration {
    public void writeTo(String filePath);
    public void setOptions(Map<String,Object> options);
}
