package ontologmetrics.configurator;

import ontologymetrics.core.Beta;
import ontologymetrics.core.Beta.Cause;

@Beta(cause=Cause.NOTYETIMPLEMETED)
public abstract class ConfigurationFactory<T extends Configuration> { 
    public static<T> T createConfiguration(Class<T> clazz) throws InstantiationException, IllegalAccessException{
        return clazz.newInstance();
    }
}
