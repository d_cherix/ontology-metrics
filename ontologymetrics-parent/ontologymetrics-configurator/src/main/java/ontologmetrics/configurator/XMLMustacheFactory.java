package ontologmetrics.configurator;

import static com.github.mustachejava.util.HtmlEscaper.escape;

import java.io.IOException;
import java.io.Writer;

import ontologymetrics.core.Beta;
import ontologymetrics.core.Beta.Cause;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.MustacheFactory;
@Beta(cause=Cause.NOTYETIMPLEMETED)
public class XMLMustacheFactory extends DefaultMustacheFactory implements MustacheFactory {
    
    
    private static final Logger logger = LoggerFactory.getLogger(XMLMustacheFactory.class);
    
    @Override
    public void encode(String value, Writer writer) {
        try {
            writer.append(value);
        } catch (IOException e) {
            logger.error("exception on wrting mustache",e);
        }
    }
}
