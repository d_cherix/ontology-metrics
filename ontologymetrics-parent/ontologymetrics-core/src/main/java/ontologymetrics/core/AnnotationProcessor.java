package ontologymetrics.core;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

/**
 * An annotation processor to hanlde the annontaions of the project.
 * @author d.cherix
 *
 */
@SupportedAnnotationTypes("com.unister.semweb.quality.ontologymetrics.core.Beta")
public class AnnotationProcessor extends AbstractProcessor {
    
    private ProcessingEnvironment env;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void init(ProcessingEnvironment pe) {
        this.env = pe;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (!roundEnv.processingOver()) {
            for (TypeElement te : annotations) {
                final Set< ? extends Element> elts = roundEnv.getElementsAnnotatedWith(te);
                for (Element elt : elts) {
                    env.getMessager().printMessage(Kind.WARNING,  String.format("%s : thou shalt not hack %s", roundEnv.getRootElements(), elt),
                            elt);
                }
            }
        }
        return true;
    }

}
