package ontologymetrics.core;

import java.util.Map;

import com.google.common.collect.Maps;

public class Config {
    private Map<String,String> configStrings;
    
    public Config(){
        configStrings=Maps.newLinkedHashMap();
    }
    
    public Config(Map<String,String> values){
        this();
        configStrings.putAll(values);
    }
    
    public String getConfigValue(String key){
        return configStrings.get(key);
    }
    
    public void addConfigValues(Map<String,String> values){
        this.configStrings.putAll(values);
    }
    
    public void addConfigValue(String key, String value){
        this.configStrings.put(key, value);
    }
}
