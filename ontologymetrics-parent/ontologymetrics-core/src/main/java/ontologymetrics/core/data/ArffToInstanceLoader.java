package ontologymetrics.core.data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import ontologymetrics.core.data.IAttribute.NullAttribute;
import ontologymetrics.core.data.IAttribute.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class ArffToInstanceLoader implements ILoader {

    private static final Logger logger = LoggerFactory.getLogger(ArffToInstanceLoader.class);
    private double[] maxValues;

    public RawData load(String file) {
        BufferedReader reader = null;
        Gson gson = new Gson();
        RawData instances = new RawData();
        try {
            reader = new BufferedReader(new FileReader(file));
            String line;
            LinkedList<Type> attributesType = new LinkedList<IAttribute.Type>();
            String[] tokens;
            boolean isdata = false;
            maxValues = null;

            while (reader.ready()) {
                line = reader.readLine();
                if (line.startsWith("@attribute")) {
                    tokens = line.split("\\s");
                    Type type = Type.fromArffName(tokens[tokens.length - 1]);
                    if (type == null) {
                        type = Type.NOMINAL;
                    }
                    attributesType.add(type);

                } else if (line.startsWith("@data")) {
                    isdata = true;
                    maxValues = new double[attributesType.size()];
                    logger.debug("Attributes Type {}", attributesType);
                } else if (isdata) {
                    tokens = line.split(",(?=([^\']*\'[^\']*\')*[^\']*$)");
                    Instance instance = new Instance(instances);
                    instance.setName(tokens[0]);
                    IAttribute[] attributes = new IAttribute[attributesType.size() - 2];
                    int pos = 0;
                    for (int i = 1; i < tokens.length; i++) {
                        if (tokens[i].equals("?")) {
                            if (attributesType.get(i) != Type.NOMINAL) {
                                attributes[pos] = new NullAttribute();
                                pos++;
                            }
                            continue;
                        }
                        switch (attributesType.get(i)) {
                        case NUMERIC:
                            try {
                                double value = Double.parseDouble(tokens[i]);
                                attributes[pos] = new NumericAttribute(value);
                                if (value > maxValues[pos]) {
                                    maxValues[pos] = value;
                                }
                                pos++;
                            } catch (NumberFormatException e) {
                                for (String token : tokens) {
                                    logger.debug("token {}", token);
                                }
                                return null;
                            }
                            break;
                        case GEOPOINT:
                            attributes[pos] = gson.fromJson(tokens[i].replace("'", "").replace("\\", ""),
                                    GeoAttribute.class);
                            pos++;
                            break;
                        case SET:
                            attributes[pos] = new SetAttribute(gson.fromJson(tokens[i].replace("'", ""), int[].class));
                            pos++;
                            break;
                        case STRINGSET:
                            pos++;
                            break;
                        case NOMINAL:
                            if (tokens[i].equals("untrusted")) {
                                instance.setUntrusted(true);
                            } else if (tokens[i].equals("trusted")) {
                                instance.setTrusted(true);
                            }
                            break;
                        case NULL:
                            break;
                        default:
                            break;
                        }
                    }
                    instance.setAttributes(attributes);
                    instances.addInstance(instance);
                }

            }
        } catch (FileNotFoundException e) {
            logger.error("Error on reading file {}", file, e);
            return null;
        } catch (IOException e) {
            logger.error("Error on reading file {}", file, e);
            return null;
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                logger.error("Error on closing reader", e);
            }
        }
        logger.debug("Instances {}", instances);
        instances.setMaxValues(maxValues);
        return instances;
    }

    @Override
    public double[] getMaxValues() {
        return maxValues;
    }

    public static void main(String[] args) {
        ArffToInstanceLoader l = new ArffToInstanceLoader();
        RawData i = l.load("/data/d.cherix/master/arffs/untrusted/unister-owl:Hotelcombined.arff");
        System.out.println(i.getInstances().size());
    }
}
