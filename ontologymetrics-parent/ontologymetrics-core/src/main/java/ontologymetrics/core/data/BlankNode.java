package ontologymetrics.core.data;

import lombok.Data;

@Data
public class BlankNode implements IResource {
    private String blankNodeId;
}
