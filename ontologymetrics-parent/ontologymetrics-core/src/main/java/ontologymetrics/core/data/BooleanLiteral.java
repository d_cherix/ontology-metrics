package ontologymetrics.core.data;

public class BooleanLiteral extends LiteralResource<Boolean> {

    public BooleanLiteral() {
        super();
    }

    public BooleanLiteral(String language, String datatype, Boolean value) {
        super(language, datatype, value);
    } 
    
}
