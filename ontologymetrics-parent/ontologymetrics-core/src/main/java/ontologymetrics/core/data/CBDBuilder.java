package ontologymetrics.core.data;

import java.util.List;

public class CBDBuilder {
    
    public static CBDBuilder cbd(){
        return new CBDBuilder();
    }
    
    private final CounciseBoundDescription cbd;
    
    private CBDBuilder(){
        cbd=new CounciseBoundDescription();
    }
    
    public CounciseBoundDescription build(){
        return this.cbd;
    }
    
    public CBDBuilder subject(UriResource subject){
        this.cbd.setSubject(subject);
        return this;
    }
    
    public CBDBuilder datatypeProperties(List<DatatypeProperty> datatypeProperties){
        this.cbd.setDatatypeProperties(datatypeProperties);
        return this;
    }
    
    public CBDBuilder objectProperties(List<ObjectProperty> objectProperties){
        this.cbd.setObjectProperties(objectProperties);
        return this;
    }
}
