package ontologymetrics.core.data;

import java.util.List;
import java.util.Map;

import lombok.Data;


public class CounciseBoundDescription {
    private UriResource subject;
    private List<DatatypeProperty> datatypeProperties;
    private List<ObjectProperty> objectProperties;
    private Map<UriResource,CounciseBoundDescription> descriptions;
    public UriResource getSubject() {
        return subject;
    }
    public void setSubject(UriResource subject) {
        this.subject = subject;
    }
    public List<DatatypeProperty> getDatatypeProperties() {
        return datatypeProperties;
    }
    public void setDatatypeProperties(List<DatatypeProperty> datatypeProperties) {
        this.datatypeProperties = datatypeProperties;
    }
    public List<ObjectProperty> getObjectProperties() {
        return objectProperties;
    }
    public void setObjectProperties(List<ObjectProperty> objectProperties) {
        this.objectProperties = objectProperties;
    }
    public Map<UriResource, CounciseBoundDescription> getDescriptions() {
        return descriptions;
    }
    public void setDescriptions(Map<UriResource, CounciseBoundDescription> descriptions) {
        this.descriptions = descriptions;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((datatypeProperties == null) ? 0 : datatypeProperties.hashCode());
        result = prime * result + ((descriptions == null) ? 0 : descriptions.hashCode());
        result = prime * result + ((objectProperties == null) ? 0 : objectProperties.hashCode());
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CounciseBoundDescription other = (CounciseBoundDescription) obj;
        if (datatypeProperties == null) {
            if (other.datatypeProperties != null)
                return false;
        } else if (!datatypeProperties.equals(other.datatypeProperties))
            return false;
        if (descriptions == null) {
            if (other.descriptions != null)
                return false;
        } else if (!descriptions.equals(other.descriptions))
            return false;
        if (objectProperties == null) {
            if (other.objectProperties != null)
                return false;
        } else if (!objectProperties.equals(other.objectProperties))
            return false;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "CounciseBoundDescription [subject=" + subject + ", datatypeProperties=" + datatypeProperties
                + ", objectProperties=" + objectProperties + ", descriptions=" + descriptions + "]";
    }
    
    
}
