package ontologymetrics.core.data;

import lombok.Data;


public class DatatypeProperty implements IProperty {
    private UriResource subject;
    private UriResource property;
    @SuppressWarnings("rawtypes")
    private LiteralResource literal;
    public UriResource getSubject() {
        return subject;
    }
    public void setSubject(UriResource subject) {
        this.subject = subject;
    }
    public UriResource getProperty() {
        return property;
    }
    public void setProperty(UriResource property) {
        this.property = property;
    }
    public LiteralResource getLiteral() {
        return literal;
    }
    public void setLiteral(LiteralResource literal) {
        this.literal = literal;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((literal == null) ? 0 : literal.hashCode());
        result = prime * result + ((property == null) ? 0 : property.hashCode());
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DatatypeProperty other = (DatatypeProperty) obj;
        if (literal == null) {
            if (other.literal != null)
                return false;
        } else if (!literal.equals(other.literal))
            return false;
        if (property == null) {
            if (other.property != null)
                return false;
        } else if (!property.equals(other.property))
            return false;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "DatatypeProperty [subject=" + subject + ", property=" + property + ", literal=" + literal + "]";
    }
    
    
}
