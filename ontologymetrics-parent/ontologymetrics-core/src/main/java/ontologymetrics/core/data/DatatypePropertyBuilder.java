package ontologymetrics.core.data;

public class DatatypePropertyBuilder {
    private final DatatypeProperty datatypeProperty;

    private DatatypePropertyBuilder() {
        datatypeProperty = new DatatypeProperty();
    }

    public static DatatypePropertyBuilder datatypeProperty() {
        return new DatatypePropertyBuilder();
    }

    public DatatypePropertyBuilder subject(UriResource subject) {
        datatypeProperty.setSubject(subject);
        return this;
    }

    public DatatypePropertyBuilder subject(String subject) {
        return this.subject(UriResourceBuilder.uriResource().uri(subject).build());
    }
    
    public DatatypePropertyBuilder property(UriResource property){
        datatypeProperty.setProperty(property);
        return this;
    }
    
    public DatatypePropertyBuilder property(String property){
        return this.property(UriResourceBuilder.uriResource().uri(property).build());
    }
    
    @SuppressWarnings("rawtypes")
    public DatatypePropertyBuilder literal(LiteralResource literal){
        datatypeProperty.setLiteral(literal);
        return this;
    }
    
    public DatatypeProperty build(){
        return this.datatypeProperty;
    }
}
