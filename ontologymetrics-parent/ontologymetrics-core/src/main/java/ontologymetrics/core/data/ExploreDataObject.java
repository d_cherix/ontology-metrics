package ontologymetrics.core.data;

import java.util.List;
import java.util.Map;

public class ExploreDataObject {
    private String[] instances;
    private List<Integer>[] values;
    private Map<String,Integer> attributes;
    private String desc;
    
    public String[] getInstances() {
        return instances;
    }
    public void setInstances(String[] instances) {
        this.instances = instances;
    }
    public List<Integer>[] getValues() {
        return values;
    }
    public void setValues(List<Integer>[] values) {
        this.values = values;
    }
    public Map<String, Integer> getAttributes() {
        return attributes;
    }
    public void setAttributes(Map<String, Integer> attributes) {
        this.attributes = attributes;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
}
