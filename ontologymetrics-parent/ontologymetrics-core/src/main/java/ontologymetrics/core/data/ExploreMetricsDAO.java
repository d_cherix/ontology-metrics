package ontologymetrics.core.data;

import java.util.List;
import java.util.Map;

public interface ExploreMetricsDAO {

    public abstract void save(String[] instances, List<Integer>[] values, Map<String, Integer> attributes,
            String desc);
    
    public void saveClusterMetric(int metric,double fiveP, double fiftyP,double ninetyfiveP);

}