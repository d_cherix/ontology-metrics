package ontologymetrics.core.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
/**
 * Attribute for geo data.
 * @author d.cherix
 *
 */
public class GeoAttribute implements IAttribute {

    private static final Logger logger = LoggerFactory.getLogger(GeoAttribute.class);

    /**
     * earth radius in meters
     */
    public static final double EARTH_RADIUS_IN_METERS = 6371010;
    /**
     * half of the equator length
     */
    public static final double EQUATORHALFLENGTH = 20037508.5;

    private double latitude;
    private double longitude;

    /**
     * Constructor.
     * 
     * @param latitude
     *            the latitude
     * @param longitude
     *            the longitude
     */
    public GeoAttribute(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public int compareTo(IAttribute attr) {
        return ((Double) (this.distance(attr) * 100)).intValue();
    }

    @Override
    public Type getType() {
        return Type.GEOPOINT;
    }

    /**
     * Calculate the distance to anthoer GeoAttribute in meters.
     * 
     * @param attr
     *            the other GeoAttribute
     * @return the distance in meters.
     */
    public double calculateDistanceInMeters(GeoAttribute attr) {
        double lat1 = latitude;
        double lat2 = attr.getLatitude();
        double lon1 = longitude;
        double lon2 = attr.getLongitude();
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));

        return (EARTH_RADIUS_IN_METERS * c) / EQUATORHALFLENGTH;
    }

    private double distance(IAttribute attr) {

        if (attr instanceof GeoAttribute) {
            return this.calculateDistanceInMeters((GeoAttribute) attr);
        }

        return Double.MAX_VALUE;
    }

    /**
     * Getter
     * 
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Setter
     * 
     * @param latitude
     *            the latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * Getter
     * 
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Setter
     * 
     * @param longitude
     *            the longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "GeoAttribute [latitude=" + latitude + ", longitude=" + longitude + "]";
    }

    @Override
    public double distance(IAttribute attr, double maxValue) {
        if (attr instanceof NullAttribute) {
            return 1;
        }
        return this.distance(attr);
    }

    @Override
    public String getWeka(String nullvalue) {
        Gson gson = new Gson();
        return "'" + gson.toJson(this) + "'";
    }
    
    public static void main(String[] args) {
        GeoAttribute g1 = new GeoAttribute(33.9613, 116.797);
        GeoAttribute g2 = new GeoAttribute(0.0, 116.797);
        System.out.println(g1.distance(g2, 0));
    }

}
