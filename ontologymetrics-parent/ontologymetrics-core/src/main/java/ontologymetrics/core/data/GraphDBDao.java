package ontologymetrics.core.data;

import java.util.Collection;
import java.util.List;

import ontologymetrics.core.exceptions.DBException;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.openrdf.model.Model;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepositoryConnection;

/**
 * DAO for the Neo4J graph.
 * 
 * @author d.cherix
 * 
 */
public interface GraphDBDao {
    /**
     * List all instances from a class.
     * 
     * @param classUri
     *            the uri of the class.
     * @return List of the instances.
     */
    public Collection<UriResource> getInstancesFor(UriResource classUri);

    public void save(Model m, String name) throws DBException;

    public List<LiteralResource> getDatatypeObject(UriResource instance, UriResource property) throws DBException;

    public List<ObjectProperty> listObjectProperties(UriResource instance) throws DBException;

    public List<DatatypeProperty> listDatatypeProperties(UriResource instance) throws DBException;
    
    public List<UriResource> getLinkedUriResources(UriResource instance, UriResource property) throws DBException;

    public List<IResource> getObjects(UriResource subject, UriResource property) throws DBException;

    public List<IProperty> listProperties(UriResource subject) throws DBException;

    public List<UriResource> getType(UriResource subject) throws DBException;

    public List<DatatypeProperty> listDatatypeProperties(UriResource uri, Collection<UriResource> consideredProperties)
            throws DBException;

    public List<IProperty> listProperties(UriResource subject, Collection<UriResource> consideresProperties)
            throws DBException;

    public List<ObjectProperty> listObjectProperties(UriResource instance, Collection<UriResource> consideredProperties)
            throws DBException;

    public TupleQueryResult executeSelectQuery(String sparql, SailRepositoryConnection connection) throws DBException;

    public SailRepositoryConnection connection() throws RepositoryException;

}