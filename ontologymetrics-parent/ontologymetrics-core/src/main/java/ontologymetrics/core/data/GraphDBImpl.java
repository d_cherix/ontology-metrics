package ontologymetrics.core.data;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Setter;
import ontologymetrics.core.data.LiteralBuilder.LiteralType;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.repository.sail.SailRepositoryConnection;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.turtle.TurtleWriter;
import org.openrdf.sail.SailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

/**
 * Implements the {@link GraphDBDao} for neo4j.
 *
 * @author d.cherix
 *
 */
public class GraphDBImpl implements GraphDBDao {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(GraphDBImpl.class);

    public static void main(String... args) throws SailException, RepositoryException, DBException,
            MalformedQueryException, QueryEvaluationException {
        GraphDBImpl g = new GraphDBImpl("/data/d.cherix/master/db/hotel_aktuel_City");


        Map<String, String> prefixesMap = new HashMap<String, String>();
        prefixesMap.put("unister-owl", "http://ontology.unister.de/ontology#");
        prefixesMap.put("dbpedia", "http://dbpedia.org/resource/");
        prefixesMap.put("dbpedia-owl", "http://dbpedia.org/ontology/");
        prefixesMap.put("dbpprop", "http://dbpedia.org/property/");
        prefixesMap.put("foaf", "http://xmlns.com/foaf/0.1/");
        prefixesMap.put("category", "http://dbpedia.org/resource/Category:");
        prefixesMap.put("dcterms", "http://purl.org/dc/terms/");
        prefixesMap.put("lubm", "http://swat.cse.lehigh.edu/onto/univ-bench.owl#");
        SparqlPrefixes prefixes = new SparqlPrefixes(prefixesMap);
        g.setPrefixes(prefixes);
        g.init();
       g.dump();
       g.close();
    }

    private GraphDatabaseService graphDb;
    private String dbPath;
    private ExecutionEngine engine = null;

    @Autowired
    private SparqlPrefixes prefixes;

    private Neo4jGraph graph;

    private GraphSail<Neo4jGraph> sail;

    // private SailRepositoryConnection connection;

    private SailRepository repository;

    public GraphDBImpl(String dbPath) {
        this.dbPath = dbPath;
    }

    public void close() throws RepositoryException, SailException {
        // connection.close();
        sail.shutDown();
        graph.shutdown();
        graphDb.shutdown();
    }

    public void closeConnection() throws DBException {
        // try {
        // // connection.close();
        // } catch (RepositoryException e) {
        // throw new DBException(e);
        // }
    }

    public SailRepositoryConnection connection() throws RepositoryException {
        return repository.getConnection();
    }

    public GraphQueryResult constructQuery(String queryString) throws MalformedQueryException, RepositoryException,
            QueryEvaluationException {
        SailRepositoryConnection connection = repository.getConnection();
        GraphQuery query;
        try {
            query = connection.prepareGraphQuery(QueryLanguage.SPARQL, queryString);
        } finally {
            connection.close();
        }
        return query.evaluate();
    }

    private List<DatatypeProperty> evaluateResultToDatatype(UriResource instance, TupleQueryResult results)
            throws QueryEvaluationException, RepositoryException {
        BindingSet bindings;
        List<DatatypeProperty> dProperties = Lists.newLinkedList();
        while (results.hasNext()) {
            bindings = results.next();
            try {
                LiteralType type = LiteralType.getLiteralType(((Literal) bindings.getBinding("o").getValue())
                        .getDatatype().stringValue());
                LiteralResource literal = LiteralBuilder.literal(type)
                        .value(this.getValue((Literal) bindings.getValue("o"), type)).build();
                dProperties.add(DatatypePropertyBuilder.datatypeProperty().subject(instance)
                        .property(bindings.getValue("p").stringValue()).literal(literal)
                        .build());
            } catch (NullPointerException e) {
                LiteralResource literal = LiteralBuilder.literal(LiteralType.STRING)
                        .value(bindings.getBinding("o").getValue().stringValue()).build();
                dProperties.add(DatatypePropertyBuilder.datatypeProperty().subject(instance)
                        .property(bindings.getValue("p").stringValue()).literal(literal)
                        .build());
            }
        }
        results.close();
        return dProperties;
    }

    private List<ObjectProperty> evaluateResultToObjectProperty(UriResource instance, TupleQueryResult results)
            throws QueryEvaluationException, RepositoryException {
        BindingSet bindings;
        List<ObjectProperty> oProperties = Lists.newLinkedList();
        while (results.hasNext()) {
            bindings = results.next();
            oProperties.add(ObjectPropertyBuilder.objectProperty().subject(instance)
                    .property(bindings.getValue("p").stringValue()).object(bindings.getValue("o").stringValue())
                    .build());
        }
        results.close();
        return oProperties;
    }

    @Override
    public TupleQueryResult executeSelectQuery(String sparql, SailRepositoryConnection connection) throws DBException {
        TupleQuery query;
        TupleQueryResult result = null;
        try {
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL, sparql);
            LOGGER.trace("{}",query);
            result = query.evaluate();
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        }
        return result;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<LiteralResource> getDatatypeObject(UriResource instance,
            UriResource property) throws DBException {
        List<LiteralResource> results = new LinkedList<LiteralResource>();
        TupleQueryResult result = null;
        LOGGER.trace("{} : {}", instance.getUri(), property.getUri());
        SailRepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            TupleQuery query = connection.prepareTupleQuery(
                    QueryLanguage.SPARQL,
                    prefixes.addPrefixesToQuery("SELECT distinct ?o where { " + instance.sparql() + " "
                            + property.sparql() + " ?o "
                            + " FILTER(isLiteral(?o)) }"));
            LOGGER.trace("SELECT distinct ?o where { " + instance.sparql() + " "
                    + property.sparql() + " ?o "
                    + " FILTER(isLiteral(?o)) }");
            result = query.evaluate();
            while (result.hasNext()) {
                BindingSet bindings = result.next();
                Literal literal = (Literal) bindings.getBinding("o").getValue();
                LiteralType type;
                if (literal.getDatatype() == null) {
                    type = LiteralType.STRING;
                } else {
                    type = LiteralType.getLiteralType(literal.getDatatype().stringValue());
                }
                results.add(LiteralBuilder.literal(type).datatype(type.getDatatype())
                        .value(this.getValue(literal, type)).language(literal.getLanguage()).build());
            }
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        } finally {
            try {
                result.close();
                connection.close();
            } catch (RepositoryException e) {
                throw new DBException(e);
            } catch (QueryEvaluationException e) {
                throw new DBException(e);
            }
        }

        return results;
    }

    // private Node getNode(String uri) {
    // Node node;
    // if (indexNodes.get("uri", uri).hasNext()) {
    // node = indexNodes.get("uri", uri).next();
    // LOGGER.trace("node for {} founded", uri);
    // } else {
    // node = graphDb.createNode();
    // node.setProperty("uri", uri);
    // indexNodes.add(node, "uri", uri);
    // LOGGER.trace("node for {} created", uri);
    // }
    // return node;
    // }

    /**
     * @return the dbPath
     */
    public String getDbPath() {
        return dbPath;
    }

    public ExecutionEngine getExecutionEngine() {
        if (engine == null) {
            engine = new ExecutionEngine(graphDb);
        }
        return engine;
    }

    /**
     * @return the graphDb
     */
    public GraphDatabaseService getGraphDb() {
        return graphDb;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.unister.semweb.quality.ontologymetrics.core.data.GraphDBDao#
     * getInstancesFor(java.lang.String)
     */
    @Override
    public Collection<UriResource> getInstancesFor(UriResource classUri) {
        SailRepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
        } catch (RepositoryException e) {
            LOGGER.error("Error by querying instances: {}", e);
        }
        Set<UriResource> results = Sets.newLinkedHashSet();
        TupleQuery query;
        TupleQueryResult result = null;
        try {
            query = connection.prepareTupleQuery(
                    QueryLanguage.SPARQL,
                    prefixes.addPrefixesToQuery("SELECT distinct ?i WHERE {?i a "
                            + classUri.sparql() + "}"));
            result = query.evaluate();
            while (result.hasNext()) {
                results.add(UriResourceBuilder
                        .uriResource()
                        .uri(result.next().getBinding("i").getValue().stringValue())
                        .build());
            }
        } catch (MalformedQueryException e) {
            LOGGER.error("Error by querying instances: {}", e);
        } catch (RepositoryException e) {
            LOGGER.error("Error by querying instances: {}", e);
        } catch (QueryEvaluationException e) {
            LOGGER.error("Error by querying instances: {}", e);
        } finally {
            try {
                result.close();
                connection.close();
            } catch (QueryEvaluationException e) {
                LOGGER.error("Error by querying instances: {}", e);
            } catch (RepositoryException e) {
                LOGGER.error("Error by querying instances: {}", e);
            }
        }
        return ImmutableList.copyOf(results);
    }

    @Override
    public List<UriResource> getLinkedUriResources(UriResource instance,
            UriResource property) throws DBException {
        TupleQuery query;
        List<UriResource> objects = null;
        TupleQueryResult results = null;
        SailRepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    "SELECT distinct ?o WHERE { " + instance.sparql()
                            + " " + property.sparql() + " ?o } FILTER(!isLiteral(?o))");
            objects = new LinkedList<UriResource>();
            results = query.evaluate();
            BindingSet bindings;
            while (results.hasNext()) {
                bindings = results.next();
                Value value = bindings.getValue("o");
                objects.add(UriResourceBuilder.uriResource().uri(bindings.getValue("o").stringValue()).build());
            }
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        } finally {
            try {
                results.close();
                connection.close();
            } catch (QueryEvaluationException e) {
                throw new DBException(e);
            } catch (RepositoryException e) {
                throw new DBException(e);
            }
        }
        return ImmutableList.copyOf(objects);
    }

    @Override
    public List<IResource> getObjects(UriResource subject, UriResource property) throws DBException {
        LinkedList<IResource> results = new LinkedList<IResource>();
        results.addAll(this.getDatatypeObject(subject, property));
        results.addAll(this.getLinkedUriResources(subject, property));
        return ImmutableList.copyOf(results);
    }

    @Override
    public List<UriResource> getType(UriResource subject) throws DBException {
        TupleQuery query;
        List<DatatypeProperty> dProperties = null;
        List<UriResource> types = new LinkedList<UriResource>();
        TupleQueryResult results = null;
        SailRepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    prefixes.addPrefixesToQuery("SELECT distinct ?type WHERE { " + subject.sparql()
                            + " rdf:type ?type }"));
            results = query.evaluate();
            while (results.hasNext()) {
                types.add(UriResourceBuilder.uriResource().uri(results.next().getValue("type").stringValue()).build());
            }
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        } finally {
            try {
                results.close();
                connection.close();
            } catch (QueryEvaluationException e) {
                throw new DBException(e);
            } catch (RepositoryException e) {
                throw new DBException(e);
            }
        }
        return ImmutableList.copyOf(types);
    }

    private Object getValue(Literal literal, LiteralType type) {
        switch (type) {
        case BOOLEAN:
            return Boolean.parseBoolean(literal.stringValue());
        case BYTE:
        case UNSIGNEDBYTE:
            return literal.byteValue();
        case FLOAT:
        case DOUBLE:
            return Double.parseDouble(literal.stringValue());
        case INT:
        case SHORT:
        case INTEGER:
        case UNSIGNEDINT:
        case NEGATIVEINTEGER:
        case POSITIVEINTEGER:
        case NONNEGATIVEINTEGER:
        case NONPOSITIVEINTEGER:
            return Long.parseLong(literal.stringValue());
        case DATETIME:
        case DATE:
        case TIME:
        case GDAY:
        case GMONTHDAY:
        case GMONTH:
        case GYEAR:
        case GYEARMONTH:
            return literal.calendarValue();
        case LONG:
        case UNSIGNEDLONG:
            return Long.parseLong(literal.stringValue());
        case STRING:
        case NORMALIZEDSTRING:
        case TOKEN:
        case NMTOKEN:
            return literal.stringValue();
        default:
            return literal.stringValue();
        }
    }

    public void init() throws SailException, RepositoryException {
        LOGGER.info("loading graph database");
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(dbPath)
                .newGraphDatabase();
        LOGGER.info("Graph database loaded");
        graph = new Neo4jGraph(this.graphDb);
        sail = new GraphSail<Neo4jGraph>(graph);
        sail.initialize();
        repository = new SailRepository(sail);
//        registerShutdownHook(graphDb);
    }

    public void dump() throws DBException{
        TupleQuery query=null;
        SailRepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    "select * where { ?s ?p ?o}");
            TurtleWriter writer = new TurtleWriter(new FileWriter("dbpedia.ttl"));
//            writer.startRDF();
            TupleQueryResult r = query.evaluate();
           while(r.hasNext()){
               System.out.println(r.next());
           }
//           writer.endRDF();

        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (RepositoryException e) {
                throw new DBException(e);
            }

        }
    }

    @Override
    public List<DatatypeProperty> listDatatypeProperties(UriResource instance) throws DBException {
        TupleQuery query;
        List<DatatypeProperty> dProperties = null;
        TupleQueryResult results = null;
        SailRepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    "SELECT distinct ?p ?o WHERE { " + instance.sparql()
                            + " ?p ?o . FILTER(isLiteral(?o)) . }");
            dProperties = this.evaluateResultToDatatype(instance, query.evaluate());

        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        } finally {
            try {
                connection.close();
            } catch (RepositoryException e) {
                throw new DBException(e);
            }

        }
        return ImmutableList.copyOf(dProperties);
    }

    @Override
    public List<DatatypeProperty> listDatatypeProperties(UriResource uri, Collection<UriResource> consideredProperties)
            throws DBException {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        for(int i=0;i<consideredProperties.size();i++){
            builder.append("?o"+i+" ");
        }
        builder.append(" WHERE {");
        int i = 0;
        for (UriResource p : consideredProperties) {
            builder.append(" optional {");
            builder.append(uri.sparql());
            builder.append(p.sparql());
            builder.append(" ?o");
            builder.append(i);
            builder.append(" }");
            i++;
        }
        builder.append(" }");
        System.out.println(builder.toString());
        List<DatatypeProperty> properties = Lists.newLinkedList();
        SailRepositoryConnection connection = null;
        try {
            connection = this.repository.getConnection();
            TupleQueryResult result = this.executeSelectQuery(builder.toString(), connection);
            while (result.hasNext()) {
                BindingSet bindings = result.next();
                i = 0;
                for (UriResource p : consideredProperties) {
                    if (bindings.hasBinding("o" + i)) {
                        try {
                            LiteralType type = LiteralType.getLiteralType(((Literal) bindings.getBinding("o")
                                    .getValue())
                                    .getDatatype().stringValue());
                            LiteralResource literal = LiteralBuilder.literal(type)
                                    .value(this.getValue((Literal) bindings.getValue("o" + i), type)).build();
                            properties.add(DatatypePropertyBuilder.datatypeProperty().subject(uri)
                                    .property(p).literal(literal)
                                    .build());
                        } catch (NullPointerException e) {
                            LiteralResource literal = LiteralBuilder.literal(LiteralType.STRING)
                                    .value(bindings.getBinding("o" + i).getValue().stringValue()).build();
                            properties.add(DatatypePropertyBuilder.datatypeProperty().subject(uri)
                                    .property(p).literal(literal)
                                    .build());
                        }
                    }
                }
            }
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } finally {
            try {
                connection.close();
            } catch (RepositoryException e) {
                throw new DBException(e);
            }
        }
        return properties;
    }

    @Override
    public List<ObjectProperty> listObjectProperties(UriResource instance) throws DBException {
        List<ObjectProperty> oProperties = null;
        SailRepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            String queryString = "SELECT distinct ?p ?o WHERE { " + instance.sparql()
                    + " ?p ?o . FILTER(!isLiteral(?o)) }";

            oProperties = this.evaluateResultToObjectProperty(instance,
                    this.executeSelectQuery(queryString, connection));
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        } finally {
            try {
                connection.close();
            } catch (RepositoryException e) {
                throw new DBException(e);
            }
        }
        return oProperties;
    }

    @Override
    public List<ObjectProperty> listObjectProperties(UriResource instance, Collection<UriResource> consideredProperties)
            throws DBException {
        List<ObjectProperty> oProperties = null;
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ?p ?o WHERE { ");
        builder.append(instance.sparql());
        builder.append(" ?p ?o "
                + "FILTER(?p IN (");
        for (UriResource p : consideredProperties) {
            builder.append(p.sparql());
            builder.append(",");
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.append(") && !isLiteral(?o)) }");
        SailRepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            oProperties = this.evaluateResultToObjectProperty(instance,
                    this.executeSelectQuery(builder.toString(), connection));
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } finally {
            try {
                connection.close();
            } catch (RepositoryException e) {
                throw new DBException(e);
            }
        }
        return oProperties;
    }

    @Override
    public List<IProperty> listProperties(UriResource subject) throws DBException {
        List<IProperty> results = new LinkedList<IProperty>();
        results.addAll(this.listDatatypeProperties(subject));
        results.addAll(this.listObjectProperties(subject));
        return results;
    }

    @Override
    public List<IProperty> listProperties(UriResource subject, Collection<UriResource> consideredProperties)
            throws DBException {
        List<IProperty> results = new LinkedList<IProperty>();
        results.addAll(this.listDatatypeProperties(subject, consideredProperties));
        results.addAll(this.listObjectProperties(subject, consideredProperties));
        return results;
    }

    private void registerShutdownHook(GraphDatabaseService graphDb2) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    sail.shutDown();
                    graph.shutdown();
                    graphDb.shutdown();

                } catch (SailException e) {
                    LOGGER.error("Error on shutding down the database {}", e);
                }
            }
        });
    }

    @Override
    public void save(Model m, String name) throws DBException {
        try {
            SailRepositoryConnection connection = repository.getConnection();
            connection.begin();
            connection.add(m);
            connection.commit();
            connection.close();
        } catch (RepositoryException e) {
            throw new DBException("Exception on saving tuples", e);
        }
    }
    
    public void setPrefixes(SparqlPrefixes prefixes) {
        this.prefixes = prefixes;
    }

}
