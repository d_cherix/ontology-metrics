package ontologymetrics.core.data;
/**
 * This interface defines the principals method for the attributes.
 * @author d.cherix
 *
 */
public interface IAttribute extends Comparable<IAttribute> {
    /**
     * This enum enumeartes all possibles types of attributes.
     * @author d.cherix
     *
     */
    public enum Type {
        /**
         * fopr numeric attributes
         */
        NUMERIC("numeric"),
        /**
         * for geolocation attributes
         */
        GEOPOINT("geopoint"),
        /**
         * for set attributes
         */
        SET("set"),
        /**
         * for string set attributes
         */
        STRINGSET("stringset"),
        /**
         * for nominal attribute
         */
        NOMINAL("nominal"),
        /**
         * for the null attribute
         */
        NULL("?");

        private String arffName;

        Type(String arffName) {
            this.arffName = arffName;
        }
        
        /**
         * To return the extended arff reprensation of the the attribute type.
         * @return the arff representation
         */
        public String getArffName() {
            return this.arffName;
        }

        /**
         * Conver from an arff representation into th enum
         * @param arffName the arff representation of the type.
         * @return the corresponding Type object.
         */
        public static Type fromArffName(String arffName) {
            if (arffName != null) {
                for (Type t : Type.values()) {
                    if (arffName.equals(t.arffName)) {
                        return t;
                    }
                }
            }
            return null;
        }
    }

    /**
     * To get the type of the IAttribute.
     * @return
     */
    public Type getType();
    /**
     * To calculate the distance to another attribute
     * @param attr the other attribute
     * @param maxValue the biggest value for this metric
     * @return the distance
     */
    public double distance(IAttribute attr, double maxValue);
    /**
     * @param nullvalue 
     * @return a weka representation of the attribute.
     */
    public String getWeka(String nullvalue);
    /**
     * Static class to represent an empty attribute. 
     * @author d.cherix
     *
     */
    static class NullAttribute implements IAttribute {

        
        @Override
        public int compareTo(IAttribute o) {
            if (o instanceof NullAttribute) {
                return 0;
            }
            return Integer.MAX_VALUE;
        }

        @Override
        public Type getType() {
            return Type.NULL;
        }

        @Override
        public double distance(IAttribute attr, double maxValue) {
            return 1;
        }

        @Override
        public String getWeka(String nullvalue) {
            return nullvalue;
        }
    }

}
