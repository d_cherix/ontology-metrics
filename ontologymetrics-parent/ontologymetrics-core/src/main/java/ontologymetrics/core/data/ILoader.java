package ontologymetrics.core.data;
/**
 * This interface defines loader to load data into {@link RawData Instances}.
 * @author d.cherix
 *
 */
public interface ILoader {
    /**
     * Load from a file.
     * @param file the path to the file
     * @return
     */
    public RawData load(String file);
    /**
     * Getter
     * @return the max value for each metric.
     */
    public double[] getMaxValues();
}
