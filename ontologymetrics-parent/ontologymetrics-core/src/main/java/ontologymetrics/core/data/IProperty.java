package ontologymetrics.core.data;

public interface IProperty {

    public abstract UriResource getProperty();

    public abstract UriResource getSubject();

}