package ontologymetrics.core.data;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.ml.clustering.Clusterable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * This class represents an ontology instance in a clusterable form.
 * 
 * @see Clusterable
 * @author d.cherix
 * 
 */
public class Instance implements Clusterable {

    private static final Logger logger = LoggerFactory.getLogger(Instance.class);

    private String name;
    private List<Double> coord;
    private RawData instances;
    private boolean trusted = false;
    private boolean untrusted = false;

    /**
     * Constructor. The Constructor need an instances object.
     * The Instances object represent the dataset containing this instance.
     * 
     * @param instances
     *            The Instances object.
     */
    public Instance(RawData instances) {
        this.instances = instances;
        this.coord = Lists.newLinkedList();
    }
    
//    public void open(){
//        this.coord=Lists.newLinkedList(this.coord);
//    }
//    
//    public void close(){
//        this.coord=ImmutableList.copyOf(this.coord);
//    }

    //
    // public double distance(Instance inst, double[] weight,double[] maxValues){
    // double sum=0;
    // for(int i=0; i<attributes.length; i++){
    // double distancePart =
    // weight[i]*Math.pow(Math.abs(attributes[i].distance(inst.getAttributes()[i],maxValues[i])),2.0);
    // logger.debug("distance part {} weight {}",distancePart,weight[i]);
    // sum+=distancePart;
    // }
    // return Math.sqrt(sum);
    // }

    @Override
    public String toString() {
        return "Instance [name=" + name + ", coord=" + (coord) + ", instances=" + instances
                + ", trusted=" + trusted + ", untrusted=" + untrusted + "]";
    }

    /**
     * 
     * @param coord
     */
    public void setCoord(double[] coord) {
        this.coord = new ArrayList<Double>(coord.length);
        for (int i = 0; i < coord.length; i++) {
            this.coord.add(coord[i]);
        }
    }

    public void setAttributes(IAttribute[] attrs) {
        coord = new ArrayList<Double>(attrs.length);
        for (int i = 0; i < attrs.length; i++) {
            coord.add(this.instances.addAttribute(attrs[i]));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public double[] getPoint() {
        double[] point = new double[this.coord.size()];
        for (int i = 0; i < this.coord.size(); i++) {
            point[i] = this.coord.get(i);
        }
        return point;
    }

    public boolean isTrusted() {
        return trusted;
    }

    public void setTrusted(boolean trusted) {
        this.trusted = trusted;
    }

    public boolean isUntrusted() {
        return untrusted;
    }

    public void setUntrusted(boolean untrusted) {
        this.untrusted = untrusted;
    }

    public void addAttribute(IAttribute attribute, int pos) {
        while (this.coord.size() - 1 < pos) {
            this.coord.add(.0);
        }
        this.coord.set(pos, this.instances.addAttribute(attribute));
    }

    public int numbOfAttributes() {
        return this.coord.size();
    }

    public String getWeka(int nbofAttributes) {
        return this.getWeka(nbofAttributes, "?");
    }

    public String getWeka(int nbofAttributes, String nullvalue) {
        StringBuilder builder = new StringBuilder();
        builder.append("'");
        try {
            builder.append(URLEncoder.encode(name, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            builder.append(name);
        }
        builder.append("'");
        for (Double point : this.coord) {
            builder.append(",");
            builder.append(instances.getAttributes().get(point.intValue()).getWeka(nullvalue));
        }
        if (this.coord.size() < nbofAttributes) {
            // to complete the instances whitch don't have the las attributes.
            // the nbofAttributes contains the name.
            // The name is not in the coordinates, so we add 1 to the coord size.
            for (int i = 0; i < (nbofAttributes - this.coord.size()); i++) {
                builder.append(",");
                builder.append(nullvalue);
            }
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        RawData instances = new RawData();
        Instance instance = new Instance(instances);
        instance.setName("test");
        instance.addAttribute(new SetAttribute(new int[] { 1, 2, 3 }), 0);
        instance.addAttribute(new SetAttribute(new int[] { 1, 2 }), 2);
        System.out.println(instance.getWeka(4));
    }
}
