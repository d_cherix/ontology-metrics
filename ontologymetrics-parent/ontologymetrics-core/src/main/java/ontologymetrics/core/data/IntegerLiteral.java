package ontologymetrics.core.data;

public class IntegerLiteral extends LiteralResource<Integer> {

    public IntegerLiteral() {
        super();
    }

    public IntegerLiteral(String language, String datatype, Integer value) {
        super(language, datatype, value);
    }

}
