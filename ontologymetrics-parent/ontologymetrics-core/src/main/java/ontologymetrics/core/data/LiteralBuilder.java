package ontologymetrics.core.data;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class LiteralBuilder {

    public enum LiteralType {
        STRING("http://www.w3.org/2001/XMLSchema#string"),
        BOOLEAN("http://www.w3.org/2001/XMLSchema#boolean"),
        DECIMAL("http://www.w3.org/2001/XMLSchema#decimal"),
        FLOAT("http://www.w3.org/2001/XMLSchema#float"),
        DOUBLE("http://www.w3.org/2001/XMLSchema#double"),
        DATETIME("http://www.w3.org/2001/XMLSchema#dateTime"),
        TIME("http://www.w3.org/2001/XMLSchema#time"),
        DATE("http://www.w3.org/2001/XMLSchema#date"),
        GYEARMONTH("http://www.w3.org/2001/XMLSchema#gYearMonth"),
        GYEAR("http://www.w3.org/2001/XMLSchema#gYear"),
        GMONTHDAY("http://www.w3.org/2001/XMLSchema#gMonthDay"),
        GDAY("http://www.w3.org/2001/XMLSchema#gDay"),
        GMONTH("http://www.w3.org/2001/XMLSchema#gMonth"),
        HEXBINARY("http://www.w3.org/2001/XMLSchema#hexBinary"),
        BASE64BINARY("http://www.w3.org/2001/XMLSchema#base64Binary"),
        ANYURI("http://www.w3.org/2001/XMLSchema#anyURI"),
        NORMALIZEDSTRING("http://www.w3.org/2001/XMLSchema#normalizedString"),
        TOKEN("http://www.w3.org/2001/XMLSchema#token"),
        LANGUAGE("http://www.w3.org/2001/XMLSchema#language"),
        NMTOKEN("http://www.w3.org/2001/XMLSchema#NMTOKEN"),
        NAME("http://www.w3.org/2001/XMLSchema#Name"),
        NCNAME("http://www.w3.org/2001/XMLSchema#NCName"),
        INTEGER("http://www.w3.org/2001/XMLSchema#integer"),
        NONPOSITIVEINTEGER("http://www.w3.org/2001/XMLSchema#nonPositiveInteger"),
        NEGATIVEINTEGER("http://www.w3.org/2001/XMLSchema#negativeInteger"),
        LONG("http://www.w3.org/2001/XMLSchema#long"),
        INT("http://www.w3.org/2001/XMLSchema#int"),
        SHORT("http://www.w3.org/2001/XMLSchema#short"),
        BYTE("http://www.w3.org/2001/XMLSchema#byte"),
        NONNEGATIVEINTEGER("http://www.w3.org/2001/XMLSchema#nonNegativeInteger"),
        UNSIGNEDLONG("http://www.w3.org/2001/XMLSchema#unsignedLong"),
        UNSIGNEDINT("http://www.w3.org/2001/XMLSchema#unsignedInt"),
        UNSIGNEDSHORT("http://www.w3.org/2001/XMLSchema#unsignedShort"),
        UNSIGNEDBYTE("http://www.w3.org/2001/XMLSchema#unsignedByte"),
        POSITIVEINTEGER("http://www.w3.org/2001/XMLSchema#positiveInteger");

        private String datatype;
        private static final Map<String, LiteralType> map;
        static {
            map = new HashMap<String, LiteralType>();
            for (LiteralType t : LiteralType.values()) {
                map.put(t.datatype, t);
            }
        }

        private LiteralType(String datatype) {
            this.datatype = datatype;
        }

        public String getDatatype() {
            return this.datatype;
        }

        public static LiteralType getLiteralType(String datatype) {

            LiteralType type = map.get(datatype);
            if (type == null) {
                type = LiteralType.STRING;
            }
            return type;
        }
    }

    public static LiteralBuilder literal(String datatype) {
        return literal(LiteralType.valueOf(datatype));
    }

    public static LiteralBuilder literal(LiteralType type) {
        return new LiteralBuilder(type);
    }

    private final LiteralResource resource;

    private LiteralBuilder(LiteralType type) {
        switch (type) {
        case BOOLEAN:
            this.resource = new BooleanLiteral();
            break;
        case BYTE:
        case UNSIGNEDBYTE:
            this.resource = new ByteLiteral();
            break;
        case FLOAT:
        case DOUBLE:
        case DECIMAL:
            this.resource = new DoubleLiteral();
            break;
        case INT:
        case INTEGER:
        case UNSIGNEDINT:
        case NEGATIVEINTEGER:
        case POSITIVEINTEGER:
        case NONNEGATIVEINTEGER:
        case NONPOSITIVEINTEGER:
            this.resource = new IntegerLiteral();
            break;
        case DATETIME:
        case DATE:
        case TIME:
        case GDAY:
        case GMONTHDAY:
        case GMONTH:
        case GYEAR:
        case GYEARMONTH:
            this.resource = new XMLGregorianCalendarLiteral();
            break;
        case LONG:
        case UNSIGNEDLONG:
            this.resource = new LongLiteral();
            break;
        case STRING:
        case NORMALIZEDSTRING:
        case TOKEN:
        case NMTOKEN:
            this.resource = new StringLiteral();
            break;
        default:
            this.resource = null;
        }
    }

    public LiteralResource build() {
        return resource;
    }

    public LiteralBuilder language(String language) {
        resource.setLanguage(language);
        return this;
    }

    public LiteralBuilder value(Object value) {
        resource.setValue(value);
        return this;
    }

    public LiteralBuilder value(String value) {
        resource.setValue(value);
        return this;
    }

    public LiteralBuilder datatype(String datatype) {
        this.resource.setDatatype(datatype);
        return this;
    }
}
