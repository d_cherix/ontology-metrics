package ontologymetrics.core.data;



public abstract class LiteralResource<T extends Comparable> implements IResource, Comparable<LiteralResource<T>> {
    private String language;
    private String datatype;
    private T value;
    
    public LiteralResource(){
        
    }

    public LiteralResource(String language, String datatype, T value) {
        this.language = language;
        this.datatype = datatype;
        this.value = value;
    }

    public void setValue(T value) {
        this.value=value;
    }
    
    public void setValue(Object value){
        this.value=(T)value;
    }

    @Override
    public int compareTo(LiteralResource<T> o) {
       return value.compareTo(getValue());
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public T getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((datatype == null) ? 0 : datatype.hashCode());
        result = prime * result + ((language == null) ? 0 : language.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        @SuppressWarnings("rawtypes")
        LiteralResource other = (LiteralResource) obj;
        if (datatype == null) {
            if (other.datatype != null)
                return false;
        } else if (!datatype.equals(other.datatype))
            return false;
        if (language == null) {
            if (other.language != null)
                return false;
        } else if (!language.equals(other.language))
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }    
    
    
    
}
