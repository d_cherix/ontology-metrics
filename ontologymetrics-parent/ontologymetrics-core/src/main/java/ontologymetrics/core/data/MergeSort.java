package ontologymetrics.core.data;

import java.util.Comparator;
@Deprecated
public class MergeSort<T> {
    private static final int TMP_SIZE = 1024;
    private static final int MINBLOCKSIZE = 16;
    private T[] data;
    private Comparator<T> comp;
    private T[] tmp;

    public void sortArray(T[] data, Comparator<T> comp) {
        this.data = data;
        this.comp = comp;
        int length = Math.max((int) (100 * Math.log(data.length)), TMP_SIZE);
        length = Math.min(data.length, length);
        this.tmp = (T[]) new Object[length];
        mergeSort(0, data.length - 1);
    }

    private void mergeSort(int low, int high) {
        if (high - low < MINBLOCKSIZE) {
            binaryInsertionSort(low, high);
            return;
        }
        int m=(low+high)>>>1;
        mergeSort(low,m);
        mergeSort(m+1,high);
        merge(low,m+1,high);
    }

    private void binaryInsertionSort(int low, int high) {
        for (int i = low + 1; i <= high; i++) {
            T x = data[i];
            int ins = binarySearch(x, low, i - 1);
            for (int j = i - 1; j >= ins; j--) {
                data[j + 1] = data[j];
            }
            data[ins] = x;
        }
    }

    private int binarySearch(T x, int low, int high) {
        while (low <= high) {
            int m = (low + high) >>> 1;
            if (comp.compare(x, data[m]) >= 0) {
                low = m + 1;
            } else {
                high = m - 1;
            }
        }
        return low;
    }
    
    private void merge(int low, int m, int high) {
        int length1=m-low, length2=high-m+1;
        if(length1==0||length2==0){
            return;
        }
        if(length1+length2==2){
            if(this.comp.compare(this.data[m], this.data[low])<0){
                swap(data,m,low);
            }
            return;
        }
        if(length1<=this.tmp.length){
            System.arraycopy(data, low, this.tmp, 0, length1);
            mergeSmall(data,low,this.tmp,0,length1-1,data,m,high);
        } else if(length2 <=tmp.length){
            System.arraycopy(data, m,this.tmp , 0, length2);
            System.arraycopy(this.data, low, this.data, high-length1+1, length1);
            mergeSmall(this.data, low, this.data, high-length1+1, high, this.tmp, 0, length2-1);
        }
        mergeBig(low,m, high);
    }

    private void mergeBig(int low, int m, int high) {
        int len1 = m - low, len2 = high - m + 1;
        int firstCut, secondCut, newSecond;
        if (len1 > len2) {
            firstCut = low + len1 / 2;
            secondCut = findLower(data[firstCut], m, high);
            int len = secondCut - m;
            newSecond = firstCut + len;
        } else {
            int len = len2 / 2;
            secondCut = m + len;
            firstCut = findUpper(data[secondCut], low, m - 1);
            newSecond = firstCut + len;
        }
        swapBlocks(firstCut, m, secondCut - 1);
        merge(low, firstCut, newSecond - 1);
        merge(newSecond, secondCut, high);
    }

    private void swapBlocks(int low, int m, int high) {
        int len1 = m - low, len2 = high - m + 1;
        if (len1 == 0 || len2 == 0) {
            return;
        }
        if (len1 < this.tmp.length) {
            System.arraycopy(this.data, low, this.tmp, 0, len1);
            System.arraycopy(data, m, data, low, len2);
            System.arraycopy(tmp, 0, data, low + len2, len1);
            return;
        } else if (len2 < tmp.length) {
            System.arraycopy(data, m, tmp, 0, len2);
            System.arraycopy(data, low, data, low + len2, len1);
            System.arraycopy(tmp, 0, data, low, len2);
            return;
        }
        reverseBlock(low, m - 1);
        reverseBlock(m, high);
        reverseBlock(low, high);
    }

    private void reverseBlock(int low, int high) {
        while (low < high) {
            T old = data[low];
            data[low++] = data[high];
            data[high--] = old;
        }
    }

    private int findUpper(T x, int low, int high) {
        int len = high - low + 1, half;
        while (len > 0) {
            half = len / 2;
            int m = low + half;
            if (comp.compare(data[m], x) <= 0) {
                low = m + 1;
                len = len - half - 1;
            } else {
                len = half;
            }
        }
        return low;
    }

    private int findLower(T x, int low, int high) {
        int len = high - low + 1, half;
        while (len > 0) {
            half = len / 2;
            int m = low + half;
            if (comp.compare(data[m], x) < 0) {
                low = m + 1;
                len = len - half - 1;
            } else {
                len = half;
            }
        }
        return low;
    }

    private void mergeSmall(T[] target, int pos, T[] source1, int low1, int high1, T[] source2, int low2, int high2) {
       T x1 = source1[low1], x2=source2[low2];
       while(true){
           if(this.comp.compare(x1, x2)<=0){
               target[pos++]=x1;
               if(++low1>high1){
                   System.arraycopy(source2, low2, target, pos, high2-low2+1);
                   break;
               }
               x1=source1[low1];
           } else {
               target[pos++]=x2;
               if(++low2>high2){
                   System.arraycopy(source1, low2, target, pos, high1-low1+1);
                   break;
               }
               x2=source2[low2];
           }
       }
    }

    private void swap(T[] d, int a, int b) {
        T t = d[a];
        d[a] = d[b];
        d[b] = t;
    }
}
