package ontologymetrics.core.data;

import java.util.Map;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;

public class MyNeo4jGraph extends Neo4jGraph {

    private long numberOfItems = 0;
    private long maxNumberOfItems = 1;

    public MyNeo4jGraph(final String directory, long maxNumberOfItems) {
        super(directory, null);
        this.maxNumberOfItems = maxNumberOfItems;
    }

    public MyNeo4jGraph(final String directory, final Map<String, String> configuration, long maxNumberOfItems) {
        super(directory, configuration);
        this.maxNumberOfItems = maxNumberOfItems;
    }

    public Vertex addVertex(final Object id) {
        Vertex vertex = super.addVertex(id);
        commitIfRequired();
        return vertex;
    }

    public Edge addEdge(final Object id, final Vertex outVertex, final Vertex inVertex, final String label) {
        Edge edge = super.addEdge(id, outVertex, inVertex, label);
        commitIfRequired();
        return edge;
    }

    private void commitIfRequired() {
        // Check whether commit should be executed
        if (++numberOfItems % maxNumberOfItems == 0) {
            // Stop the transaction
            commit();
            // Immediately start a new one
            autoStartTransaction();
        }
    }

}