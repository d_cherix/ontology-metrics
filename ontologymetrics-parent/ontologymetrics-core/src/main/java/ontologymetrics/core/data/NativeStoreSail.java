package ontologymetrics.core.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Setter;
import ontologymetrics.core.data.LiteralBuilder.LiteralType;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.repository.sail.SailRepositoryConnection;
import org.openrdf.sail.SailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Sets;
import com.google.gson.annotations.Until;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

/**
 * Implements the {@link GraphDBDao} for neo4j.
 *
 * @author d.cherix
 *
 */
@Deprecated
public class NativeStoreSail implements GraphDBDao {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(NativeStoreSail.class);

    private GraphDatabaseService graphDb;
    private String dbPath;
    private ExecutionEngine engine = null;
    @Autowired
    private SparqlPrefixes prefixes;

    private OrientGraph graph;

    private GraphSail<OrientGraph> sail;

    private SailRepositoryConnection connection;

    public NativeStoreSail(String dbPath) {
        this.dbPath = dbPath;
    }

    public void init() throws SailException, RepositoryException {
        LOGGER.info("loading graph database");
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(dbPath)
                .newGraphDatabase();
        LOGGER.info("Graph database loaded");
        graph = new OrientGraph("local:"+dbPath);
        sail = new GraphSail<OrientGraph>(graph);
        sail.initialize();
        connection = new SailRepository(sail).getConnection();
        registerShutdownHook(graphDb);
    }

    public void close() throws RepositoryException, SailException {
        connection.close();
        graph.shutdown();
        graphDb.shutdown();
    }

    private void registerShutdownHook(GraphDatabaseService graphDb2) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    connection.close();
                    sail.shutDown();
                    graph.shutdown();
                    graphDb.shutdown();
                } catch (RepositoryException e) {
                    LOGGER.error("Error on shutding down the database {}", e);
                } catch (SailException e) {
                    LOGGER.error("Error on shutding down the database {}", e);
                }
            }
        });
    }

    public ExecutionEngine getExecutionEngine() {
        if (engine == null) {
            engine = new ExecutionEngine(graphDb);
        }
        return engine;
    }

    /**
     * @return the graphDb
     */
    public GraphDatabaseService getGraphDb() {
        return graphDb;
    }

    /**
     * @return the dbPath
     */
    public String getDbPath() {
        return dbPath;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.unister.semweb.quality.ontologymetrics.core.data.GraphDBDao#
     * getInstancesFor(java.lang.String)
     */
    @Override
    public Collection<UriResource> getInstancesFor(UriResource classUri) {
        Set<UriResource> results = Sets.newLinkedHashSet();
        TupleQuery query;
        try {
            query = connection.prepareTupleQuery(
                    QueryLanguage.SPARQL,
                    prefixes.addPrefixesToQuery("SELECT distinct ?i WHERE {?i a "
                            + classUri.sparql() + "}"));
            TupleQueryResult result = query.evaluate();
            while (result.hasNext()) {
                results.add(UriResourceBuilder
                        .uriResource()
                        .uri(result.next().getBinding("i").getValue().stringValue())
                        .build());
            }
        } catch (MalformedQueryException e) {
            LOGGER.error("Error by querying instances: {}", e);
        } catch (RepositoryException e) {
            LOGGER.error("Error by querying instances: {}", e);
        } catch (QueryEvaluationException e) {
            LOGGER.error("Error by querying instances: {}", e);
        }

        return results;
    }

    @Override
    public void save(Model m, String name) throws DBException {
        try {
            connection.begin();
            connection.add(m);
            connection.commit();
        } catch (RepositoryException e) {
            throw new DBException("Exception on saving tuples", e);
        }
    }

    // private Node getNode(String uri) {
    // Node node;
    // if (indexNodes.get("uri", uri).hasNext()) {
    // node = indexNodes.get("uri", uri).next();
    // LOGGER.trace("node for {} founded", uri);
    // } else {
    // node = graphDb.createNode();
    // node.setProperty("uri", uri);
    // indexNodes.add(node, "uri", uri);
    // LOGGER.trace("node for {} created", uri);
    // }
    // return node;
    // }

    @SuppressWarnings("rawtypes")
    @Override
    public List<LiteralResource> getDatatypeObject(UriResource instance,
            UriResource property) throws DBException {
        List<LiteralResource> results = new LinkedList<LiteralResource>();
        try {
            TupleQuery query = connection.prepareTupleQuery(
                    QueryLanguage.SPARQL,
                    prefixes.addPrefixesToQuery("SELECT ?object where { " + instance.sparql() + " "
                            + property.sparql() + " ?o }"
                            + " FILTER(isLiteral(?o))"));
            TupleQueryResult result = query.evaluate();
            while (result.hasNext()) {
                Literal literal = ((Literal) result.next().getValue("o"));
                LiteralType type = LiteralType.getLiteralType(literal.getDatatype().stringValue());
                results.add(LiteralBuilder.literal(type).datatype(literal.getDatatype().stringValue())
                        .value(this.getValue(literal, type)).language(literal.getLanguage()).build());
            }
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        }

        return results;
    }

    private Object getValue(Literal literal, LiteralType type) {
        switch (type) {
        case BOOLEAN:
            return literal.booleanValue();
        case BYTE:
        case UNSIGNEDBYTE:
            return literal.byteValue();
        case FLOAT:
        case DOUBLE:
            return literal.doubleValue();
        case INT:
        case SHORT:
        case INTEGER:
        case UNSIGNEDINT:
        case NEGATIVEINTEGER:
        case POSITIVEINTEGER:
        case NONNEGATIVEINTEGER:
        case NONPOSITIVEINTEGER:
            return literal.intValue();
        case DATETIME:
        case DATE:
        case TIME:
        case GDAY:
        case GMONTHDAY:
        case GMONTH:
        case GYEAR:
        case GYEARMONTH:
            return literal.calendarValue();
        case LONG:
        case UNSIGNEDLONG:
            return literal.longValue();
        case STRING:
        case NORMALIZEDSTRING:
        case TOKEN:
        case NMTOKEN:
            return literal.stringValue();
        default:
            return literal.stringValue();
        }
    }

    @Override
    public List<ObjectProperty> listObjectProperties(UriResource instance) throws DBException {
        TupleQuery query;
        List<ObjectProperty> oProperties = null;
        try {
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL, "SELECT ?p ?o WHERE { " + instance.sparql()
                    + " ?p ?o FILTER(!isLiteral(?o)) }");
            oProperties = new LinkedList<ObjectProperty>();
            TupleQueryResult results = query.evaluate();
            BindingSet bindings;
            while (results.hasNext()) {
                bindings = results.next();
                oProperties.add(ObjectPropertyBuilder.objectProperty().subject(instance)
                        .property(bindings.getValue("p").stringValue()).object(bindings.getValue("o").stringValue())
                        .build());
            }
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        }
        return oProperties;
    }

    @Override
    public List<DatatypeProperty> listDatatypeProperties(UriResource instance) throws DBException {
        TupleQuery query;
        List<DatatypeProperty> dProperties = null;
        try {
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    "SELECT distinct ?p ?o WHERE { " + instance.sparql()
                            + " ?p ?o . FILTER(isLiteral(?o)) . }");
            dProperties = new LinkedList<DatatypeProperty>();
            TupleQueryResult results = query.evaluate();
            BindingSet bindings;
            while (results.hasNext()) {
                bindings = results.next();
                try {
                LiteralType type = LiteralType.getLiteralType(((Literal) bindings.getBinding("o").getValue())
                        .getDatatype().stringValue());
                LiteralResource literal = LiteralBuilder.literal(type)
                        .value(this.getValue((Literal) bindings.getValue("o"), type)).build();
                dProperties.add(DatatypePropertyBuilder.datatypeProperty().subject(instance)
                        .property(bindings.getValue("p").stringValue()).literal(literal)
                        .build());
                } catch (NullPointerException e){
                    LiteralResource literal = LiteralBuilder.literal(LiteralType.STRING)
                            .value(bindings.getBinding("o").getValue().stringValue()).build();
                    dProperties.add(DatatypePropertyBuilder.datatypeProperty().subject(instance)
                            .property(bindings.getValue("p").stringValue()).literal(literal)
                            .build());
                }
            }
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        }
        return dProperties;
    }

    @Override
    public List<IProperty> listProperties(UriResource subject) throws DBException {
        List<IProperty> results = new LinkedList<IProperty>();
        results.addAll(this.listDatatypeProperties(subject));
        results.addAll(this.listObjectProperties(subject));
        return results;
    }

    @Override
    public List<UriResource> getType(UriResource subject) throws DBException {
        TupleQuery query;
        List<DatatypeProperty> dProperties = null;
        List<UriResource> types = new LinkedList<UriResource>();
        try {
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    prefixes.addPrefixesToQuery("SELECT distinct ?type WHERE { " + subject.sparql()
                            + " rdf:type ?type }"));
            TupleQueryResult results = query.evaluate();
            while (results.hasNext()) {
                types.add(UriResourceBuilder.uriResource().uri(results.next().getValue("type").stringValue()).build());
            }
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        }
        return types;
    }

    @Override
    public List<UriResource> getLinkedUriResources(UriResource instance,
            UriResource property) throws DBException {
        TupleQuery query;
        List<UriResource> objects = null;
        try {
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    "SELECT distinct ?o WHERE { " + instance.sparql()
                            + " " + property.sparql() + " ?o } FILTER(!isLiteral(?o))");
            objects = new LinkedList<UriResource>();
            TupleQueryResult results = query.evaluate();
            BindingSet bindings;
            while (results.hasNext()) {
                bindings = results.next();
                Value value = bindings.getValue("o");
                objects.add(UriResourceBuilder.uriResource().uri(bindings.getValue("o").stringValue()).build());
            }
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        }
        return objects;
    }

    @Override
    public List<IResource> getObjects(UriResource subject, UriResource property) throws DBException {
        LinkedList<IResource> results = new LinkedList<IResource>();
        results.addAll(this.getDatatypeObject(subject, property));
        results.addAll(this.getLinkedUriResources(subject, property));
        return results;
    }


    public TupleQueryResult executeSelectQuery(String sparql) throws DBException {
        TupleQuery query;
        try {
            query = connection.prepareTupleQuery(QueryLanguage.SPARQL, sparql);
            return query.evaluate();
        } catch (MalformedQueryException e) {
            throw new DBException(e);
        } catch (RepositoryException e) {
            throw new DBException(e);
        } catch (QueryEvaluationException e) {
            throw new DBException(e);
        }
    }

    public static void main(String... args) throws SailException, RepositoryException, DBException {
        NativeStoreSail g = new NativeStoreSail("/data/d.cherix/master/db/hotel_aktuel_test");
        try {

            Map<String, String> prefixesMap = new HashMap<String, String>();
            prefixesMap.put("unister-owl", "http://ontology.unister.de/ontology#");
            prefixesMap.put("dbpedia", "http://dbpedia.org/resource/");
            prefixesMap.put("dbpedia-owl", "http://dbpedia.org/ontology/");
            prefixesMap.put("dbpprop", "http://dbpedia.org/property/");
            prefixesMap.put("foaf", "http://xmlns.com/foaf/0.1/");
            prefixesMap.put("category", "http://dbpedia.org/resource/Category:");
            prefixesMap.put("dcterms", "http://purl.org/dc/terms/");
            prefixesMap.put("lubm", "http://swat.cse.lehigh.edu/onto/univ-bench.owl#");
            SparqlPrefixes prefixes = new SparqlPrefixes(prefixesMap);
            g.setPrefixes(prefixes);
            g.init();
            Collection<UriResource> result = g.getInstancesFor(UriResourceBuilder.uriResource()
                    .uri("http://ontology.unister.de/ontology#Hotel").build());
            System.out.println(result.size());
            List dp = g.listDatatypeProperties(result.iterator().next());
            System.out.println(dp);
        } finally {
            g.close();
        }
    }

    @Override
    public List<DatatypeProperty> listDatatypeProperties(UriResource uri, Collection<UriResource> consideredProperties)
            throws DBException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<IProperty> listProperties(UriResource subject, Collection<UriResource> consideresProperties)
            throws DBException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ObjectProperty> listObjectProperties(UriResource instance, Collection<UriResource> consideredProperties)
            throws DBException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TupleQueryResult executeSelectQuery(String sparql, SailRepositoryConnection connection) throws DBException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public SailRepositoryConnection connection() throws RepositoryException {
        // TODO Auto-generated method stub
        return null;
    }

    public void setPrefixes(SparqlPrefixes prefixes) {
        this.prefixes = prefixes;
    }
    

}
