package ontologymetrics.core.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

public class NumericAttribute implements IAttribute {

    
    private static final Logger logger = LoggerFactory.getLogger(NumericAttribute.class);
    
    private double value;

    public NumericAttribute(int value) {
        this.value = value;
    }

    public NumericAttribute(double value) {
        this.value = value;
    }

    @Override
    public int compareTo(IAttribute attr) {
        if (attr.getType() == Type.NUMERIC) {
            return ((Double) value).compareTo(((NumericAttribute) attr).getValue());
        }
        return Integer.MAX_VALUE;
    }

    @Override
    public Type getType() {
        return Type.NUMERIC;
    }

    @Override
    public double distance(IAttribute attr, double maxValue) {
        if (attr instanceof NullAttribute){
            return 1;
        }
        if (attr.getType() == Type.NUMERIC) {
            return ((value/maxValue) - (((NumericAttribute) attr).getValue()/maxValue));
        }
        return Double.MAX_VALUE;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "NumericAttribute [value=" + value + "]";
    }

    @Override
    public String getWeka(String nullvalue) {
       return String.valueOf(value);
    }

}
