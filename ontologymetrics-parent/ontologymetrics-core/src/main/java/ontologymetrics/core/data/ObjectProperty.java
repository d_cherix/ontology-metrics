package ontologymetrics.core.data;



public class ObjectProperty implements IProperty{
    private UriResource subject;
    private UriResource property;
    private UriResource object;
    
    public UriResource getSubject() {
        return subject;
    }
    public void setSubject(UriResource subject) {
        this.subject = subject;
    }
    public UriResource getProperty() {
        return property;
    }
    public void setProperty(UriResource property) {
        this.property = property;
    }
    public UriResource getObject() {
        return object;
    }
    public void setObject(UriResource object) {
        this.object = object;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((object == null) ? 0 : object.hashCode());
        result = prime * result + ((property == null) ? 0 : property.hashCode());
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ObjectProperty other = (ObjectProperty) obj;
        if (object == null) {
            if (other.object != null)
                return false;
        } else if (!object.equals(other.object))
            return false;
        if (property == null) {
            if (other.property != null)
                return false;
        } else if (!property.equals(other.property))
            return false;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "ObjectProperty [subject=" + subject + ", property=" + property + ", object=" + object + "]";
    }
    
    
}
