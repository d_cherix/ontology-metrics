package ontologymetrics.core.data;

public class ObjectPropertyBuilder {
    private final ObjectProperty objectProperty;
    
    private ObjectPropertyBuilder(){
        objectProperty=new ObjectProperty();
    }
    
    public static ObjectPropertyBuilder objectProperty(){
        return new ObjectPropertyBuilder();
    }
    
    public ObjectPropertyBuilder property(UriResource property){
        objectProperty.setProperty(property);
        return this;
    }
    
    public ObjectPropertyBuilder property(String property){
        return this.property(UriResourceBuilder.uriResource().uri(property).build());
    }
    
    public ObjectPropertyBuilder subject(UriResource subject){
        objectProperty.setSubject(subject);
        return this;
    }
    
    public ObjectPropertyBuilder subject(String subject){
        return this.subject(UriResourceBuilder.uriResource().uri(subject).build());
    }
    
    public ObjectPropertyBuilder object(UriResource object){
        objectProperty.setObject(object);
        return this;
    }
    
    public ObjectPropertyBuilder object(String object){
        return this.object(UriResourceBuilder.uriResource().uri(object).build());
    }
    
    public ObjectProperty build(){
        return this.objectProperty;
    }
}
