package ontologymetrics.core.data;

import ontologymetrics.core.Config;
import ontologymetrics.core.exceptions.PipelineObjectException;
import ontologymetrics.core.stages.ArffFileManager;
import weka.core.Instances;

public interface PipelineObject {

    public enum Type {
        RAWDATA(RawData.class), CONFIG(Config.class), ARFFFILEMANAGER(ArffFileManager.class), 
        WEKAINSTANCES(Instances.class);
        private Class className;

        private Type(Class className) {
            this.className = className;
        }

        public Class getClassName() {
            return this.className;
        }
    }

    public String getStringValue(String key);
    public String addKeyValue(String key, String value);
    public Object getData(Type type);

    public void addData(Type type, Object object) throws PipelineObjectException;
    
    public void removeData(Type type);
}
