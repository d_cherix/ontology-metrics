package ontologymetrics.core.data;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ontologymetrics.core.Config;
import ontologymetrics.core.exceptions.PipelineObjectException;

import com.google.common.collect.Maps;

public class PipelineObjectImpl implements PipelineObject {

    
    private static final Logger logger = LoggerFactory.getLogger(PipelineObjectImpl.class);
    private Map<Type, Object> data;
    private Map<String,String> keyValue;

    public PipelineObjectImpl() {
        data = Maps.newLinkedHashMap();
        keyValue = Maps.newLinkedHashMap();
    }

    @Override
    public Object getData(Type type) {
        return data.get(type);
    }

    @Override
    public void addData(Type type, Object object) throws PipelineObjectException {
        Class clazz = type.getClassName();
        if (!object.getClass().equals(clazz)) {
            throw new PipelineObjectException("Object of type " + object.getClass() + " doesn't correspond to type "
                    + type);
        }
        this.data.put(type, object);
    }
    
    public void setConfig(Config config){
        try {
            this.addData(Type.CONFIG, config);
        } catch (PipelineObjectException e) {
            logger.error("Error by setting config object",e);
        }
    }

    @Override
    public void removeData(Type type) {
        this.data.remove(type);
    }

    @Override
    public String getStringValue(String key) {
        return keyValue.get(key);
    }

    @Override
    public String addKeyValue(String key, String value) {
       return this.keyValue.put(key, value);
    }

}
