package ontologymetrics.core.data;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * This class represnt a dataset containing instances with metrics (attributes) and values.
 * 
 * @author d.cherix
 * 
 */
public class RawData {
    private List<Instance> instances;
    private List<IAttribute> attributes;
    private double[] weights;
    private double[] maxValues;
    private int untrusteds = 0;

    public RawData() {
        this.instances = Lists.newLinkedList();
        this.attributes = Lists.newLinkedList();
        this.attributes.add(new IAttribute.NullAttribute());
    }

    @Override
    public String toString() {
        return "Instances [instances=" + instances.size() + ", attributes=" + attributes.size() + "untrusteds="
                + untrusteds + "]";
    }
    
    public void setAttributtesImmutable(){
        this.attributes=ImmutableList.copyOf(this.attributes);
    }
    
    public void setInstancesImmutable(){
        this.instances=ImmutableList.copyOf(instances);
    }

    /**
     * Getter.
     * 
     * @return
     */
    public List<Instance> getInstances() {
        return instances;
    }

    /**
     * Setter
     * 
     * @param instances
     */
    public void setInstances(List<Instance> instances) {
        this.instances = instances;
    }

    /**
     * To add an instance to this dataset.
     * 
     * @param inst
     *            the instance to add.
     */
    public void addInstance(Instance inst) {
        this.instances.add(inst);
        if (inst.isUntrusted()) {
            untrusteds++;
        }
    }

    /**
     * Getter
     * 
     * @return
     */
    public List<IAttribute> getAttributes() {
        return attributes;
    }

    /**
     * to add an attribute.
     * 
     * @param attr
     *            the attribute to add.
     * @return the position in the attribute list
     */
    public double addAttribute(IAttribute attr) {
        int index = this.attributes.indexOf(attr);
        if (index == -1) {
            this.attributes.add(attr);
            index = this.attributes.size() - 1;
        }
        return index;
    }

    /**
     * Setter
     * 
     * @param attributes
     */
    public void setAttributes(List<IAttribute> attributes) {
        this.attributes = attributes;
    }

    /**
     * Getter
     * 
     * @return the weights for each attribute.
     */
    public double[] getWeights() {
        return weights;
    }

    /**
     * Setter
     * 
     * @param weights
     */
    public void setWeights(double[] weights) {
        this.weights = weights;
    }

    /**
     * Getter.
     * 
     * @return an array of the maxvalue for each attribute,
     */
    public double[] getMaxValues() {
        return maxValues;
    }

    /**
     * setter.
     * 
     * @param maxValues
     */
    public void setMaxValues(double[] maxValues) {
        this.maxValues = maxValues;
    }

    /**
     * Getter
     * 
     * @return the number of untrusteds instances.
     */
    public int getUntrusteds() {
        return untrusteds;
    }
}
