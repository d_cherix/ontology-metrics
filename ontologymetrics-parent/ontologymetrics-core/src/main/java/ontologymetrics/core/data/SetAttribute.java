package ontologymetrics.core.data;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetAttribute implements IAttribute {

    private static final Logger logger = LoggerFactory.getLogger(SetAttribute.class);

    private int[] value;

    public SetAttribute(int[] value) {
        this.value = value;
    }

    public SetAttribute(Object[] value) {
        this.value = new int[value.length];
        for (int i = 0; i < value.length; i++) {
            this.value[i] = value[i].hashCode();
        }
    }

    @Override
    public int compareTo(IAttribute o) {
        if (o instanceof SetAttribute) {
            SetAttribute attr = (SetAttribute) o;
            if (value.length > attr.getValue().length) {
                return ((Double) (-10 * jaccardIndex(value, attr.getValue()))).intValue();
            }
            return ((Double) (10 * jaccardIndex(value, attr.getValue()))).intValue();
        }
        return Integer.MAX_VALUE;
    }

    @Override
    public Type getType() {
        return Type.SET;
    }

    @Override
    public double distance(IAttribute attr, double maxValue) {
        if (attr instanceof NullAttribute) {
            return 1;
        }
        if (attr instanceof SetAttribute) {
            return jaccardIndex(value, ((SetAttribute) attr).getValue());
        }
        return Double.MAX_VALUE;
    }

    public static double jaccardIndex(int[] set1, int[] set2) {
        ArrayList<Integer> list1 = new ArrayList<Integer>(set1.length);
        for (int i : set1) {
            list1.add(i);
        }
        ArrayList<Integer> list2 = new ArrayList<Integer>(set2.length);
        for (int i : set2) {
            list2.add(i);
        }
        double intersection = (double) CollectionUtils.intersection(list1, list2).size();
        double union = (double) CollectionUtils.union(list1, list2).size();
        return ((union - intersection) / union);
    }

    public int[] getValue() {
        return value;
    }

    public void setValue(int[] value) {
        this.value = value;
    }

    @Override
    public String getWeka(String nullvalue) {
        return "'" + Arrays.toString(this.value) + "'";
    }

    @Override
    public String toString() {
        return "SetAttribute [value=" + Arrays.toString(value) + "]";
    }

    public static void main(String[] args) {
        int[] set1 = new int[] { 1699800107, 1699800105, -1746283057, 1699800192, 1699800200, 1154195636, -1746283058,
                -1746283054, 1699800253, 1154196504, 1699800228 };
        int[] set2 = new int[] { 1154196527, 1154195572, -1746283056, 1699800102, 1699800101, 1154195538, 1154195666,
                1699800129, 1699800164, 1154195789, 1699800131, 1154195633, -1746283055, 1699800192, 1699800165,
                1154196653, 1154196712, 1154195537, 1699800255, 1154196497, 1154196622, 1699800321, 1699800107,
                1699800228, -1746283057, 1154196623, 1699800138, -1746283054, 1154195659, 1699800200, 1154196526,
                1154195814 };
        SetAttribute a1 = new SetAttribute(set1);
        SetAttribute a2 = new SetAttribute(set2);

        System.out.println(a1.distance(a2, 0));
    }

}
