package ontologymetrics.core.data;

import java.util.ArrayList;
import java.util.List;

public class SymmetricMatrix<T extends Comparable> {
    private List<T> data;
    private int dimension;
    
    public SymmetricMatrix(int dimension){
        this.dimension=dimension;
        data = new ArrayList<T>();
    }
    
    public void add(int row, int col, T value){
        int pos;
        if(row<=col){
            pos=row * this.dimension -(row-1)*row/2+col-row;
        } else {
            pos = col*this.dimension-(col-1)*col/2+row-col;
        }
        while(pos>data.size()-1){
            data.add(null);
        }
        data.set(pos, value);
    }
    
    public T get(int row, int col){
        int pos;
        if(row<=col){
            pos=row * this.dimension -(row-1)*row/2+col-row;
        } else {
            pos = col*this.dimension-(col-1)*col/2+row-col;
        }
        return data.get(pos);
    }
    
    private int[] fromArrayToCoordinates(int pos){
        int row=0;
        int keyafter;
        do{
            row++;
            keyafter= row*this.dimension-(row-1)*row/2;
        } while(pos>=keyafter);
        row--;
       int col=this.dimension-keyafter+pos;
       return new int[]{row, col};
    }
    
    public int[] getSmallest(){
        int smallest = 0;
       for(int i=1; i<data.size();i++){
           if(data.get(i).compareTo(data.get(smallest))>0){
               smallest=1;
           }
       }
        return this.fromArrayToCoordinates(smallest);
    }
}
