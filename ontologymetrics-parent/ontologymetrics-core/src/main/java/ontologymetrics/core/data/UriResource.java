package ontologymetrics.core.data;

import lombok.Data;


public class UriResource implements IResource, Comparable<UriResource> {
    private String uri;

    public String sparql() {
        return "<" + uri + ">";
    }

    @Override
    public int compareTo(UriResource other) {
        return this.uri.compareTo(other.getUri());
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uri == null) ? 0 : uri.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UriResource other = (UriResource) obj;
        if (uri == null) {
            if (other.uri != null)
                return false;
        } else if (!uri.equals(other.uri))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "UriResource [uri=" + uri + "]";
    }

}
