/**
 * 
 */
package ontologymetrics.core.data;

import ontologymetrics.core.sparql.SparqlPrefixes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author didier
 *
 */

public class UriResourceBuilder {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(UriResourceBuilder.class);

	@Autowired
	private SparqlPrefixes prefixes;
	
	public static UriResourceBuilder uriResource(){
		return new UriResourceBuilder();
	}
	
	private final UriResource uriResource;
	
	private UriResourceBuilder(){
		uriResource=new UriResource();
	}
	
	public UriResourceBuilder uri(String uri){
		uriResource.setUri(uri);
		return this;
	}
	
	public UriResource build(){
		return uriResource;
	}
}
