package ontologymetrics.core.data;

import java.util.GregorianCalendar;

import javax.xml.datatype.XMLGregorianCalendar;

public class XMLGregorianCalendarLiteral extends LiteralResource<GregorianCalendar> {

    public XMLGregorianCalendarLiteral() {
        super();
    }

    public XMLGregorianCalendarLiteral(String language, String datatype, GregorianCalendar value) {
        super(language, datatype, value);
    }
    
    public XMLGregorianCalendarLiteral(String language, String datatype, XMLGregorianCalendar value) {
        super(language, datatype, value.toGregorianCalendar());
    }
    

}
