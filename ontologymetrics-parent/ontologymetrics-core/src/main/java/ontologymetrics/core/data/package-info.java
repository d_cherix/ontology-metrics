
/**
 * This package contains all data types needed in more than one module.
 * @author d.cherix
 *
 */
package ontologymetrics.core.data;