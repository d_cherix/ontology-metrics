/**
 * 
 */
package ontologymetrics.core.exceptions;

/**
 * @author didier
 *
 */
public class DBException extends Exception {

	/**
	 * 
	 */
	public DBException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public DBException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public DBException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public DBException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
}
