package ontologymetrics.core.exceptions;

public class GraphException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * {@link Exception#Exception()}
     */
	public GraphException() {
		super();
	}

	/**
     * {@link Exception#Exception(String, Throwable)}
     */
	public GraphException(String message, Throwable cause) {
		super(message, cause);
	}
	/**
     * {@link Exception#Exception(String)}
     */
	public GraphException(String message) {
		super(message);
	}

	/**
     * {@link Exception#Exception(Throwable)}
     */
	public GraphException(Throwable cause) {
		super(cause);
	}

}
