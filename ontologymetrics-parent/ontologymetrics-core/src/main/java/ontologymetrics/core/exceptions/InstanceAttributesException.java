package ontologymetrics.core.exceptions;

public class InstanceAttributesException extends Exception {
    /**
     * {@link Exception#Exception()}
     */
    public InstanceAttributesException() {
        super();
    }

    /**
     * {@link Exception#Exception(String, Throwable)}
     */
    public InstanceAttributesException(String message, Throwable cause) {
        super(message, cause);
    }
    /**
     * {@link Exception#Exception(String)}
     */
    public InstanceAttributesException(String message) {
        super(message);
    }

    /**
     * {@link Exception#Exception(Throwable)}
     */
    public InstanceAttributesException(Throwable cause) {
        super(cause);
    }

}
