package ontologymetrics.core.exceptions;

public class PipelineObjectException extends Exception {

    public PipelineObjectException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public PipelineObjectException(String arg0, Throwable arg1) {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    }

    public PipelineObjectException(String arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    public PipelineObjectException(Throwable arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

}
