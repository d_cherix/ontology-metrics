/**
 * 
 */
package ontologymetrics.core.exceptions;

/**
 * This exception should be use when some prerequisites for a stage aren't provided.
 * @author didier
 *
 */
public class StageInitException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9055324437333863980L;

	/**
     * {@link Exception#Exception()}
     */
	public StageInitException(){
	    super();
	}
	
	/**
	 * {@link Exception#Exception(String, Throwable)}
	 */
	public StageInitException(String message, Throwable throwable) {
		super(message, throwable);
	}

	/**
	 * {@link Exception#Exception(String)}
	 */
	public StageInitException(String message) {
		super(message);
	}

	/**
	{@link Exception#Exception(Throwable)}
	 */
	public StageInitException(Throwable throwable) {
		super(throwable);
	}
	
}
