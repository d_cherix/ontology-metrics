package ontologymetrics.core.exceptions;

public class StageProcessingException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 3571105208998780767L;

    public StageProcessingException() {
        super();
    }

    public StageProcessingException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public StageProcessingException(String arg0) {
        super(arg0);
    }

    public StageProcessingException(Throwable arg0) {
        super(arg0);
    }

}
