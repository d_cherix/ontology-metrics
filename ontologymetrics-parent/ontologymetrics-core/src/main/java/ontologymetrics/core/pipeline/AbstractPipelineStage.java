/**
 * 
 */
package ontologymetrics.core.pipeline;

import ontologymetrics.core.data.PipelineObject;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.StageProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * Define an abstarct class for all pipeline stages.
 * 
 * @author didier
 * 
 */
public abstract class AbstractPipelineStage implements PipelineStage {

    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory
            .getLogger(AbstractPipelineStage.class);

    protected String name="";
    protected PipelineStageStatus status = PipelineStageStatus.AVALAIBLE;

    /**
     * Proof if all prerequisites are fullfilled
     * 
     * @return
     * @throws StageInitException
     */
    protected abstract boolean prerequisitesFulfilled() throws StageInitException;

    /**
     * Process this pipeline stage
     */
    protected abstract void process() throws StageProcessingException;

    /**
     * Post processing work.
     */
    protected abstract void finalise();

    protected PipelineObject data;

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.core.pipeline.PipelineStage#init()
     */
    @Override
    @Required
    public void init(PipelineObject data) throws StageInitException {
        this.data=data;
        if (name.isEmpty()) {
            logger.warn("Every stage of the pipeline needs a name");
            name=this.getClass().getName()+"-"+this.hashCode();
        }
        if (prerequisitesFulfilled()) {
            this.status = PipelineStageStatus.READY;
        } else {
            throw new StageInitException();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.core.pipeline.PipelineStage#resolveStage()
     */
    @Override
    public void resolveStage() throws Exception{
        this.status = PipelineStageStatus.PROCESSING;
        this.process();
        this.finalise();
        this.status = PipelineStageStatus.FINISHED;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.core.pipeline.PipelineStage#getStatus()
     */
    @Override
    public PipelineStageStatus getStatus() {
        return this.status;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.core.pipeline.PipelineStage#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.core.pipeline.PipelineStage#setName(java.lang.String)
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }
}
