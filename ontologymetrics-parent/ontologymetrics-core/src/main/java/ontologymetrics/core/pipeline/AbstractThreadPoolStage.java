package ontologymetrics.core.pipeline;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import ontologymetrics.core.data.PipelineObject;
import ontologymetrics.core.exceptions.StageInitException;

import org.springframework.beans.factory.annotation.Required;

public  abstract class AbstractThreadPoolStage extends AbstractPipelineStage {
	protected ThreadPoolExecutor pool=null;
	protected BlockingQueue<Runnable> queue=null;
	protected int maxSize=4;
	protected long timeout=1000;
	protected TimeUnit unit=TimeUnit.SECONDS;
	protected int minSize=2;
	protected int capacity=10;
	
	@Override
	@Required
	public void init(PipelineObject data) throws StageInitException{
	    this.data=data;
		if(queue==null){
			queue= new ArrayBlockingQueue<Runnable>(capacity);
		}
		pool=new ThreadPoolExecutor(minSize, maxSize, timeout, unit, queue);
			if(prerequisitesFulfilled()){
				this.status=PipelineStageStatus.READY;
			}
	}

	/* (non-Javadoc)
	 * @see com.unister.semweb.quality.ontologymetrics.core.pipeline.AbstractPipelineStage#finalise()
	 */
	@Override
	protected void finalise() {
		pool.shutdown();	
	}

	/**
	 * @param pool the pool to set
	 */
	public void setPool(ThreadPoolExecutor pool) {
		this.pool = pool;
	}

	/**
	 * @param queue the queue to set
	 */
	public void setQueue(BlockingQueue<Runnable> queue) {
		this.queue = queue;
	}

	/**
	 * @param maxSize the maxSize to set
	 */
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	/**
	 * @param unit the unit to set
	 */
	public void setUnit(TimeUnit unit) {
		this.unit = unit;
	}

	/**
	 * @param minSize the minSize to set
	 */
	public void setMinSize(int minSize) {
		this.minSize = minSize;
	}

	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	
}
