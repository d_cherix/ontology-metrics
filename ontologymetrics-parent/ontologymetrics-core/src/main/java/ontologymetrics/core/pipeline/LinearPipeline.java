package ontologymetrics.core.pipeline;

import ontologymetrics.core.data.PipelineObject;
import ontologymetrics.core.exceptions.StageInitException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jamonapi.proxy.MonProxyFactory;

public class LinearPipeline implements PipelineInterface {

	Logger logger = LoggerFactory.getLogger(LinearPipeline.class);

	private PipelineStage[] stages;
    @Autowired
    private PipelineObject pipelineObject;

	public LinearPipeline(int stagesNumber) {
		stages = new PipelineStage[stagesNumber];
	}

	@Override
	public void process() throws Exception {

		for (int i = 0; i < stages.length; i++) {
			@SuppressWarnings("unchecked")
			PipelineStage stage=(PipelineStage)MonProxyFactory.monitor(stages[i]);
			logger.info("Initiliazing stage: {} {} of {} stages", new Object[] {
					stages[i].getName(), i + 1, stages.length });
			try {
			stage.init(pipelineObject);
			stage.resolveStage();
			} catch(StageInitException e){
				logger.error("Stage {} has encoured a component init exception",stages[i].getName(), e);
			}
			logger.info("Stage: {} {} of {} stages has finished", new Object[] {
					stages[i].getName(), i + 1, stages.length });
		}
	}

	@Override
	public PipelineStage [] getStages() {
		return this.stages;
	}

	@Override
	public void setStages(PipelineStage[] stages) {
		this.stages = stages;

	}

}
