/**
 * 
 */
package ontologymetrics.core.pipeline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author d.cherix
 * 
 */
public interface PipelineInterface {


	public void process() throws Exception;

	/**
	 * @return the stages
	 */
	public PipelineStage[] getStages();

	/**
	 * @param stages the stages to set
	 */
	public void setStages(PipelineStage[] stages) ;
		
}
