package ontologymetrics.core.pipeline;

import ontologymetrics.core.data.PipelineObject;
import ontologymetrics.core.exceptions.StageInitException;

import org.springframework.beans.factory.annotation.Required;
/**
 * This interface defines the needed methods for a pipeline stage.
 * @author d.cherix
 *
 * @param <I>
 * @param <E>
 */
public interface PipelineStage {

	@Required
	/**
	 * To init the stage
	 * @throws ComponentInitException
	 */
	public abstract void init(PipelineObject object) throws StageInitException;

	/**
	 * To solve the task of this stage.
	 * @return The output
	 */
	public abstract void resolveStage() throws Exception;

	/**
	 * Getter for the status of this stage.
	 * @return the status of the stage
	 */
	public abstract PipelineStageStatus getStatus();

	/**
	 * Getter for the name of the stage.
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * To set a name to a stage.
	 * @param name the name to set
	 */
	public abstract void setName(String name);

}