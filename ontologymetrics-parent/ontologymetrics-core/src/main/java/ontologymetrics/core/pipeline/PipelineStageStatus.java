/**
 * 
 */
package ontologymetrics.core.pipeline;

/**
 * @author didier
 *
 */
public enum PipelineStageStatus {
READY, FINISHED, PROCESSING, AVALAIBLE;
}
