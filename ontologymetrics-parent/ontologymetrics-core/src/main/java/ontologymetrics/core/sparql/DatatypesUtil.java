package ontologymetrics.core.sparql;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

public class DatatypesUtil {
    
    @Autowired
    private SparqlPrefixes prefixes;
    
    public enum Category{
        Number, String, Date, Binary, None;
        
        public static EnumSet<Category> nonEmpty(){
            return EnumSet.of(Category.Number, Category.Binary, Category.Date, Category.Date);
        }
    }
    
    private Map<Category,List<String>> datatypes;
    
    public DatatypesUtil(){
        datatypes= new HashMap<DatatypesUtil.Category, List<String>>();
 
    }
    
    public List<String> getCategory(Category category){
        return datatypes.get(category);
    }
    
    public Category getCategoryFor(String uri){
        for(Category c: Category.nonEmpty()){
            if(datatypes.get(c).contains(prefixes.expand(uri))){
                return c;
            }
        }
        return Category.None;
    }
    
    public void init(){
        this.initNumbers();
        this.initStrings();
        this.initDate();
        this.initBinary();
    }
    
    private void initNumbers(){
        List<String> numbers = new ArrayList<String>(14);
        numbers.add(prefixes.expand("xsd:decimal"));
        numbers.add(prefixes.expand("xsd:float"));
        numbers.add(prefixes.expand("xsd:double"));
        numbers.add(prefixes.expand("xsd:integer"));
        numbers.add(prefixes.expand("xsd:nonPositiveInteger"));
        numbers.add(prefixes.expand("xsd:negativeInteger"));
        numbers.add(prefixes.expand("xsd:long"));
        numbers.add(prefixes.expand("xsd:int"));
        numbers.add(prefixes.expand("xsd:short"));
        numbers.add(prefixes.expand("xsd:nonNegativeInteger"));
        numbers.add(prefixes.expand("xsd:unsignedLong"));
        numbers.add(prefixes.expand("xsd:unsignedInt"));
        numbers.add(prefixes.expand("xsd:unsignedShort"));
        numbers.add(prefixes.expand("xsd:positiveInteger"));
        datatypes.put(Category.Number, numbers);
    }
    
    private void initStrings(){
        List<String> strings = new ArrayList<String>(8);
        strings.add(prefixes.expand("xsd:string"));
        strings.add(prefixes.expand("xsd:anyURI"));
        strings.add(prefixes.expand("xsd:normalizedString"));
        strings.add(prefixes.expand("xsd:token"));
        strings.add(prefixes.expand("xsd:language"));
        strings.add(prefixes.expand("xsd:NMTOKEN"));
        strings.add(prefixes.expand("xsd:Name"));
        strings.add(prefixes.expand("xsd:NCName"));
        datatypes.put(Category.String, strings);
    }
    
    private void initDate(){
        List<String> dates = new ArrayList<String>(8);
        dates.add(prefixes.expand("xsd:dateTime"));
        dates.add(prefixes.expand("xsd:time"));
        dates.add(prefixes.expand("xsd:date"));
        dates.add(prefixes.expand("xsd:gYearMonth"));
        dates.add(prefixes.expand("xsd:gYear"));
        dates.add(prefixes.expand("xsd:gMonthDay"));
        dates.add(prefixes.expand("xsd:gDay"));
        dates.add(prefixes.expand("xsd:gMonth"));
        datatypes.put(Category.Date, dates);
    }
    
    private void initBinary(){
        List<String> binaries = new ArrayList<String>(4);
        binaries.add(prefixes.expand("xsd:hexBinary"));
        binaries.add(prefixes.expand("xsd:base64Binary"));
        binaries.add(prefixes.expand("xsd:byte"));
        binaries.add(prefixes.expand("xsd:unsignedByte"));
        datatypes.put(Category.Binary, binaries);
    }
    
    
}
