package ontologymetrics.core.sparql;

import org.openrdf.model.Model;
import org.openrdf.query.TupleQueryResult;

// TODO: Auto-generated Javadoc
/**
 * The Interface QueryExecutor.
 */
public interface QueryExecutor {
	
	/**
	 * Execute construct query.
	 *
	 * @param query the query
	 * @param graph the graph
	 * @return the model
	 * @throws Exception 
	 */
	public Model executeConstructQuery(String query, String graph);
	
	/**
	 * Execute select query.
	 *
	 * @param query the query
	 * @param graph the graph
	 * @return the result set
	 */
	public TupleQueryResult executeSelectQuery(String query, String graph);
	
	/**
	 * Execute ask query.
	 *
	 * @param query the query
	 * @param graph the graph
	 * @return true, if successful
	 */
	public boolean executeAskQuery(String query, String graph);
}
