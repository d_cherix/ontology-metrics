package ontologymetrics.core.sparql;

import java.util.Map;

import ontologymetrics.core.data.UriResource;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * Wrapper for the Jena PrefixMapping class.
 * 
 * @author d.cherix
 * 
 */
public class SparqlPrefixes {
	private BiMap<String, String> mapping;

	/**
	 * Constructor
	 * 
	 * @param prefixes
	 *            a list of own prefixes to add.
	 */
	public SparqlPrefixes(Map<String, String> prefixes) {
		mapping = HashBiMap.create();
		mapping.put("dc", "http://purl.org/dc/elements/1.1/");
		mapping.put("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		mapping.put("eg", "http://www.example.org/");
		mapping.put("owl", "http://www.w3.org/2002/07/owl#");
		mapping.put("xsd", "http://www.w3.org/2001/XMLSchema#");
		mapping.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		mapping.put("rss", "http://purl.org/rss/1.0/");
		mapping.put("vcard", "http://www.w3.org/2001/vcard-rdf/3.0#");
		mapping.put("ja", "http://jena.hpl.hp.com/2005/11/Assembler#");
		mapping.putAll(prefixes);
	}
	
	@Deprecated
	public SparqlPrefixes getPrefixes(){
	    return this;
	}
	
	@Deprecated
	public String expandPrefix(String uri){
	    return this.expand(uri);
	}

	public String listPrefixes() {
		StringBuilder builder = new StringBuilder();
		for (String key : mapping.keySet()) {
			builder.append("PREFIX ");
			builder.append(key);
			builder.append(": <");
			builder.append(mapping.get(key));
			builder.append(">\n");
		}
		return builder.toString();
	}
	
	public Map<String,String> prefixesMap(){
	    return this.mapping;
	}

	public String shortForm(String uri) {
		for (String value : mapping.values()) {
			if (uri.startsWith(value)) {
				return uri.replace(value, mapping.inverse().get(value) + ":");
			}
		}
		return uri;
	}
	
	public String shortForm(UriResource uri) {
	    return this.shortForm(uri.getUri());
	}

	public String expand(String shortedUri) {
		if (shortedUri.startsWith("http")) {
			return shortedUri;
		}
		String[] parts = shortedUri.split(":");
		if (parts == null) {
			return shortedUri;
		}
		String longform = mapping.get(parts[0]);
		if (longform == null) {
			return shortedUri;
		}
		return longform + parts[1];
	}
	
	public String addPrefixesToQuery(String query){
		StringBuilder builder = new StringBuilder();
		for (String key : mapping.keySet()) {
			builder.append("PREFIX ");
			builder.append(key);
			builder.append(": <");
			builder.append(mapping.get(key));
			builder.append(">\n");
		}
		builder.append(query);
		return builder.toString();
	}
}
