
/**
 * This package contains the SPARQL classes that are needed in other submodules than the SPARQL one.
 * @author d.cherix
 *
 */
package ontologymetrics.core.sparql;