package ontologymetrics.core.stages;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;

import lombok.Cleanup;
import ontologymetrics.core.data.GeoAttribute;
import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.data.IAttribute;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.NumericAttribute;
import ontologymetrics.core.data.RawData;
import ontologymetrics.core.data.SetAttribute;
import ontologymetrics.core.data.UriResource;
import ontologymetrics.core.data.UriResourceBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import weka.core.Instances;

import com.google.common.collect.Lists;

public class ArffFileManager {

    private static final Logger logger = LoggerFactory
            .getLogger(ArffFileManager.class);

    private List<String> attributesName;
    private boolean balanced = false;
//    @Getter
//    private Map<String, Instance> values;

    private RawData instances;
    @Autowired
    private GraphDBDao graphdB;
   
    private String name;
    
    private String tmpFile=null;

    public int addAttribute(String name, IAttribute.Type type) {
        String attrName = name + " " + type.getArffName();
        if (!this.attributesName.contains(attrName)) {
            this.attributesName.add(attrName);
            return this.attributesName.size() - 1;
        } else {
            return this.attributesName.indexOf(attrName);
        }
        
    }

    public void addValue(Instance uri, double latitude, double longitude, int pos) {
        IAttribute attr = new GeoAttribute(latitude, longitude);
        this.addValue(uri, attr, pos);
    }

    public void addValue(Instance uri, double value, int pos) {
        IAttribute attr = new NumericAttribute(value);
        this.addValue(uri, attr, pos);
    }

//    public void addValue(UriResource uri, double value, int pos) {
//        this.addValue(uri.getUri(), value, pos);
//    }

    private void addValue(Instance inst, IAttribute attr, int pos) {
        inst.addAttribute(attr, pos);
        this.balanced = false;
        logger.trace("Add value at position {} numb of values {}", pos, inst.numbOfAttributes());
    }

    public void addValue(Instance uri, Object[] value, int pos) {
        IAttribute attr = new SetAttribute(value);
        this.addValue(uri, attr, pos);
    }

//    public void addValue(UriResource uri, Object[] value, int pos) {
//        this.addValue(uri, value, pos);
//    }

    public void addValue(Instance uri, String[] value, int pos) {
        IAttribute attr = new SetAttribute(value);
        this.addValue(uri, attr, pos);
    }

    public String getWeka() {
        return this.getWeka("?");
    }

    public String getWeka(String nullValue) {
        StringBuilder builder = new StringBuilder();
        builder.append("@relation ");
        builder.append(this.name);
        builder.append("\n\n");
        builder.append("@attribute ID numeric\n");
        builder.append("@attribute name string\n");
        for (int i = 0; i < attributesName.size(); i++) {
            builder.append("@attribute ");
            builder.append(attributesName.get(i));
            builder.append("\n");

        }
        builder.append("\n");
        builder.append("@data\n");
        for (int i = 0; i < instances.getInstances().size(); i++) {
            builder.append(String.valueOf(i));
            builder.append(",");
            if (nullValue == null) {
                builder.append(instances.getInstances().get(i).getWeka(this.attributesName.size()));
            } else {
                builder.append(instances.getInstances().get(i).getWeka(this.attributesName.size(), nullValue));
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    public void init(String classUri) {
        logger.debug("Creating manager for {}", classUri);
        this.name = classUri;
        this.attributesName = Lists.newLinkedList();
        instances = new RawData();
//        this.values = new HashMap<String, Instance>();
        for (Iterator<UriResource> it = graphdB.getInstancesFor(UriResourceBuilder.uriResource().uri(classUri).build()).iterator(); it
                .hasNext();) {
            UriResource uri = it.next();
            Instance instance = new Instance(instances);
            instances.addInstance(instance);
            instance.setName(uri.getUri());
//            values.put(uri.getUri(), instance);
        }
        this.instances.setInstancesImmutable();
    }

    public Instances getWekaInstances() {
        BufferedReader reader = new BufferedReader(new StringReader(this.getWeka()));
        Instances instancesTmp = null;
        try {
            instancesTmp = new Instances(reader);
        } catch (IOException e) {
            logger.debug("Error on writing instances", e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                logger.debug("Error on writing instances", e);
            }
        }
        return instancesTmp;
    }

    public void writeToFile(String filePath) {
        this.writeToFile(filePath, null);
    }
    

    public void writeToFile(String filePath, String nullValue) {
        try {
            @Cleanup
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.append("@relation ");
            writer.append(this.name);
            writer.append("\n\n");
            writer.append("@attribute ID numeric\n");
            writer.append("@attribute name string\n");
            for (int i = 0; i < attributesName.size(); i++) {
                writer.append("@attribute ");
                writer.append(attributesName.get(i));
                writer.append("\n");
            }
            writer.append("\n");
            writer.flush();
            writer.append("@data\n");
            for (int i = 0; i < instances.getInstances().size(); i++) {
                writer.append(String.valueOf(i));
                writer.append(",");
                if (nullValue == null) {
                    writer.append(instances.getInstances().get(i).getWeka(this.attributesName.size()));
                } else {
                    writer.append(instances.getInstances().get(i).getWeka(this.attributesName.size(), nullValue));
                }
                writer.append("\n");
                writer.flush();
            }
        } catch (IOException e) {
            logger.error("Error on writing arrffile", e);
        }
    }

    public RawData getInstances() {
        return instances;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGraphdB(GraphDBDao graphdB) {
        this.graphdB = graphdB;
    }

    public List<String> getAttributesName() {
        return attributesName;
    }

}
