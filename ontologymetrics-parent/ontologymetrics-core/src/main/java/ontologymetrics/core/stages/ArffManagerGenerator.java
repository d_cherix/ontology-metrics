package ontologymetrics.core.stages;

import lombok.Setter;
import ontologymetrics.core.Config;
import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.PipelineObjectException;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ArffManagerGenerator extends
		AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(ArffManagerGenerator.class);

	@Autowired
	private GraphDBDao graphdB;
	@Autowired
	private SparqlPrefixes prefixes;
	@Setter
	private String[] owlClasses;

	@Override
	protected boolean prerequisitesFulfilled() throws StageInitException {
//		Config config = (Config)this.data.getData(Type.CONFIG);
//		String configValue=config.getConfigValue(this.name+".listOfowlclasses");
//		if((configValue==null || configValue.isEmpty())){
//		    throw new StageInitException("Missing config option: "+this.name+".listOfowlclasses");
//		}
//		
//		owlClasses=configValue.split(";");
		return true;
	}

	@Override
	protected void process() {
		ArffFileManager manager = new ArffFileManager();
		for (int i=0; i<owlClasses.length;i++) {
		    manager = new ArffFileManager();
		    manager.setName(prefixes.shortForm(owlClasses[i]));
		    manager.setGraphdB(graphdB);
		    manager.init(prefixes.getPrefixes().expandPrefix(owlClasses[i]));
		}
		try {
            this.data.addData(Type.ARFFFILEMANAGER, manager);
        } catch (PipelineObjectException e) {
            logger.error("Error by adding managers",e);
            return;
        }
	}

	@Override
	protected void finalise() {
		
	}

    public void setOwlClasses(String[] owlClasses) {
        this.owlClasses = owlClasses;
    }

}
