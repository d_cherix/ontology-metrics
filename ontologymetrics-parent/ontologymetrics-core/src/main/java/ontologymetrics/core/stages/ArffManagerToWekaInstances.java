package ontologymetrics.core.stages;

import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.PipelineObjectException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;

public class ArffManagerToWekaInstances extends AbstractPipelineStage {

    private ArffFileManager arff;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        arff = (ArffFileManager) this.data.getData(Type.ARFFFILEMANAGER);
        if (arff == null) {
            throw new StageInitException("No arff file manager");
        }
        return true;
    }

    @Override
    protected void process() throws StageProcessingException {
        try {
            this.data.addData(Type.WEKAINSTANCES, arff.getWekaInstances());
            this.data.removeData(Type.ARFFFILEMANAGER);
        } catch (PipelineObjectException e) {
            throw new StageProcessingException(e);
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
