package ontologymetrics.core.stages;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ontologymetrics.core.Config;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArffMerger extends AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(ArffMerger.class);
    private String[] arffFiles;
    private String out;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        Config config = (Config) data.getData(Type.CONFIG);
        if (config.getConfigValue(this.name + ".arffFiles") == null
                || config.getConfigValue(this.name + ".outPath") == null) {
            throw new StageInitException();
        }
        arffFiles = config.getConfigValue(this.name + ".arffFiles").split(";");
        out = config.getConfigValue(this.name + ".outPath");
        return true;
    }

    @Override
    protected void process() {
        String relation = "";
        BufferedReader reader = null;
        String line;
        List<String> attributes = new LinkedList<String>();
        List<String> attributesForFile;
        List<List<String>> data = new LinkedList<List<String>>();
        for (String file : arffFiles) {
            try {
                reader = new BufferedReader(new FileReader(file));
                relation = reader.readLine();
                while (reader.ready()) {
                    line = reader.readLine();
                    if (line.startsWith("@attribute")) {
                        if (!attributes.contains(line)) {
                            attributes.add(line);
                        }
                    } else if (line.startsWith("@data")) {
                        continue;
                    }
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(this.out));
            writer.append(relation + "\n");
            writer.append("\n");
            for (int i = 0; i < attributes.size(); i++) {
                writer.append(attributes.get(i));
                writer.append("\n");
            }
            writer.append("\n@data\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int instances = 0;
        for (String file : arffFiles) {
            System.out.println("Reading file " + file);
            attributesForFile = new LinkedList<String>();
            try {
                reader = new BufferedReader(new FileReader(file));
                boolean isData = false;
                relation = reader.readLine();
                while (reader.ready()) {
                    line = reader.readLine();
                    if (line.startsWith("@attribute")) {
                        attributesForFile.add(line);
                    } else if (line.startsWith("@data")) {
                        isData = true;
                    } else if (isData) {
                        String[] tmpData = line.split(",(?=([^\']*\'[^\']*\')*[^\']*$)");
                        // System.out.println("i "+Arrays.toString(tmpData));
                        List<String> dataArray = new ArrayList<String>(attributes.size());
                        for (int i = 0; i < attributes.size(); i++) {
                            dataArray.add("?");
                        }
                        for (int i = 0; i < tmpData.length; i++) {
                            int pos = attributes.indexOf(attributesForFile.get(i));
                            dataArray.set(pos, tmpData[i]);
                        }
                        for (int i = 0; i < dataArray.size(); i++) {
                            if (i > 0) {
                                writer.append(",");
                            }
                            writer.append(dataArray.get(i));
                        }
                        writer.append("\n");

                        instances++;
                        writer.flush();
                        if (instances % 1000 == 0) {
                            logger.info("{} instances processed", instances);

                        }
                    }
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    public void setArffFiles(String[] arffFiles) {
        this.arffFiles = arffFiles;
    }

    public void setOut(String out) {
        this.out = out;
    }

    public static void main(String[] args) {
        ArffMerger m = new ArffMerger();
        String[] files = new String[] { "/data/d.cherix/master/arffs/ont1/unister-owl:Hotel.arff",
                "/data/d.cherix/master/arffs/ont1/untrusted/unister-owl:Hotel.arff" };
        m.setArffFiles(files);
        m.setOut("/data/d.cherix/master/arffs/ont1/unister-owl:Hotelcombined.arff");
        m.process();
    }

}
