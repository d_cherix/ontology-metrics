package ontologymetrics.core.stages;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArffUtils {

    private static final Logger logger = LoggerFactory.getLogger(ArffUtils.class);

    public static int numAttributesInFile(String arfffile) {
        BufferedReader reader = null;
        int atts = 0;
        try {
            reader = new BufferedReader(new FileReader(arfffile));
            String line;
            while (reader.ready()) {
                line = reader.readLine();
                if (line.startsWith("@attribute")) {
                    atts++;
                }
                if (line.startsWith("@data")) {
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return atts;
    }
}
