package ontologymetrics.core.stages;

import java.io.File;

import ontologymetrics.core.Config;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractThreadPoolStage;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ArffWriter extends AbstractThreadPoolStage {

    private static final Logger logger = LoggerFactory
            .getLogger(ArffWriter.class);
    private String path = "";
    @Autowired
    private SparqlPrefixes prefixes;
    private String nullvalue = null;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        Config config = (Config) this.data.getData(Type.CONFIG);
        if (config!=null && config.getConfigValue(this.name + ".outPath") == null && path.isEmpty()) {
            throw new StageInitException("Path to write the files needed");
        }
        if (path.isEmpty()) {
            path = config.getConfigValue(this.name + ".outPath");
        }
        if(!new File(path).exists()){
            new File(path).mkdirs();
        }
        return true;
    }

    public void setNullvalue(String nullvalue) {
        this.nullvalue = nullvalue;
    }

    @Override
    protected void process() {
        final String path = this.path;
        ArffFileManager manager = (ArffFileManager) data.getData(Type.ARFFFILEMANAGER);
        final ArffFileManager arff = manager;
        Runnable r = new Runnable() {
            @Override
            public void run() {
                if (nullvalue == null) {
                    arff.writeToFile(path + "/" + prefixes.getPrefixes().shortForm(arff.getName()) + ".arff");
                } else {
                    arff.writeToFile(path + "/" + prefixes.getPrefixes().shortForm(arff.getName()) + ".arff",
                            nullvalue);
                }
            }
        };
        this.queue.add(r);
    }

    public void setPath(String path) {
        this.path = path;
    }

}
