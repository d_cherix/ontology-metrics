package ontologymetrics.core.stages;

@Deprecated
public class DatatypeValue {
	private Object value;
	private String datatype;
	
	
	
	public DatatypeValue(Object value, String datatype) {
		super();
		this.value = value;
		this.datatype = datatype;
	}
	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	/**
	 * @return the datatype
	 */
	public String getDatatype() {
		return datatype;
	}
	/**
	 * @param datatype the datatype to set
	 */
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
}
