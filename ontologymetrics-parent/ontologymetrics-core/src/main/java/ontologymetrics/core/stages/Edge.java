package ontologymetrics.core.stages;
@Deprecated
public class Edge implements Comparable<Edge> {
	private String uri;
	private Vertex from;
	private Vertex to;

	public Edge(String uri, Vertex from, Vertex to) {
		super();
		this.uri = uri;
		this.from = from;
		this.to = to;
	}
	
	

	/**
	 * @return the from
	 */
	public Vertex getFrom() {
		return from;
	}



	public void setFrom(Vertex from) {
		this.from = from;
	}

	public Vertex getTo() {
		return to;
	}

	public void setTo(Vertex to) {
		this.to = to;
	}

	public Edge(String uri) {
		this.uri = uri;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Edge [uri=" + uri + ", from=" + from + ", to=" + to + "]";
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Edge)) {
			return false;
		}
		Edge other = (Edge) obj;
		if (from == null) {
			if (other.from != null) {
				return false;
			}
		} else if (!from.equals(other.from)) {
			return false;
		}
		if (to == null) {
			if (other.to != null) {
				return false;
			}
		} else if (!to.equals(other.to)) {
			return false;
		}
		if (uri == null) {
			if (other.uri != null) {
				return false;
			}
		} else if (!uri.equals(other.uri)) {
			return false;
		}
		return true;
	}



	@Override
	public int compareTo(Edge edge) {
		if(!from.equals(edge.getFrom())){
			return from.getUri().compareTo(edge.getFrom().getUri());
		}
		if(!to.equals(edge.getTo())){
			return to.getUri().compareTo(edge.getTo().getUri());
		}
		if(!uri.equals(edge.getUri())){
			return uri.compareTo(edge.getUri());
		}
		return 0;
	}
}
