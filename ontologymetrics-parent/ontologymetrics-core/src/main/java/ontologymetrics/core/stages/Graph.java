package ontologymetrics.core.stages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ontologymetrics.core.exceptions.GraphException;
@Deprecated
public class Graph {
	

	private Set<Triple> outgoing;
	private Set <Triple> ingoing;
	private Set<Triple> properties;
	
	private String name;
	private String sparqlQuery;
	private Date date;

	public Graph(String name, String sparqlQuery, Date date) {
		super();
		this.name = name;
		this.sparqlQuery = sparqlQuery;
		this.date = date;
		outgoing= new HashSet<Triple>();
		ingoing= new HashSet<Triple>();
		properties= new HashSet<Triple>();
	}

	public Set<Triple>getTriples(){
		HashSet<Triple> triples = new HashSet<Triple>();
		triples.addAll(ingoing);
		triples.addAll(outgoing);
		triples.addAll(properties);
		return triples;
	}
	
	public void addIngoing(Triple triple){
		this.ingoing.add(triple);
	}
	
	public void addOutgoing(Triple triple){
		this.outgoing.add(triple);
	}
	
	public void addProperty(Triple triple){
		this.properties.add(triple);
	}
	
	/**
	 * @return the outgoing
	 */
	public Set<Triple> getOutgoing() {
		return outgoing;
	}

	/**
	 * @return the ingoing
	 */
	public Set<Triple> getIngoing() {
		return ingoing;
	}

	/**
	 * @return the properties
	 */
	public Set<Triple> getProperties() {
		return properties;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSparqlQuery() {
		return sparqlQuery;
	}

	public void setSparqlQuery(String sparqlQuery) {
		this.sparqlQuery = sparqlQuery;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
