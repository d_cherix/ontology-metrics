package ontologymetrics.core.stages;

import ontologymetrics.core.data.GraphDBImpl;

import org.neo4j.cypher.javacompat.ExecutionEngine;

public class GraphQueryEngine {
	
	private ExecutionEngine engine;
	
	public GraphQueryEngine(GraphDBImpl graphDb){
		engine= new ExecutionEngine(graphDb.getGraphDb());
	}
	
	public ExecutionEngine getEngine(){
		return engine;
	}
}
