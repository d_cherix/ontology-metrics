package ontologymetrics.core.stages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Deprecated
public class Vertex {
	private String uri;
	private Map<String, Object> properties;

	public Vertex(String uri) {
		this.uri = uri;
		properties = new HashMap<String, Object>();
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Map<String, Object> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	public boolean hasProperty(String property) {
		return this.properties.containsKey(property);
	}

	@SuppressWarnings("unchecked")
	public void addProperty(String property, Object value) {
		if (properties.containsKey(property)) {
			Object contained = properties.get(property);
			if (contained != value) {
				if (contained instanceof List) {
					((List<Object>) contained).add(value);
				} else {
					ArrayList<Object> list = new ArrayList<Object>();
					list.add(contained);
					list.add(value);
					properties.put(property, list);
				}
			}
		} else {
			properties.put(property, value);
		}
	}

	public Object getProperty(String property) {
		return this.properties.get(property);
	}

	@Override
	public String toString() {
		return this.uri;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Vertex)) {
			return false;
		}
		Vertex other = (Vertex) obj;
		if (uri == null) {
			if (other.uri != null) {
				return false;
			}
		} else if (!uri.equals(other.uri)) {
			return false;
		}
		return true;
	}

	

}
