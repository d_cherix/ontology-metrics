package ontologymetrics.core.stages;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import lombok.Getter;
import lombok.Setter;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.PipelineObjectException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

public class WekaLoaderFromArffFile extends AbstractPipelineStage {

    @Getter
    @Setter
    private String path;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        if (this.data.getData(Type.WEKAINSTANCES) != null) {
            throw new StageInitException("Weka instances allready exists");
        }
        return true;
    }

    @Override
    protected void process() throws StageProcessingException {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(path));
            Instances instances = new Instances(reader);
            this.data.addData(Type.WEKAINSTANCES, instances);
        } catch (FileNotFoundException e) {
            throw new StageProcessingException(e);
        } catch (IOException e) {
            throw new StageProcessingException(e);
        } catch (PipelineObjectException e) {
            throw new StageProcessingException(e);
        }

    }

    @Override
    protected void finalise() {
       
    }

}
