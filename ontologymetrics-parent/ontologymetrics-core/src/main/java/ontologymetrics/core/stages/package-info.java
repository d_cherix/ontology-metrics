
/**
 * This package contains pipeline stages that are used in all the application.
 * @author d.cherix
 *
 */
package ontologymetrics.core.stages;