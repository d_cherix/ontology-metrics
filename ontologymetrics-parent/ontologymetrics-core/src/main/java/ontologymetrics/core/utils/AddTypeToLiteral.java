package ontologymetrics.core.utils;


public class AddTypeToLiteral {
    private String property;
    private String endpoint;
    private String graph = "http://swat.cse.lehigh.edu/onto/univ-bench.owl";

    public void CorrectType(int limit, int offset) {
//        ParameterizedSparqlString query = new ParameterizedSparqlString("SELECT * WHERE { ?x ?property ?y . ?x a <http://swat.cse.lehigh.edu/onto/univ-bench.owl#GraduateStudent> .} "
//                + "order by ?x LIMIT ?limit OFFSET ?offset");
//        query.setLiteral("limit",limit);
//        query.setLiteral("offset", offset);
//        query.setIri("property",property);
//        QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query.asQuery());
//        ParameterizedSparqlString updateString = new ParameterizedSparqlString("WITH ?graph "
//                + "DELETE { ?x ?p ?y } "
//                + "INSERT {?x ?p ?correct } "
//                + "WHERE {?x ?p ?y}");
//        ResultSet rs = qexec.execSelect();
//        QuerySolution solution;
//        while (rs.hasNext()) {
//            solution = rs.next();
//            Literal y = solution.getLiteral("y");
//            RDFNode x = solution.get("x");
//            if (y.getDatatype() == null) {
//                updateString.setIri("graph", graph);
//                updateString.setIri("x", x.toString());
//                updateString.setLiteral("y", y.toString());
//                Model model = ModelFactory.createDefaultModel();
//                updateString.setLiteral("correct", model.createTypedLiteral(y.toString()));
//                updateString.setIri("p", property);
//                try {
//                    URL url = new URL(endpoint + "?query=" + URLEncoder.encode(updateString.toString(), "utf-8"));
//                    URLConnection connection = url.openConnection();
//                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                    while (in.ready()) {
//                        System.out.println(in.readLine());
//                    }
//                    System.out.println();
//                } catch (UnsupportedEncodingException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                } catch (MalformedURLException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//        }
    }

    public static void main(String[] args) {
        AddTypeToLiteral attl = new AddTypeToLiteral("http://swat.cse.lehigh.edu/onto/univ-bench.owl#name",
                "http://localhost:8890/sparql");
        attl.CorrectType(1000, 1000);
    }

    public AddTypeToLiteral(String property, String endpoint) {
        super();
        this.property = property;
        this.endpoint = endpoint;
    }
}
