package ontologymetrics.core.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AttributesStatistic {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = null;
        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
        ThreadPoolExecutor tpe = new ThreadPoolExecutor(10, 20, 1000, TimeUnit.SECONDS, queue);
        tpe.prestartAllCoreThreads();
        try {

            reader = new BufferedReader(new FileReader(args[0]));
            String line;
            ArrayList<String> attributesName = new ArrayList<String>();
            String[] tokens;
            int[] nullValues = null;
            boolean isdata = false;
            int instances = 0;
            while (reader.ready()) {
                line = reader.readLine();
                if (isdata) {
                    instances++;
                    Splitter s = new Splitter(line, nullValues,instances);
                    tpe.execute(s);
                }
                else if (line.startsWith("@attribute")) {
                    tokens = line.split("\\s");
                    attributesName.add(tokens[1]);
                } else if (line.startsWith("@data")) {
                    isdata = true;
                    nullValues = new int[attributesName.size()];
                    for (int i = 0; i < nullValues.length; i++) {
                        nullValues[i] = 0;
                    }
                }
            }
            tpe.shutdown();
            tpe.awaitTermination(1, TimeUnit.HOURS);
            BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]));
            writer.append("Name\tnull values\tpercent");
            writer.append("****************************************************************************\n");
            for (int i = 0; i < attributesName.size(); i++) {
                double percent = (double) nullValues[i] / (double) instances * 100;
                writer.append(attributesName.get(i) + "\t" + nullValues[i] + "\t" + percent);
                writer.append("\n");
                writer.flush();
            }
            writer.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
