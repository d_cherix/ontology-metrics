package ontologymetrics.core.utils;


public class OntologyTypes {
//    @Setter
//    private String endpoint;
//    private ParameterizedSparqlString distQuery;
//    
//    public void init(){
//        distQuery = new ParameterizedSparqlString("select ?x (count(distinct ?h) as ?count) WHERE {?x ?property ?h . ?x a ?class . } group by ?x");
//    }
//    
//    public Map<String, List<String>> propertiesForClasses(String prefix) {
//        String query = "select distinct ?property ?class where {?x a ?class . ?x ?property ?y .} order by ?class";
//        QueryExecution exec = QueryExecutionFactory.sparqlService(endpoint, query);
//        ResultSet rs = exec.execSelect();
//        QuerySolution solution;
//        Map<String, List<String>> propertiesCoresp = Maps.newLinkedHashMap();
//        while (rs.hasNext()) {
//            solution = rs.next();
//            String key = solution.get("class").asResource().getURI();
//            if (key.startsWith(prefix)) {
//                if (!propertiesCoresp.containsKey(key)) {
//                    propertiesCoresp.put(key, new LinkedList<String>());
//                }
//                propertiesCoresp.get(key).add(solution.get("property").asResource().getURI());
//            }
//        }
//        return propertiesCoresp;
//    }
//    
//    public void distribution(String owlClass, String property){
//        distQuery.setIri("property", property);
//        distQuery.setIri("class", owlClass);
//        QueryExecution exec = QueryExecutionFactory.sparqlService(endpoint, distQuery.asQuery());
//        ResultSet rs = exec.execSelect();
//        QuerySolution solution;
//        Map<Integer,Integer> dist= Maps.newHashMap();
//        double i=0;
//        while(rs.hasNext()){
//            solution=rs.next();
//            int key = solution.get("count").asLiteral().getInt();
//            if(dist.containsKey(key)){
//                dist.put(key, dist.get(key)+1);
//            } else {
//                dist.put(key, 1);
//            }
//            i++;
//        }
//        for(int key:dist.keySet()){
//            double a = dist.get(key).doubleValue()/i*10000;
//            a=Math.round(a);
//            a=a/100.0;
//            System.out.println(a+"% of "+owlClass+" have "+key+" values for property "+property);
//        }
//        System.out.println("**");
//    }
//
//    public static void main(String[] args) {
//        OntologyTypes o = new OntologyTypes();
//        o.setEndpoint("http://localhost:8890/sparql");
//        o.init();
//        Map<String, List<String>> map = o.propertiesForClasses("http://swat.cse.lehigh.edu/onto/univ-bench.owl");
//        for(String owlClass:map.keySet()){
//            for(String property:map.get(owlClass)){
//                o.distribution(owlClass, property);
//            }
//        }
//    }
}
