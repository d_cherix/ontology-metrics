package ontologymetrics.core.utils;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class SetUtils {
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2, Comparator<T> comparator){
        HashSet<T> resultSet = new HashSet<T>();
        for(T t1:set1){
            for(T t2:set2){
               if( comparator.compare(t1, t2)==0){
                   resultSet.add(t1);
                   break;
               }
            }
        }
        return resultSet;
    }
}
