package ontologymetrics.core.utils;

import ontologymetrics.core.data.UriResource;
import ontologymetrics.core.data.UriResourceBuilder;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

public class StringToUriResource implements Converter<String, UriResource> {

    @Autowired
    SparqlPrefixes prefixes;
    @Override
    public UriResource convert(String uri) {
        uri=prefixes.expand(uri);
        return UriResourceBuilder.uriResource().uri(uri).build();
    }

}
