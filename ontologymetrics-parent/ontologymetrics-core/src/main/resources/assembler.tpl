@prefix tdb:     <http://jena.hpl.hp.com/2008/tdb#> .
@prefix ja:      <http://jena.hpl.hp.com/2005/11/Assembler#> .
{{#prefixes}}
@prefix {{this}} .
{{/prefixes}}

[] ja:loadClass "com.hp.hpl.jena.tdb.TDB" .
tdb:DatasetTDB  rdfs:subClassOf  ja:RDFDataset .
tdb:GraphTDB    rdfs:subClassOf  ja:Model      .

<#dataset> rdf:type         tdb:DatasetTDB ;
    tdb:location "{{path}}/DB";
    .