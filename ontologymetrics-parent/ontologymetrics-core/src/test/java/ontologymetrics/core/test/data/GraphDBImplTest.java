package ontologymetrics.core.test.data;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ontologymetrics.core.data.GraphDBImpl;
import ontologymetrics.core.data.ObjectProperty;
import ontologymetrics.core.data.UriResourceBuilder;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.junit.Test;
import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.repository.RepositoryException;
import org.openrdf.sail.SailException;

public class GraphDBImplTest {

    @Test
    public void test() throws SailException, RepositoryException, DBException {
        GraphDBImpl graphDb = new GraphDBImpl(System.getProperty("java.io.tmpdir")+"/"+System.currentTimeMillis());
        SparqlPrefixes prefixes = new SparqlPrefixes(new HashMap());
        graphDb.setPrefixes(prefixes);
        graphDb.init();
        
        Set<Statement> model = new HashSet<Statement>();
        ValueFactory factory = ValueFactoryImpl.getInstance();
        URI subject = factory.createURI("http://example.org/subject");
        URI predicate = factory.createURI("http://example.org/predicate");
        URI object = factory.createURI("http://example.org/object");
        URI dataTypeP = factory.createURI("http://example.org/dtp");
        Literal literal = factory.createLiteral("literal");
        
        model.add(factory.createStatement(subject, predicate, object));
        model.add(factory.createStatement(subject, dataTypeP, literal));
        graphDb.save(new TreeModel(model), "");
        
        List<ObjectProperty> result = graphDb.listObjectProperties(UriResourceBuilder.uriResource().uri(subject.stringValue()).build());
        assertEquals(1, result.size());
    }

}
