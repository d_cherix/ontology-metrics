package ontologymetrics.interfaces;

import java.util.Iterator;

import ontologymetrics.core.data.GraphDBImpl;

import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.tooling.GlobalGraphOperations;

public class IndexUtils {
    public static void main(String[] args) {
        GraphDBImpl graphdB = new GraphDBImpl("/data/d.cherix/master/db/dbpedia");
        GraphDatabaseService db = graphdB.getGraphDb();
        Index<Node> index = db.index().forNodes("uri");
        Index<Relationship> rels = db.index().forRelationships("name");
        Index<Relationship> datatypesProperties = db.index().forRelationships("datatypeProperties");
        Index<Relationship> objectProperties = db.index().forRelationships("objectProperties");
         ExecutionResult result =
         graphdB.getExecutionEngine().execute("start n = node:uri(uri=\"http://dbpedia.org/resource/Düsseldorf\")"
         +
         "  " +
         "return n");
         System.out.println(result.dumpToString());
       
        // Node node = index.query("uri",
        // "\"http://ontology.unister.de/resource/geonames/2637052\"").getSingle();
        // System.out.println(node.getPropertyKeys());
        // Iterable<Relationship> rels = node.getRelationships(Direction.INCOMING);
        // int i=0;
        // for(Iterator<Relationship> it=rels.iterator();it.hasNext();){
        // System.out.println(it.next().getStartNode().getPropertyKeys());
        // i++;
        // if(i>=10){
        // break;
        // }
        // }
        // System.out.println(i);
db.shutdown();
    }
}
