package ontologymetrics.interfaces;

import ontologymetrics.core.pipeline.LinearPipeline;
import ontologymetrics.core.pipeline.PipelineInterface;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import uk.org.lidalia.sysoutslf4j.context.LogLevel;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import com.jamonapi.MonitorFactory;
import com.jamonapi.proxy.MonProxyFactory;

public class MetricsStandalone {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(MetricsStandalone.class);

	public static void main(String[] args) throws Exception {
		SysOutOverSLF4J.sendSystemOutAndErrToSLF4J(LogLevel.DEBUG,
				LogLevel.WARN);

		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
				"config-metrics.xml");
		context.registerShutdownHook();
		PipelineInterface pipeline = (PipelineInterface) MonProxyFactory
				.monitor(BeanFactoryUtils.beanOfType(context,
						LinearPipeline.class));

		pipeline.process();
		LOGGER.info(MonitorFactory.getReport());
	}

	@Autowired
	private SparqlPrefixes prefixes;
}
