package ontologymetrics.interfaces;

import java.util.Iterator;
import java.util.Map;

import ontologymetrics.core.data.GraphDBImpl;

import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoveDuplicate {
    
    
    private static final Logger logger = LoggerFactory.getLogger(RemoveDuplicate.class);
    
    public static void main(String[] args) {
        GraphDBImpl graphdB = new GraphDBImpl("/data/d.cherix/master/db/dbpedia");
        GraphDatabaseService db = graphdB.getGraphDb();
        Index<Node> index = db.index().forNodes("uri");
        Index<Relationship> objectProperties = db.index().forRelationships("objectProperties");
        ExecutionResult result = graphdB.getExecutionEngine().execute("start n = node:uri('uri:*') return n");
        Transaction tx = db.beginTx();
        for (Iterator<Map<String, Object>> it = result.iterator(); it.hasNext();) {
            try {
                Node n = (Node) it.next().get("n");
                String uri = (String) n.getProperty("uri");
                logger.info("processing node {}",n);
                IndexHits<Node> nodes = index.get("uri", uri);
                while (nodes.hasNext()) {
                    Node other = nodes.next();
                    if (other.getId() != n.getId()) {
                        Iterable<Relationship> rels = other.getRelationships(Direction.INCOMING);
                        Relationship rel;
                        Relationship newRel;
                        for (Iterator<String> iter = other.getPropertyKeys().iterator(); iter.hasNext();) {
                            String key = iter.next();
                            n.setProperty(key, other.getProperty(key));
                        }
                        for (Iterator<Relationship> iter = rels.iterator(); iter.hasNext();) {
                            rel = iter.next();
                            Node start = rel.getStartNode();
                            newRel = start.createRelationshipTo(n, rel.getType());
                            objectProperties.remove(rel);
                            objectProperties.add(newRel, "node", start);
                            rel.delete();
                            index.remove(other);
                            other.delete();
                        }
                    }
                }
                tx.success();
                tx.finish();
                tx=db.beginTx();
                nodes.close();
            } catch (IllegalStateException e) {
                logger.info("node allready deleted");
                tx=db.beginTx();
                continue;
            }
        }
    }
}
