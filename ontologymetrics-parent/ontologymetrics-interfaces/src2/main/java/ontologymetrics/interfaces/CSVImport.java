/**
 * 
 */
package ontologymetrics.interfaces;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVReader;

/**
 * @author didier
 * 
 */
public class CSVImport {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(CSVImport.class);

    public static void main(String[] args) throws IOException, RepositoryException, MalformedQueryException, QueryEvaluationException {
        CSVReader reader = new CSVReader(new FileReader("dbpedia.csv"));
        List<String[]> myEntries = reader.readAll();
        Set<String> uris = new HashSet<String>();
        for (String[] entry : myEntries) {
            uris.add(entry[2]);
        }
        System.out.println(uris);
        String queryString = "SELECT DISTINCT ?type WHERE { ?instance <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?type "
                +
                "FILTER (?instance IN (";

        int i = 0;
        for (String uri : uris) {
            if (i > 0)
                queryString += ",";
            queryString += "<" + uri + ">";
            i++;
        }
        queryString += "))}";
        HTTPRepository repository = new HTTPRepository("http://live.dbpedia.org/sparql");
        RepositoryConnection con=null;
        TupleQuery query;
        TupleQueryResult result;
        BufferedWriter writer=null;
		try {
			con = repository.getConnection();
			query = con.prepareTupleQuery(QueryLanguage.SPARQL,queryString);
	        result = query.evaluate();
	        writer = new BufferedWriter(new FileWriter("dbpediatypes.txt"));
	        while (result.hasNext()) {
	            writer.append(result.next().getBinding("type").getValue().stringValue());
	            writer.append("\n");
	        }
		} finally{
		    writer.close();
		    con.close();
		}
        

    }
}
