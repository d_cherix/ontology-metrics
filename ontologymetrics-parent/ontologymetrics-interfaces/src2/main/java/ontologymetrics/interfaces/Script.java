package ontologymetrics.interfaces;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import ontologymetrics.core.data.SPARQLGraphDb;
import ontologymetrics.core.data.UriResource;

import org.openrdf.model.Statement;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.turtle.TurtleWriter;
import org.openrdf.sail.SailException;

@Deprecated
public class Script {
//    public static void main(String[] args) throws MalformedQueryException, RepositoryException, QueryEvaluationException, IOException, RDFHandlerException, SailException {
//        SPARQLGraphDb db = new SPARQLGraphDb("/data/d.cherix/master/db/dbpedia");
//        db.init();
//        TurtleWriter writer = new TurtleWriter(new FileWriter("dbpedia.ttl"));
//        Collection<UriResource> query = db.getInstancesFor(new UriResource(""));
//         writer.startRDF();
//         BufferedWriter writer2 = new BufferedWriter(new FileWriter("tmp"));
//         GraphQueryResult result = query.evaluate();
//         while(result.hasNext()){
//             Statement statement = result.next();
//             writer.handleStatement(statement);
//             writer2.append(statement.toString());
//             writer2.flush();
//         }
//         writer.endRDF();
//         db.close();
//    }
}
