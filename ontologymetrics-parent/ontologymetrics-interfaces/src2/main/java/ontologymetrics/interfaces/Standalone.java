package ontologymetrics.interfaces;


import ontologymetrics.core.pipeline.LinearPipeline;
import ontologymetrics.core.pipeline.PipelineInterface;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import uk.org.lidalia.sysoutslf4j.context.LogLevel;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import com.jamonapi.MonitorFactory;
import com.jamonapi.proxy.MonProxyFactory;

public class Standalone {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(Standalone.class);

	public static void main(String[] args) throws Exception {
		SysOutOverSLF4J.sendSystemOutAndErrToSLF4J(LogLevel.DEBUG,
				LogLevel.WARN);

		ConfigurableApplicationContext context = new FileSystemXmlApplicationContext(
				args[0]);

		PipelineInterface pipeline = (PipelineInterface) MonProxyFactory
				.monitor(BeanFactoryUtils.beanOfType(context,
						LinearPipeline.class));

		pipeline.process();
		LOGGER.info(MonitorFactory.getReport());
		context.close();
	}

	@Autowired
	private SparqlPrefixes prefixes;
}
