package ontologymetrics.interfaces;

import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.pipeline.LinearPipeline;
import ontologymetrics.core.pipeline.PipelineInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import uk.org.lidalia.sysoutslf4j.context.LogLevel;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import com.jamonapi.MonitorFactory;
import com.jamonapi.proxy.MonProxyFactory;

public class StandaloneIntern {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(Standalone.class);

    public static void main(String[] args) throws Exception {
        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J(LogLevel.DEBUG,
                LogLevel.WARN);

        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
                args[0]);

        PipelineInterface pipeline = (PipelineInterface) MonProxyFactory
                .monitor(BeanFactoryUtils.beanOfType(context,
                        LinearPipeline.class));

        pipeline.process();
        LOGGER.info(MonitorFactory.getReport());
        context.close();
    }
}
