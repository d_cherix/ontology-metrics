package ontologymetrics.metrics;

import java.util.Set;
import java.util.regex.Pattern;

import lombok.Setter;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.data.UriResource;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.core.sparql.SparqlPrefixes;
import ontologymetrics.core.stages.ArffFileManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractMetric extends AbstractPipelineStage{

    private static final Logger logger = LoggerFactory.getLogger(AbstractMetric.class);
    @Autowired
    protected SparqlPrefixes prefixes;
    @Setter
    protected Set<UriResource> consideredProperties;
    @Setter
    private String propertyPattern=".*";
    protected Pattern pattern;

    public AbstractMetric(){
        pattern = Pattern.compile(propertyPattern);
    }
    
    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        if (this.data.getData(Type.ARFFFILEMANAGER) == null) {
            throw new StageInitException("Component " + name
                    + " need a list of ArffFileManager to process");
        }
        return this.checkRequires();
    }

    @Override
    protected void process() throws StageProcessingException {
        
        ArffFileManager arff = (ArffFileManager) this.data.getData(Type.ARFFFILEMANAGER);
        int i = 0;
        for (Instance instance : arff.getInstances().getInstances()) {
            try {
                this.processInstance(instance, arff);
            } catch (DBException e) {
                throw new StageProcessingException(e);
            }
            i++;
            if (i % 1000 == 0) {
                logger.debug("{} nodes of type {} processed", i, arff.getName());
            } else if( i%100==0){
                logger.debug("{} % of next 1000",(i%1000)/10);
            }
        }
    }

    protected abstract boolean checkRequires() throws StageInitException;

    protected abstract void processInstance(Instance instance, ArffFileManager arff) throws StageProcessingException,
            DBException;
    

}
