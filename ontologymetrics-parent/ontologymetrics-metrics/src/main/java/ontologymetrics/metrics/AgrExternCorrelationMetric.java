package ontologymetrics.metrics;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import ontologymetrics.core.data.DatatypeProperty;
import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.data.IAttribute.Type;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.LiteralBuilder;
import ontologymetrics.core.data.LiteralBuilder.LiteralType;
import ontologymetrics.core.data.LiteralResource;
import ontologymetrics.core.data.ObjectProperty;
import ontologymetrics.core.data.UriResource;
import ontologymetrics.core.data.UriResourceBuilder;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.sparql.SparqlPrefixes;
import ontologymetrics.core.stages.ArffFileManager;
import ontologymetrics.core.utils.SetUtils;

import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class AgrExternCorrelationMetric extends AbstractMetric {

    
    private static final Logger logger = LoggerFactory.getLogger(AgrExternCorrelationMetric.class);
    
    @Autowired
    private GraphDBDao graphDb;
    @Autowired
    private SparqlPrefixes prefixes;

    @Override
    protected boolean checkRequires() throws StageInitException {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void processInstance(Instance instance, ArffFileManager arff) throws DBException {
        UriResource uriResource = UriResourceBuilder.uriResource().uri(instance.getName()).build();
        List<UriResource> types = Lists.newLinkedList();
        types.add(UriResourceBuilder.uriResource().uri("http://ontology.unister.de/ontology#City").build());
        types.add(UriResourceBuilder.uriResource().uri("http://ontology.unister.de/ontology#Continent").build());
        types.add(UriResourceBuilder.uriResource().uri("http://ontology.unister.de/ontology#Country").build());
        HTTPRepository repository = new HTTPRepository("http://localhost:8890/sparql");
        RepositoryConnection con=null;
        try {
            con = repository.getConnection();
            for (UriResource type : types) {
                TupleQuery query = con.prepareTupleQuery(
                        QueryLanguage.SPARQL,
                        prefixes.addPrefixesToQuery("SELECT ?o ?place where {" + uriResource.sparql()
                                + " unister-owl:locatedIn ?place . ?place unister-owl:population ?o . ?place rdf:type "
                                + type.sparql() + "}"));
                TupleQueryResult result = query.evaluate();
                
                double thosePopulation = 0;
                String place="";
                if (result.hasNext()) {
                    BindingSet binding = result.next();
                    thosePopulation = Double.parseDouble(binding.getBinding("o").getValue().stringValue());
                    place = binding.getBinding("place").getValue().stringValue();
                    query = con.prepareTupleQuery(
                            QueryLanguage.SPARQL,
                            prefixes.addPrefixesToQuery("select sum(?p) as ?total  where { ?x unister-owl:locatedIn <"+place+"> . ?x unister-owl:population ?p . ?x rdf:type unister-owl:City }"));
                    TupleQueryResult result2 = query.evaluate();
                    double thisPopulation=0;
                    if(result2.hasNext()){
                        try{
                        thisPopulation=Double.parseDouble(result2.next().getBinding("total").getValue().stringValue());
                        } catch(NullPointerException e){
                            
                        }
                    }
                    double comp = Double.compare(thisPopulation, thosePopulation);
                    int pos = arff.addAttribute("population_" + prefixes.shortForm(type), Type.NUMERIC);
                    arff.addValue(instance, (comp > 0) ? 1 : ((comp == 0) ? 0 : -1), pos);
                }
            }

        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedQueryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (QueryEvaluationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (RepositoryException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
