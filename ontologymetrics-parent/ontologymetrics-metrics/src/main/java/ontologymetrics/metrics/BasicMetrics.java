package ontologymetrics.metrics;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ontologymetrics.core.data.DatatypeProperty;
import ontologymetrics.core.data.IResource;
import ontologymetrics.core.data.LiteralResource;
import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.data.ObjectProperty;
import ontologymetrics.core.data.UriResource;
import ontologymetrics.core.data.UriResourceBuilder;
import ontologymetrics.core.data.IAttribute.Type;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.sparql.SparqlPrefixes;
import ontologymetrics.core.stages.ArffFileManager;

import org.neo4j.graphdb.Relationship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

public class BasicMetrics extends AbstractMetric {

    private static final Logger logger = LoggerFactory.getLogger(BasicMetrics.class);

    private String[] latLongProperties;
    private List<String> stringsetProperties;
    private List<String> setProperties;
    @Autowired
    private SparqlPrefixes prefixes;
    @Autowired
    private GraphDBDao graphDb;
    private int pos;
    private int precisionPos;
    private boolean precision;

    @Override
    protected boolean checkRequires() throws StageInitException {
        for (int i = 0; i < this.latLongProperties.length; i++) {
            this.latLongProperties[i] = (prefixes.getPrefixes().expandPrefix(this.latLongProperties[i]));
            logger.debug(this.latLongProperties[i]);
        }
        return true;
    }

    @Override
    protected void processInstance(Instance instance, ArffFileManager arff) throws StageProcessingException, DBException {
        UriResource uri = UriResourceBuilder.uriResource().uri(instance.getName()).build();

        Map<UriResource, Integer> stringsetvalues = new HashMap<UriResource, Integer>();
        Double latitude = null;
        Double longitude = null;

        List<DatatypeProperty> rels;
        Relationship rel;
        rels = graphDb.listDatatypeProperties(uri);
        for (DatatypeProperty datatypeProperty : rels) {
            logger.trace(datatypeProperty.getProperty().getUri());
            if (stringsetProperties.contains(datatypeProperty)) {
                if (stringsetvalues.containsKey(datatypeProperty.getProperty())) {
                    stringsetvalues.put(datatypeProperty.getProperty(),
                            stringsetvalues.get(datatypeProperty.getProperty()) + 1);
                } else {
                    stringsetvalues.put(datatypeProperty.getProperty(), 1);
                }
            } else if (latLongProperties[0].equals(datatypeProperty.getProperty().getUri())) {
                List<LiteralResource> values = graphDb.getDatatypeObject(uri, datatypeProperty.getProperty());
                if (values.size() > 1 || values.size() == 0) {
                    latitude = null;
                } else {
                    LiteralResource value = values.get(0);
                    if (value.getValue() instanceof Double) {
                        latitude = (Double) value.getValue();
                    } else if (value.getValue() instanceof Float) {
                        latitude = ((Float) value.getValue()).doubleValue();
                    }
                }
            }
            else if (latLongProperties[1].equals(datatypeProperty.getProperty().getUri())) {
                List<LiteralResource> values = graphDb.getDatatypeObject(uri, datatypeProperty.getProperty());
                if (values.size() > 1 || values.size() == 0) {
                    longitude = null;
                } else {
                    LiteralResource value = values.get(0);
                    if (value.getValue() instanceof Double) {
                        longitude = (Double) value.getValue();
                    } else if (value.getValue() instanceof Float) {
                        longitude = ((Float) value.getValue()).doubleValue();
                    }
                }
            } else {
                List<LiteralResource> values = graphDb.getDatatypeObject(uri, datatypeProperty.getProperty());
                if (values.size() > 1 || values.size() == 0) {
                    continue;
                }
                LiteralResource value = values.get(0);
                pos = arff.addAttribute(datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                if (value.getValue() instanceof String) {
                    arff.addValue(instance, ((String) value.getValue()).length(), pos);
                } else if (value.getValue() instanceof Integer) {
                    arff.addValue(instance, ((Integer) value.getValue()).doubleValue(), pos);
                    if (precision) {
                        precisionPos = arff.addAttribute(datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                        arff.addValue(instance, 0, precisionPos);
                    }
                } else if (value.getValue() instanceof Double) {
                    arff.addValue(instance, ((Double) value.getValue()), pos);
                    if (precision) {
                        precisionPos = arff.addAttribute(datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                        arff.addValue(instance, BigDecimal.valueOf((Double) value.getValue()).scale(), precisionPos);
                    }
                } else if (value.getValue() instanceof Long) {
                    arff.addValue(instance, ((Long) value.getValue()).doubleValue(), pos);
                    if (precision) {
                        precisionPos = arff.addAttribute(datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                        arff.addValue(instance, 0, precisionPos);
                    }
                } else if (value.getValue() instanceof Float) {
                    arff.addValue(instance, ((Float) value.getValue()).doubleValue(), pos);
                    if (precision) {
                        precisionPos = arff.addAttribute(datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                        arff.addValue(instance, BigDecimal.valueOf((Float) value.getValue()).scale(), precisionPos);
                    }
                }
            }
        }

        Map<String, List<Integer>> setvalues = new HashMap<String, List<Integer>>();

        List<ObjectProperty> oRels = graphDb.listObjectProperties(uri);
        for (ObjectProperty objectProperty : oRels) {

            if (setProperties.contains(objectProperty)) {
                List<UriResource> linkedResources = graphDb.getLinkedUriResources(uri,
                        objectProperty.getProperty());
                List<Integer> values = new ArrayList<Integer>(linkedResources.size());
                for (UriResource linkedObjectUri : linkedResources) {
                    values.add(linkedObjectUri.hashCode());
                }
                setvalues.put(objectProperty.getProperty().getUri(), values);
            }
        }
        uri=null;

        pos = arff.addAttribute("geolocation", Type.GEOPOINT);
        if (latitude != null && longitude != null) {
            arff.addValue(instance, latitude, longitude, pos);
            if (precision) {
                precisionPos = arff.addAttribute("latitude_precision", Type.NUMERIC);
                String[] parts = latitude.toString().split("\\.");
                int prec = 0;
                if (parts.length == 2) {
                    prec = parts[1].length();
                }
                arff.addValue(instance, prec, precisionPos);
                precisionPos = arff.addAttribute("longitude_precision", Type.NUMERIC);
                parts = longitude.toString().split("\\.");
                prec = 0;
                if (parts.length == 2) {
                    prec = parts[1].length();
                }
                arff.addValue(instance, parts[1].length(), precisionPos);
            }
        }

        for (String propertyUri : stringsetProperties) {
            pos = arff.addAttribute(propertyUri, Type.NUMERIC);
            if (stringsetvalues.containsKey(propertyUri)) {
                arff.addValue(instance, stringsetvalues.get(propertyUri).doubleValue(), pos);
            }
        }

        for (String propertyUri : setProperties) {
            pos = arff.addAttribute(propertyUri, Type.SET);
            if (setvalues.containsKey(propertyUri)) {
                arff.addValue(instance, setvalues.get(propertyUri).toArray(), pos);
            }
        }
    }

    @Required
    public void setLatLongProperties(String[] latLongProperties) {
        this.latLongProperties = latLongProperties;
    }

    @Required
    public void setStringsetProperties(List<String> stringsetProperties) {
        this.stringsetProperties = new ArrayList<String>(stringsetProperties.size());
        for (String s : stringsetProperties) {
            this.stringsetProperties.add(prefixes.getPrefixes().expandPrefix(s));
        }
    }

    @Required
    public void setSetProperties(List<String> setProperties) {
        this.setProperties = new ArrayList<String>(setProperties.size());
        for (String s : setProperties) {
            this.setProperties.add(prefixes.getPrefixes().expandPrefix(s));
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    public void setPrecision(boolean precision) {
        this.precision = precision;
    }

}
