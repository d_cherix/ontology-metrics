package ontologymetrics.metrics;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import ontologymetrics.core.data.ExploreMetricsDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class ExploreMetricsDAOImpl implements ExploreMetricsDAO {
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterTemplate;

    @Autowired
    public void init(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.jdbcTemplate
                .execute("CREATE TABLE IF NOT EXISTS `metrics_value` (" +
                        "`id` int(11) NOT NULL AUTO_INCREMENT," +
                        "`hoteluri` VARCHAR(500)," +
                        "`metric` INTEGER," +
                        "`value` INTEGER,"
                        + "`desc` VARCHAR(255)," +
                        "PRIMARY KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
        this.jdbcTemplate
                .execute("CREATE TABLE IF NOT EXISTS `metrics` (" +
                        "`id` int(11) NOT NULL," +
                        "`name` TEXT,"
                        + "`desc` varchar(255)," +
                        "PRIMARY KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
        this.jdbcTemplate
        .execute("CREATE TABLE IF NOT EXISTS `metrics_cluster` (" +
                "`id` int(11) NOT NULL AUTO_INCREMENT," +
                "`metric` INTEGER,"
                + "`five` double,"
                + "`fiftye` double,"
                + "`ninetyfive` double,"
                + "`desc` varchar(255)," +
                "PRIMARY KEY (`id`)" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
    }

    /* (non-Javadoc)
     * @see com.unister.semweb.quality.ontologymetrics.metrics.ExploreMetrics#save(java.lang.String[], java.util.List, java.util.Map, java.lang.String)
     */
    @Override
    public void save(final String[] instances, final List<Integer>[] values, final Map<String, Integer> attributes,
            final String desc) {

        final List<String> names = new ArrayList<String>(attributes.size());
        names.addAll(attributes.keySet());
        jdbcTemplate.batchUpdate("INSERT into metrics (`id`,`name`,`desc`) VALUES (?,?,?)",
                new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, attributes.get(names.get(i)));
                        ps.setString(2, names.get(i));
                        ps.setString(3, desc);
                    }

                    @Override
                    public int getBatchSize() {
                        return attributes.size();
                    }
                });
        for (int i = 0; i < instances.length; i++) {
            jdbcTemplate.batchUpdate("INSERT INTO metrics_value (`hoteluri`, `metric`, `value`, `desc`) VALUES (?,?,?,?)",
                    new MetricsSetter(instances[i], values[i], desc, attributes.size()));
        }
    }
    
    @Override
    public void saveClusterMetric(final int metric, final double fiveP, final double fiftyP, final double ninetyfiveP) {
        jdbcTemplate.update("INSERT INTO metrics_cluster (metric,five,fifty,ninetyfive VALUES (?,?,?,?)",
                new PreparedStatementSetter(){

            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
               ps.setInt(1,metric);
               ps.setDouble(2, fiveP);
               ps.setDouble(3, fiftyP);
               ps.setDouble(4, ninetyfiveP);
            }
            
        });
    }

    class MetricsSetter implements BatchPreparedStatementSetter {

        private String uri;
        private List<Integer> values;
        private String desc;
        private int size;

        public MetricsSetter(String uri, List<Integer> values, String desc, int size) {
            this.uri = uri;
            this.values = values;
            this.desc = desc;
            this.size = size;
        }

        @Override
        public void setValues(PreparedStatement ps, int i) throws SQLException {
            ps.setString(1, uri);
            ps.setInt(2, i);
            if (i < values.size() - 1) {
                ps.setInt(3, values.get(i));
            } else {
                ps.setInt(3, 0);
            }
            ps.setString(4, desc);
        }

        @Override
        public int getBatchSize() {
            return size;
        }

    }

    
}
