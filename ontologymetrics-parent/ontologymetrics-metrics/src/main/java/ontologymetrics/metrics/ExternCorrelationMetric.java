package ontologymetrics.metrics;

import java.util.List;

import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.data.IAttribute.Type;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.UriResource;
import ontologymetrics.core.data.UriResourceBuilder;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.sparql.SparqlPrefixes;
import ontologymetrics.core.stages.ArffFileManager;

import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.repository.sail.SailRepositoryConnection;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;

public class ExternCorrelationMetric extends AbstractMetric {

    @Autowired
    private GraphDBDao graphDb;
    @Autowired
    private SparqlPrefixes prefixes;

    @Override
    protected boolean checkRequires() throws StageInitException {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void processInstance(Instance instance, ArffFileManager arff)
            throws DBException {
        List<UriResource> types = Lists.newLinkedList();
        types.add(UriResourceBuilder.uriResource().uri("http://ontology.unister.de/ontology#City").build());
        types.add(UriResourceBuilder.uriResource().uri("http://ontology.unister.de/ontology#Continent").build());
        types.add(UriResourceBuilder.uriResource().uri("http://ontology.unister.de/ontology#Country").build());
        UriResource uriResource = UriResourceBuilder.uriResource()
                .uri(instance.getName()).build();
        HTTPRepository repository = new HTTPRepository("http://localhost:8890/sparql");
        RepositoryConnection con=null;
        try {
            con = repository.getConnection();
            TupleQuery query = con.prepareTupleQuery(
                    QueryLanguage.SPARQL,
                    prefixes.addPrefixesToQuery("SELECT ?population where { " + uriResource.sparql()
                            + " unister-owl:population ?population}"));
            TupleQueryResult result = query.evaluate();
            double thisPopulation = 0;
            if (result.hasNext()) {
                thisPopulation = Double.parseDouble(result.next().getBinding("population").getValue().stringValue());
            }
            for (UriResource type : types) {
                query = con.prepareTupleQuery(
                        QueryLanguage.SPARQL,
                        prefixes.addPrefixesToQuery("SELECT ?o where {" + uriResource.sparql()
                                + " unister-owl:locatedIn ?place . ?place unister-owl:population ?o . ?place rdf:type "
                                + type.sparql() + "}"));
                result = query.evaluate();
                double thosePopulation = 0;
                if (result.hasNext()) {
                    thosePopulation = Double.parseDouble(result.next().getBinding("o").getValue().stringValue());

                }
                double comp = Double.compare(thisPopulation, thosePopulation);
                int pos = arff.addAttribute("population_" + prefixes.shortForm(type), Type.NUMERIC);
                arff.addValue(instance, (comp > 0) ? 1 : ((comp == 0) ? 0 : -1), pos);
            }

        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedQueryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (QueryEvaluationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (RepositoryException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
