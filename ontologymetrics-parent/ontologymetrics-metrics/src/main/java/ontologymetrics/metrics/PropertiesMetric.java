package ontologymetrics.metrics;

import java.util.List;
import java.util.Map;

import lombok.Setter;
import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.data.IAttribute.Type;
import ontologymetrics.core.data.IProperty;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.UriResource;
import ontologymetrics.core.data.UriResourceBuilder;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.sparql.SparqlPrefixes;
import ontologymetrics.core.stages.ArffFileManager;

import org.openrdf.query.TupleQueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class PropertiesMetric extends AbstractMetric {

    private static final Logger logger = LoggerFactory.getLogger(PropertiesMetric.class);

    @Autowired
    @Setter
    private GraphDBDao graphDb;
    @Autowired
    private SparqlPrefixes prefixes;

    @Override
    protected boolean checkRequires() throws StageInitException {
        return true;
    }

    @Override
    protected void processInstance(Instance instance, ArffFileManager arff) throws DBException {
        UriResource instanceUri = UriResourceBuilder.uriResource().uri(instance.getName()).build();
        Map<UriResource, Integer> values = Maps.newLinkedHashMap();
        for (IProperty property : graphDb.listProperties(instanceUri)) {
            if (consideredProperties != null && !consideredProperties.contains(property.getProperty()) && !consideredProperties.isEmpty()) {
                continue;
            }
            if (!pattern.matcher(property.getProperty().getUri().toString()).matches()) {
                continue;
            }
            if (!values.containsKey(property.getProperty())) {
                values.put(property.getProperty(), 1);
            } else {
                values.put(property.getProperty(), values.get(property.getProperty()) + 1);
            }
        }
        instanceUri = null;
        for (UriResource key : values.keySet()) {
            int pos = arff.addAttribute("prop:"+prefixes.shortForm(key), Type.NUMERIC);
            arff.addValue(instance, values.get(key), pos);
            logger.trace("Uri {} Attr {} value {}", new Object[] { instance.getName(), key, values.get(key) });
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
