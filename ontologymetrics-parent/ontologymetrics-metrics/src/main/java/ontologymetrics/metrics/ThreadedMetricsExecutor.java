package ontologymetrics.metrics;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import lombok.Setter;
import ontologymetrics.core.ApplicationContextProvider;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.exceptions.StageProcessingException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.core.stages.ArffFileManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class ThreadedMetricsExecutor extends AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(ThreadedMetricsExecutor.class);
    @Setter
    private List<String> metrics;
    @Autowired
    private ApplicationContext appContext;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
       if(metrics==null){
           throw new StageInitException("A list of metrics name is required");
       }
        return true;
    }

    @Override
    protected void process() throws StageProcessingException {
        ArffFileManager arff = (ArffFileManager) this.data.getData(Type.ARFFFILEMANAGER);
        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
        ThreadPoolExecutor pool = new ThreadPoolExecutor(4, 8, 1000, TimeUnit.SECONDS,queue);
        pool.prestartCoreThread();
        int jobs = arff.getInstances().getInstances().size()*metrics.size();
        for (String metricName : metrics) {
            AbstractMetric metric = (AbstractMetric) appContext.getBean(metricName);
            for (Instance instance : arff.getInstances().getInstances()) {
                Executor e = new Executor(metric,instance,arff);
                while (pool.getQueue().size()>10000){
                    try {
                       Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                       
                        e1.printStackTrace();
                    }
                }
                pool.execute(e);
                jobs--;
                if(jobs%1000==0){
                    logger.debug("{} jobs to solve",jobs);
                }
            }
        }
        pool.shutdown();
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    public class Executor implements Runnable {

        AbstractMetric metric;
        Instance instance;
        ArffFileManager arff;

        public Executor(AbstractMetric metric, Instance instance, ArffFileManager arff) {
            super();
            this.metric = metric;
            this.instance = instance;
            this.arff = arff;
        }

        @Override
        public void run() {
            try {
                metric.processInstance(instance, arff);
            } catch (StageProcessingException e) {
                logger.error("Error during processing of metric", e);
            } catch (DBException e) {
                logger.error("Error during processing of metric", e);
            }
        }

    }

}
