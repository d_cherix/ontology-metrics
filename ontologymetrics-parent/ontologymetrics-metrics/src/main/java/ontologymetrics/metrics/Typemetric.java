package ontologymetrics.metrics;

import java.util.Map;

import lombok.Setter;
import ontologymetrics.core.data.DatatypeProperty;
import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.data.IAttribute.Type;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.LiteralResource;
import ontologymetrics.core.data.ObjectProperty;
import ontologymetrics.core.data.UriResource;
import ontologymetrics.core.data.UriResourceBuilder;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.sparql.SparqlPrefixes;
import ontologymetrics.core.stages.ArffFileManager;

import org.openrdf.model.vocabulary.RDF;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class Typemetric extends AbstractMetric {
    @Autowired
    @Setter
    private GraphDBDao graphDb;

    @Autowired
    private SparqlPrefixes prefixes;

    @Override
    protected boolean checkRequires() throws StageInitException {
        return true;
    }

    @Override
    protected void processInstance(Instance instance, ArffFileManager arff) throws DBException {
        UriResource instanceUri = UriResourceBuilder.uriResource().uri(instance.getName()).build();
        Map<String, Integer> values = Maps.newLinkedHashMap();

        // for (DatatypeProperty property : Sets.newLinkedHashSet(graphDb.listDatatypeProperties(instanceUri))) {
        // String type = property.getLiteral().getDatatype();
        // if (type == null) {
        // type = "notype";
        // }
        // String key = prefixes.getPrefixes().shortForm(property.getProperty()) + "_" +
        // prefixes.getPrefixes().shortForm(type);
        // if (!values.containsKey(key)) {
        // values.put(key, 1);
        // } else {
        // values.put(key, values.get(key) + 1);
        // }
        // }
        for (ObjectProperty property : graphDb.listObjectProperties(instanceUri)) {
            UriResource uri = property.getProperty();
            if (consideredProperties != null && !consideredProperties.contains(uri) && !consideredProperties.isEmpty()) {
                continue;
            }
            if (!pattern.matcher(uri.getUri().toString()).matches()) {
                continue;
            }
            for (UriResource type : graphDb.getType(property.getObject())) {
                String key = prefixes.shortForm(property.getProperty()) + "_"
                        + prefixes.shortForm(type);
                if (!values.containsKey(key)) {
                    values.put(key, 1);
                } else {
                    values.put(key, values.get(key) + 1);
                }

            }
        }
        instanceUri = null;
        for (String key : values.keySet()) {
            int pos = arff.addAttribute("type:"+key, Type.NUMERIC);
            arff.addValue(instance, values.get(key), pos);
        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
