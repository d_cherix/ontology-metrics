package ontologymetrics.metrics.graphmetrics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ontologymetrics.core.data.IAttribute.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.core.stages.ArffFileManager;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Deprecated
public class OutgoingProperties extends AbstractPipelineStage {

    
    private static final Logger logger = LoggerFactory.getLogger(OutgoingProperties.class);
    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
//        if (in == null) {
//            throw new ComponentInitException("Component " + name
//                    + " need a list of ArffFileManager to process");
//        }
        return false;
    }

    @Override
    protected void process() {
//        for(ArffFileManager arff:in){
//           int nbOfOutgoingProps= arff.addAttribute("NbOfOutgoingProps",Type.NUMERIC);
//           int nbOfDistOutgoingProps= arff.addAttribute("NbOfDistOutgoingProps",Type.NUMERIC);
//            for (Node node : arff.getInstances()){
//                Iterable<Relationship> rels = node.getRelationships(Direction.OUTGOING);
//                int count=0;
//                Set<RelationshipType> relsType = new HashSet<RelationshipType>();
//                for(Relationship rel : rels){
//                    count++;
//                    relsType.add(rel.getType());
//                }
//                arff.addValue(node, count, nbOfOutgoingProps);
//                arff.addValue(node, relsType.size(),nbOfDistOutgoingProps);
//            }
//        }
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
