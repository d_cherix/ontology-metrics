package ontologymetrics.metrics.numericmetrics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import lombok.Setter;
import ontologymetrics.core.data.IAttribute;
import ontologymetrics.core.data.NumericAttribute;
import ontologymetrics.core.data.PipelineObject.Type;
import ontologymetrics.core.data.RawData;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.core.stages.ArffFileManager;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.primitives.Doubles;

public class CorrelationsMetric extends AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(CorrelationsMetric.class);

    private long keepAliveTime = 0;
    private int maximumPoolSize = 0;
    private int corePoolSize = 0;
    private int threads;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        if (keepAliveTime == 0) {
            throw new StageInitException("keepAliveTime must be set");
        }
        if (maximumPoolSize == 0) {
            throw new StageInitException("maximumPoolSize must be set");
        }
        if (corePoolSize == 0) {
            throw new StageInitException("corePoolSize mst be set");
        }
        return true;
    }

    @Override
    protected void process() {
        ArffFileManager arff = (ArffFileManager) this.data.getData(Type.ARFFFILEMANAGER);
        RawData instances = arff.getInstances();
        int nbOfAttributes = instances.getInstances().get(0).getPoint().length;
        double[][] values = new double[nbOfAttributes][instances.getInstances().size()];
        int pos;
        Set<Integer> attrPos = new HashSet<Integer>();
        for (int j = 0; j < instances.getInstances().size(); j++) {
            for (int i = 0; i < nbOfAttributes; i++) {
                try {
                    pos = new Double(instances.getInstances().get(j).getPoint()[i]).intValue();
                } catch (IndexOutOfBoundsException e) {
                    pos = 0;
                }
                if (instances.getAttributes().get(pos) instanceof NumericAttribute) {
                    values[i][j] = ((NumericAttribute) instances.getAttributes().get(pos)).getValue();
                    attrPos.add(i);
                }
            }
        }
        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
        ThreadPoolExecutor pool = new ThreadPoolExecutor(this.corePoolSize, this.maximumPoolSize,
                this.keepAliveTime, TimeUnit.SECONDS, queue);
        Map<String, RegressionsObject[]> regressions = new HashMap<String, RegressionsObject[]>();
        for (int x = 0; x < nbOfAttributes; x++) {
            if (!attrPos.contains(x)) {
                continue;
            }
            for (int y = x + 1; y < nbOfAttributes; y++) {
                if (!attrPos.contains(y)) {
                    continue;
                }
                double[][] tmp = this.removeZeros(values[x], values[y]);
                if (existsCorrelation(tmp[0], tmp[1])) {
                    String name = arff.getAttributesName().get(x).split("\\s")[0] + "_"
                            + arff.getAttributesName().get(y).split("\\s")[0];
                    regressions.put(name, new RegressionsObject[3]);
                    for (int i = 1; i <= 3; i++) {
                        RegressionsObject rO = new RegressionsObject();
                        rO.setName(name);
                        rO.setX(tmp[0]);
                        rO.setY(tmp[1]);
                        rO.setSourceX(values[x]);
                        rO.setSourceY(values[y]);
                        rO.setDegree(i);
                        regressions.get(name)[i - 1] = rO;
                        queue.add(new MultipleLinearRegression(rO));
                        rO.setMetric(this);
                        threads++;
                    }
                }
            }
        }
        pool.shutdown();
        while (threads > 0) {
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        List<RegressionsObject> choosedRegressions = new LinkedList<RegressionsObject>();

        for (String key : regressions.keySet()) {
            RegressionsObject maxObj = null;
            for (RegressionsObject obj : regressions.get(key)) {
                logger.debug("Regression with degree {} and RSquared {}", obj.getDegree(), obj.getRSquared());
                if (maxObj == null) {
                    maxObj = obj;
                } else {
                    if (obj.getRSquared() > maxObj.getRSquared()) {
                        maxObj = obj;
                    }
                }
            }
            choosedRegressions.add(maxObj);
            logger.debug("Best regression with degree {} for {}", maxObj.getDegree(), key);
        }
        regressions = null;
        for (int i = 0; i < instances.getInstances().size(); i++) {
            for (RegressionsObject obj : choosedRegressions) {
                pos = arff.addAttribute(obj.getName(), IAttribute.Type.NUMERIC);
                arff.addValue(instances.getInstances().get(i),
                        this.distToRegFunc(obj.getSourceX()[i], obj.getSourceY()[i], obj.getParameters()), pos);
            }
        }
    }

    private double distToRegFunc(double x, double y, double[] parameters) {
        double attendedY = 0;
        for (int i = 0; i < parameters.length; i++) {
            attendedY += parameters[i] * Math.pow(x, i);
        }
        return Math.abs(attendedY - y);
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    public synchronized void decThreads() {
        threads--;
    }

    public boolean existsCorrelation(double[] x, double[] y) {
        RealMatrix matrix = new Array2DRowRealMatrix(x.length, 2);
        matrix.setColumn(0, x);
        matrix.setColumn(1, y);
        SpearmansCorrelation correlation = new SpearmansCorrelation(matrix);
        PearsonsCorrelation rankCorrelation = correlation.getRankCorrelation();
        RealMatrix pValues = rankCorrelation.getCorrelationPValues();
        if (pValues.getEntry(0, 1) > 0.05) {
            return false;
        }
        return true;
    }

    public double[][] removeZeros(double[] x, double[] y) {
        List<Double> xTmp = new ArrayList<Double>(x.length);
        List<Double> yTmp = new ArrayList<Double>(y.length);
        for (int i = 0; i < x.length; i++) {
            if (x[i] > 0 && y[i] > 0) {
                xTmp.add(x[i]);
                yTmp.add(y[i]);
            }
        }
        double[][] result = new double[2][xTmp.size()];
        result[0] = Doubles.toArray(xTmp);
        result[1] = Doubles.toArray(yTmp);
        return result;
    }

    public static void main(String[] args) {
        CorrelationsMetric km = new CorrelationsMetric();
        km.existsCorrelation(new double[] { 0.49340322, 0.79496059,
                -0.68712162, 0.66845554, -0.09173911, -0.23594121
                , -0.62231246, -0.23685812, -0.35005990, 0.47955118, 0.21316134, 0.44385886
                , 0.13175161, -0.18895470, -0.51004574, 0.58681645, 0.78219381, -0.59844094
                , 0.63359472, 0.17006361, -0.57468788, 0.85162772, 0.29301792, 0.84327934
                , 0.58386136, 0.33646498, -0.24777461, -0.10589656, 0.09892611, -0.16776640
                , 0.91452790, 0.53679448, -0.61136999, 0.27294710, 0.62499863, -0.67769489
                , -0.98251837, 0.25349979, 0.56143537, -0.59180851, 0.20896734, -0.26318596
                , -0.45813399, -0.73626754, 0.71109035, -0.51609680, -0.52079880, 0.32831630
                , 0.68895010, -0.95813647, -0.01845793, -0.18254618, -0.13859512, -0.69776544
                , 0.85440859, -0.87975478, -0.62416515, -0.83943702, 0.31012688, 0.67979455
                , 0.68776188, -0.50977445, 0.24686782, 0.37550154, 0.07773727, 0.02310573
                , 0.23871756, -0.92512786, -0.64009245, -0.50809534, -0.79551250, 0.67743332
                , 0.55864924, 0.39562551, -0.11609089, -0.38363123, 0.68414192, -0.22407273
                , -0.77280599, 0.81813492, -0.07415953, 0.10625937, 0.70662026, 0.42091154
                , 0.78657247, -0.21403730, -0.84914999, -0.28803945, -0.58412523, 0.30959234
                , -0.37203819, -0.14576356, 0.95860328, 0.57583874, -0.56525616, 0.44787936
                , -0.74490196, -0.31021592, 0.90116509, 0.32700625 },
                new double[] { 0.6765473, 1.0250892, 2.0665731, 0.9026471, 1.0658523, 1.2634417, 2.4248080
                        , 0.9255638, 1.6206335, 0.8680592, 0.9917202, 0.6296754, 0.5546413, 0.7960569
                        , 1.7006551, 0.6331137, 0.8415272, 2.3027067, 0.8074145, 0.5269358, 1.7035120
                        , 1.2391582, 0.7066680, 0.8845870, 0.9792146, 1.0301775, 1.0181556, 1.8446645
                        , 0.9043225, 0.9007392, 0.3488676, 0.8862357, 2.4211565, 0.7023749, 0.7584726
                        , 2.7999312, 2.7207653, 0.8106198, 0.5386773, 2.3645278, 0.8279353, 1.0132547
                        , 2.0013094, 2.2880661, 0.3233408, 1.8425939, 1.3941243, 0.5391330, 1.2610529
                        , 2.5245840, 1.3111907, 1.2069483, 0.7047976, 2.3805697, 1.0436764, 2.9765930
                        , 2.2018759, 2.8162659, 1.1054054, 0.4511302, 1.3978801, 1.8883473, 1.1778392
                        , 0.6968529, 0.6435631, 0.8264613, 1.0660275, 3.0613024, 2.0285253, 1.8776068
                        , 2.3607947, 0.8201065, 0.8242999, 0.4345053, 1.2360749, 1.7018505, 0.5470571
                        , 1.5151091, 2.4528894, 0.9778121, 0.9367866, 1.3222543, 0.4451325, 1.3983542
                        , 1.3073283, 1.3086033, 2.4608884, 0.9939155, 2.1938024, 1.1190209, 1.6554159
                        , 1.0070377, 1.3771814, 1.1235808, 1.8505279, 0.5318170, 2.5464980, 1.4339813
                        , 0.8144664, 1.2118289 });
    }

    public void setKeepAliveTime(long keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

}
