package ontologymetrics.metrics.numericmetrics;

import java.util.Arrays;

import lombok.Data;

import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

public class MultipleLinearRegression implements Runnable {
    private double[] x;
    private double[] y;
    private int degree;
    private RegressionsObject results;

    public MultipleLinearRegression(RegressionsObject results) {
        this.results = results;
        this.x = this.results.getX();
        this.y = this.results.getY();
        this.degree = this.results.getDegree();
    }

    @Override
    public void run() {
        double[][] xsample = new double[x.length][degree];
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < degree; j++) {
                xsample[i][j] = Math.pow(x[i], j+1);
            }
        }
        OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();
        regression.newSampleData(y, xsample);
        results.setRSquared(regression.calculateAdjustedRSquared());
        results.setParameters(regression.estimateRegressionParameters());
        results.getMetric().decThreads();
        System.out.println(results.getParameters());
    }

    public static void main(String[] args) {
        OLSMultipleLinearRegression r = new OLSMultipleLinearRegression();
        r.newSampleData(new double[] { 106, 7, 86, 0, 100, 27, 101, 50, 99, 28, 103, 29, 97, 20, 113, 12, 112, 6, 110,
                17 }, 10, 1);
        System.out.println(Arrays.toString(r.estimateRegressionParameters()));
    }
}
