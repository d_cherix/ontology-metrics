package ontologymetrics.metrics.numericmetrics;

import java.util.List;
import java.util.regex.Pattern;

import lombok.Setter;
import ontologymetrics.core.data.DatatypeProperty;
import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.data.IAttribute.Type;
import ontologymetrics.core.data.Instance;
import ontologymetrics.core.data.LiteralResource;
import ontologymetrics.core.data.UriResource;
import ontologymetrics.core.data.UriResourceBuilder;
import ontologymetrics.core.exceptions.DBException;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.stages.ArffFileManager;
import ontologymetrics.metrics.AbstractMetric;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class NumericMetric extends AbstractMetric {


    private static final Logger logger = LoggerFactory.getLogger(NumericMetric.class);

    @Autowired
    @Setter
    private GraphDBDao graphDb;
    @Setter
    private boolean multipleValues = false;

    @Override
    protected boolean checkRequires() throws StageInitException {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    protected void processInstance(Instance instance, ArffFileManager arff) throws DBException {
        UriResource instanceUri = UriResourceBuilder.uriResource().uri(instance.getName()).build();
        List<DatatypeProperty> dataProps = graphDb.listDatatypeProperties(instanceUri);
        for (DatatypeProperty datatypeProperty : dataProps) {
            UriResource uri = datatypeProperty.getProperty();
            if (consideredProperties != null && !consideredProperties.contains(uri) && !consideredProperties.isEmpty()) {
                continue;
            }
            if (!pattern.matcher(uri.getUri().toString()).matches()) {
                continue;
            }

            Object value = datatypeProperty.getLiteral().getValue();

            if (value instanceof Double) {
                int pos = arff.addAttribute("num:" + datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                arff.addValue(instance, (Double) value, pos);
            } else if (value instanceof Float) {
                int pos = arff.addAttribute("num:" + datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                arff.addValue(instance, (Float) value, pos);
            } else if (value instanceof Integer) {
                int pos = arff.addAttribute("num:" + datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                arff.addValue(instance, (Integer) value, pos);
            } else if (value instanceof Long) {
                int pos = arff.addAttribute("num:" + datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                arff.addValue(instance, (Long) value, pos);
            } else if (value instanceof String){
                try {
                Double doubleValue = Double.valueOf((String)value);
                int pos = arff.addAttribute("num:" + datatypeProperty.getProperty().getUri(), Type.NUMERIC);
                arff.addValue(instance, (Double) doubleValue, pos);
                } catch (NumberFormatException e){
                    logger.warn("Number Format Exception on: {}",value);
                }
            }

        }

    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
