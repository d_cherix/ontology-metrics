package ontologymetrics.metrics.numericmetrics;

import java.util.Arrays;

public class RegressionsObject{
    private CorrelationsMetric metric;
    private double rSquared;
    private double[] parameters;
    private String name;
    private double[]x;
    private double[]y;
    private int degree;
    private double[] sourceX;
    private double[] sourceY;
    
    
    
    public CorrelationsMetric getMetric() {
        return metric;
    }
    public void setMetric(CorrelationsMetric metric) {
        this.metric = metric;
    }
    public double getRSquared() {
        return rSquared;
    }
    public void setRSquared(double rSquared) {
        this.rSquared = rSquared;
    }
    public double[] getParameters() {
        return parameters;
    }
    public void setParameters(double[] parameters) {
        this.parameters = parameters;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double[] getX() {
        return x;
    }
    public void setX(double[] x) {
        this.x = x;
    }
    public double[] getY() {
        return y;
    }
    public void setY(double[] y) {
        this.y = y;
    }
    public int getDegree() {
        return degree;
    }
    public void setDegree(int degree) {
        this.degree = degree;
    }
    public double[] getSourceX() {
        return sourceX;
    }
    public void setSourceX(double[] sourceX) {
        this.sourceX = sourceX;
    }
    public double[] getSourceY() {
        return sourceY;
    }
    public void setSourceY(double[] sourceY) {
        this.sourceY = sourceY;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + degree;
        result = prime * result + ((metric == null) ? 0 : metric.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + Arrays.hashCode(parameters);
        long temp;
        temp = Double.doubleToLongBits(rSquared);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + Arrays.hashCode(sourceX);
        result = prime * result + Arrays.hashCode(sourceY);
        result = prime * result + Arrays.hashCode(x);
        result = prime * result + Arrays.hashCode(y);
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RegressionsObject other = (RegressionsObject) obj;
        if (degree != other.degree)
            return false;
        if (metric == null) {
            if (other.metric != null)
                return false;
        } else if (!metric.equals(other.metric))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (!Arrays.equals(parameters, other.parameters))
            return false;
        if (Double.doubleToLongBits(rSquared) != Double.doubleToLongBits(other.rSquared))
            return false;
        if (!Arrays.equals(sourceX, other.sourceX))
            return false;
        if (!Arrays.equals(sourceY, other.sourceY))
            return false;
        if (!Arrays.equals(x, other.x))
            return false;
        if (!Arrays.equals(y, other.y))
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "RegressionsObject [metric=" + metric + ", rSquared=" + rSquared + ", parameters="
                + Arrays.toString(parameters) + ", name=" + name + ", x=" + Arrays.toString(x) + ", y="
                + Arrays.toString(y) + ", degree=" + degree + ", sourceX=" + Arrays.toString(sourceX) + ", sourceY="
                + Arrays.toString(sourceY) + "]";
    }
}
