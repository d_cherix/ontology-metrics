package ontologymetrics.metrics.textmetrics;

import java.util.List;

import ontologymetrics.core.data.IAttribute.Type;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.core.sparql.SparqlPrefixes;
import ontologymetrics.core.stages.ArffFileManager;

import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.helpers.collection.IteratorUtil;
import org.springframework.beans.factory.annotation.Autowired;
@Deprecated
public class NumberOfConnectedInstances extends
		AbstractPipelineStage {

	@Autowired
	private SparqlPrefixes prefixes;
	private String objectProperty = null;

	@Override
	protected boolean prerequisitesFulfilled() throws StageInitException {
//		if (in == null) {
//			throw new ComponentInitException("Component " + name
//					+ " need a list of ArffFileManager to process");
//		}
		if (objectProperty == null) {
			throw new StageInitException("Component " + name
					+ " need an objectProperty to process");
		}
		objectProperty = prefixes.getPrefixes().expandPrefix(objectProperty);
		return true;
	}

	@Override
	protected void process() {
//		for (ArffFileManager arff : in) {
//			int pos = arff.addAttribute(name + "_" + objectProperty,Type.NUMERIC);
//			Iterable<Relationship> instances;
//			for (Node node : arff.getInstances()) {
//				instances = node.getRelationships(DynamicRelationshipType
//						.withName(objectProperty));
//				arff.addValue(node, IteratorUtil.count(instances),pos);
//			}
//		}

	}

	/**
	 * @param objectProperty the objectProperty to set
	 */
	public void setObjectProperty(String objectProperty) {
		this.objectProperty = objectProperty;
	}

	@Override
	protected void finalise() {
		// TODO Auto-generated method stub

	}

}
