package ontologymetrics.qr.controller;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ontologymetrics.core.sparql.QueryExecutor;
import ontologymetrics.core.sparql.SparqlPrefixes;
import ontologymetrics.qr.data.Evaluation;
import ontologymetrics.qr.data.EvaluationDAO;
import ontologymetrics.qr.data.Graph;
import ontologymetrics.qr.data.Property;
import ontologymetrics.qr.data.Resource;
import ontologymetrics.qr.data.VizNode;

import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hp.hpl.jena.query.ParameterizedSparqlString;

@Controller
@SessionAttributes({ "uri", "evaluation" })
public class EvaluationController {

    private static final Logger logger = LoggerFactory.getLogger(EvaluationController.class);

    @Autowired
    private QueryExecutor executor;
    @Autowired
    private SparqlPrefixes prefixes;
    private ParameterizedSparqlString queryString = null;
    @Autowired
    private EvaluationDAO dao;
    @Value("${controller.description}")
    private String description;

    @PreAuthorize("hasRole('user')")
    @RequestMapping("/")
    public String login(Model model) {
        return this.visualize(model);
    }

    @PreAuthorize("hasRole('user')")
    @RequestMapping("/visualize")
    public String visualize(Model model) {
        String userName = "";
        Resource inst = this.getNewInstance();
        if (inst != null) {
            String uri = inst.getUri();
            List<Property> outgoing = new LinkedList<Property>();
            List<Property> incoming = new LinkedList<Property>();
            Graph g = this.getData(uri);
            for (VizNode n : g.getVizNode().getChildren()) {
                if (g.getOutgoing().contains(n.getId())) {
                    for (VizNode child : n.getChildren()) {
                        outgoing.add(new Property(n.getName(), child.getData().get("value").toString(), prefixes
                                .getPrefixes().expandPrefix(n.getName()), prefixes.getPrefixes().expandPrefix(
                                child.getData().get("value").toString())));
                    }
                } else {
                    for (VizNode child : n.getChildren()) {
                        incoming.add(new Property(n.getName(), child.getData().get("value").toString(), prefixes
                                .getPrefixes().expandPrefix(n.getName()), prefixes.getPrefixes().expandPrefix(
                                child.getData().get("value").toString())));
                    }
                }
            }
            logger.debug("outgoing {}", outgoing);
            model.addAttribute("outgoing", outgoing);
            model.addAttribute("incoming", incoming);
            model.addAttribute("userName", userName);
            model.addAttribute("prefixes", prefixes.prefixesMap());
            model.addAttribute("uri", uri);
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String name = auth.getName();
            model.addAttribute("evaluation", new Evaluation(name, uri, description));
            return "visualize";
        } else {
            return "empty";
        }
    }

    private Resource getNewInstance() {
        return dao.nextInstance(this.description);
    }

    @PreAuthorize("hasRole('user')")
    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(Model model, @ModelAttribute Evaluation evaluation) {
        dao.save(evaluation, description);
        return this.visualize(model);
    }

    @Cacheable("graph")
    @RequestMapping("/datajson")
    public @ResponseBody
    Graph getData(@RequestParam String uri) {
        if (queryString == null) {
            queryString = new ParameterizedSparqlString(prefixes.addPrefixesToQuery(
                    "SELECT DISTINCT * WHERE { " + "{ ?instance ?p ?o }"
                            + " UNION " + "{ ?s ?pp ?instance } " + "}"));
        }

        int i = 0;
        Graph graph = new Graph();
        VizNode node = new VizNode(i, prefixes.getPrefixes().shortForm(uri));
        graph.setVizNode(node);
        node.getData().put("$color", "#FFFFFF");
        i++;
        queryString.setIri("instance", uri);
        TupleQueryResult result = executor.executeSelectQuery(queryString.toString(),
                "");
        BindingSet solution;
        Map<String, VizNode> children = new HashMap<String, VizNode>();
        try {
            while (result.hasNext()) {
                solution = result.next();
                if (solution.hasBinding("p")) {
                    String p = prefixes.shortForm(solution.getBinding("p").getValue().stringValue());
                    if (!children.containsKey(p)) {
                        VizNode child = new VizNode(i, p);
                        child.getData().put("$color", "#8A0829");
                        children.put(p, child);
                        graph.addOutgoing(i);
                        i++;
                    }
                    String o;
                    VizNode child = null;

                    o = solution.getBinding("o").getValue().stringValue();

                    child = new VizNode(i, o.substring(0, Math.min(20, o.length())));
                    child.getData().put("value", o);
                    child.getData().put("$color", "#8A0829");
                    children.get(p).addChild(child);
                    graph.addOutgoing(i);
                    i++;
                } else {
                    String pp = prefixes.shortForm(solution.getBinding("pp").getValue().stringValue());
                    if (!children.containsKey(pp)) {
                        VizNode child = new VizNode(i, pp);
                        child.getData().put("$color", "#0B610B");
                        children.put(pp, child);
                        graph.addIncoming(i);
                        i++;
                    }
                    String s;
                    VizNode child = null;
                    s = prefixes.shortForm(
                            solution.getBinding("s").getValue().stringValue());
                    child = new VizNode(i, s.substring(0, Math.min(20, s.length())));
                    child.getData().put("value", s);
                    child.getData().put("$color", "#0B610B");
                    children.get(pp).addChild(child);
                    graph.addIncoming(i);
                    i++;
                }
            }
        } catch (QueryEvaluationException e) {
           logger.error("Exception while generating page {}",e);
        }
        for (VizNode child : children.values()) {
            node.addChild(child);
        }
        return graph;
    }
}
