package ontologymetrics.qr.controller;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import ontologymetrics.qr.data.EvaluationDAO;

import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.bytecode.opencsv.CSVReader;

public class ExportToEvaluate {
    public static void main(String[] args) throws IOException {

        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext-jdbc.xml");
        EvaluationDAO dao = BeanFactoryUtils.beanOfType(context, EvaluationDAO.class);

        CSVReader reader = new CSVReader(new FileReader(args[0]));
        try {
            String[] nextLine=null;
          
            while ((nextLine = reader.readNext()) != null) {
                if (nextLine[1].equals("fp") || nextLine[1].equals("fn")) {
                    dao.save(nextLine[0], args[1], nextLine[1]);
                }
            }
        } finally {
            reader.close();
        }
    }

}
