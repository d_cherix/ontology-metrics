package ontologymetrics.qr.data;

public class Evaluation {
    private String evaluation;
    private String username;
    private String uri;
    private String description;

    public Evaluation(String username, String uri, String description) {
        super();
        this.username = username;
        this.uri = uri;
        this.description=description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
