package ontologymetrics.qr.data;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class EvaluationDAO {
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterTemplate;

    @Autowired
    public void init(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `evaluation` (" +
                "`id` int(11) NOT NULL AUTO_INCREMENT," +
                "`username` VARCHAR(50)," +
                "`evaluation` VARCHAR(50)," +
                "`uri` VARCHAR(500)," +
                "`description` VARCHAR(500)," +
                "`timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
                "PRIMARY KEY (`id`)" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8");

        this.jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS `clusters` (" +
                "`id` int(11) NOT NULL AUTO_INCREMENT," +
                "`uri` VARCHAR(500)," +
                "`description` VARCHAR(500)," +
                "`category`  VARCHAR(2)," +
                "`timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
                "PRIMARY KEY (`id`)" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
    }

    public void save(Evaluation eval, String description) {
        this.jdbcTemplate.update(
                "INSERT INTO `evaluation` (`username`, `evaluation`,`uri`, `description`) VALUES (?,?,?,?)",
                new Object[] {
                        eval.getUsername(), eval.getEvaluation(), eval.getUri(), eval.getDescription()
                });
        this.jdbcTemplate.update("UPDATE `clusters` set evaluated=1 WHERE uri=? AND description=?", new Object[] {
                eval.getUri(), eval.getDescription()
        });
    }

    public void save(String uri, String description, String category) {
        this.jdbcTemplate.update("INSERT INTO `clusters` (`uri`, `description`, `category`) VALUES (?,?,?)",
                new Object[] {
                        uri, description, category
                });

    }

    public Resource nextInstance(String description) {
        try {
            return this.jdbcTemplate.queryForObject("SELECT * from clusters WHERE evaluated IS NULL AND description='"
                    + description
                    + "' LIMIT 1", new RowMapper<Resource>() {

                @Override
                public Resource mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Resource i = new Resource();
                    i.setCategory(rs.getString("category"));
                    i.setId(rs.getInt("id"));
                    i.setUri(rs.getString("uri"));
                    return i;
                }

            });
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
