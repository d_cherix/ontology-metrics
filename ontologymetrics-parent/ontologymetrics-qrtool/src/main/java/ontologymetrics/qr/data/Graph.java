package ontologymetrics.qr.data;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Graph implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 4943695470025099290L;
    private VizNode vizNode;
    private List<Integer> incoming;
    private List<Integer> outgoing;
    
    public Graph(){
    	this.incoming=new LinkedList<Integer>();
    	this.outgoing=new LinkedList<Integer>();
    }
    
	/**
	 * @return the vizNode
	 */
	public VizNode getVizNode() {
		return vizNode;
	}
	/**
	 * @param vizNode the vizNode to set
	 */
	public void setVizNode(VizNode vizNode) {
		this.vizNode = vizNode;
	}
	/**
	 * @return the incoming
	 */
	public List<Integer> getIncoming() {
		return incoming;
	}
	/**
	 * @param incoming the incoming to set
	 */
	public void setIncoming(List<Integer> incoming) {
		this.incoming = incoming;
	}
	
	public void addIncoming(int node){
		this.incoming.add(node);
	}
	/**
	 * @return the outgoing
	 */
	public List<Integer> getOutgoing() {
		return outgoing;
	}
	/**
	 * @param outgoing the outgoing to set
	 */
	public void setOutgoing(List<Integer> outgoing) {
		this.outgoing = outgoing;
	}
	
	public void addOutgoing(int node){
		this.outgoing.add(node);
	}
}
