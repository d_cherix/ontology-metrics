package ontologymetrics.qr.data;

/**
 * This class respresents a property in the quality rater tool.
 * @author d.cherix
 *
 */
public class Property {
    private String shortUri;
    private String uri;
    private String valueString;
    private String valueUri;

    /**
     * Constructor.
     */
    public Property() {
        super();
    }
    
    /**
     * Constructor.
     * @param uri uri of the property
     * @param value the linked object, literal or uri.
     */
    public Property(String uri, String value) {
        super();
        this.shortUri = uri;
        this.valueString = value;
    }
    
    /**
     * Constructor
     * @param shortUri an shorted version of the uri (preifxes)
     * @param valueString the value String
     * @param uri the complete uri
     * @param valueUri the uri of the value
     */
    public Property( String shortUri, String valueString,String uri, String valueUri) {
        super();
        this.shortUri = shortUri;
        this.uri = uri;
        this.valueString = valueString;
        this.valueUri = valueUri;
    }
    @Override
    public String toString() {
        return "Property [uri=" + shortUri + ", valueString=" + valueString + "]";
    }
    public String getShortUri() {
        return shortUri;
    }
    public void setShortUri(String uri) {
        this.shortUri = uri;
    }
    public String getValueString() {
        return valueString;
    }
    public void setValueString(String valueString) {
        this.valueString = valueString;
    }
    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }
    public String getValueUri() {
        return valueUri;
    }
    public void setValueUri(String valueUri) {
        this.valueUri = valueUri;
    }
   
    
}
