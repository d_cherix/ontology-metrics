/**
 * 
 */
package ontologymetrics.qr.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author didier
 *
 */
public class VizNode implements Serializable {
	/**
     * 
     */
    private static final long serialVersionUID = 5803784937803626410L;

	
	private int id;
	private String name;
	private Map<String,Object> data;
	private List<VizNode> children;
	
	/**
	 * @param id
	 * @param name
	 */
	public VizNode(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.data= new HashMap<String, Object>();
		this.children = new LinkedList<VizNode>();
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the data
	 */
	public Map<String, Object> getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	/**
	 * @return the children
	 */
	public List<VizNode> getChildren() {
		return children;
	}
	/**
	 * @param children the children to set
	 */
	public void setChildren(List<VizNode> children) {
		this.children = children;
	}

	public void addChild(VizNode child){
		this.children.add(child);
	}
}
