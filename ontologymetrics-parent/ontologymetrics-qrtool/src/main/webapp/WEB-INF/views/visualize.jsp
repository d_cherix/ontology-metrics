<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Hypertree - Graph Operations</title>

<!-- CSS Files -->
<link type="text/css" href="css/base.css" rel="stylesheet" />
<link type="text/css" href="css/Hypertree.css" rel="stylesheet" />

<!--[if IE]><script language="javascript" type="text/javascript" src="../../Extras/excanvas.js"></script><![endif]-->

<!-- JIT Library File -->
<script language="javascript" type="text/javascript" src="js/jit-yc.js"></script>

<!-- Example File -->
<script language="javascript" type="text/javascript"
	src="js/example1.js"></script>
<script language="javascript" type="text/javascript"
	src="js/jquery-1.10.1.min.js"></script>
</head>

<body onload="init('${uri}');">
	<div id="container">
		<div id="left-container">
			<h4>Outgoing</h4>
			<div class="CSSTableGenerator scrollingArea">
				<table>
					<tr>
						<td>Property</td>
						<td>Value</td>
					</tr>
					<c:forEach var="property" items="${outgoing}">
						<tr>
							<td><a href="${property.uri}" target="_blank">${property.shortUri}</a></td>
							<c:set var="valueUri" value="${property.valueUri}" />
							<c:choose>
								<c:when test="${fn:startsWith(valueUri, 'http')==true}">
									<td><a href="${property.valueUri}" target="_blank">${property.valueString}</a></td>
								</c:when>
								<c:otherwise>
									<td>${property.valueString}</td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
				</table>
			</div>
			<h4>Incoming</h4>
			<div class="CSSTableGenerator scrollingArea">
				<table>
					<tr>
						<td>Value</td>
						<td>Property</td>
					</tr>
					<c:forEach var="property" items="${incoming}">
						<tr>
							<c:set var="valueUri" value="${property.valueUri}" />
							<c:choose>
								<c:when test="${fn:startsWith(valueUri, 'http')==true}">
									<td><a href="${property.valueUri}" target="_blank">${property.valueString}</a></td>
								</c:when>
								<c:otherwise>
									<td>${property.valueString}</td>
								</c:otherwise>
							</c:choose>
							<td><a href="${property.uri}" target="_blank">${property.shortUri}</a></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
		<div id="center-container">
			<div id="infovis"></div>
		</div>

		<div id="right-container">

			<h4>Remove Nodes</h4>
			<div id="remove" class="removeArea">
				<table>
					<tr>
						<td>type:</td>
						<td><select id="select-type-remove-nodes">
								<option>incoming</option>
								<option>outgoing</option>
						</select></td>
					</tr>
					<tr>
						<td><input type="button" value="Remove" id="remove-nodes" />
						</td>
						<td><input type="button" value="Refresh"
							onclick="window.location = window.location" /></td>
					</tr>
				</table>
			</div>
			<input type="button" value="Hide graph" id="hide" /> <input
				type="button" value="Show graph" id="show" />
			<h4>Evaluate</h4>
			<div class="removeArea">
				<form:form method="post" action="save.html" commandName="evaluation">
					<p>
						<form:radiobutton path="evaluation" value="Right" />
						Right<br> <form:radiobutton path="evaluation" value="False" />
							False 
					</p>
					<p>
						<input class="button" type="submit" value="Save" />
					</p>
				</form:form>
			</div>
			<h4>Prefixes</h4>
			<c:forEach var="prefix" items="${prefixes}">
				<p>
					<b>${prefix.key}:</b>${prefix.value}
				</p>
			</c:forEach>
			<a href="j_spring_security_logout">Logout</a>
		</div>
		<div id="log" style="visibility: hidden;"></div>
	</div>
	<div id="header">
		<h1>${uri}</h1>
	</div>
</body>
<script language="javascript" type="text/javascript">
	$("#show").hide();
	$("#hide").click(function() {
		$("#center-container").hide();
		$("#left-container").css('margin', '0 200px 0 0');
		$("#hide").hide();
		$("#remove").hide();
		$("#show").show();
	})
	$("#show").click(function() {
		$("#center-container").show();
		$("#left-container").css('margin', '0 900px 0 0');
		$("#hide").show();
		$("#remove").show();
		$("#show").hide();
	})
</script>
</html>
