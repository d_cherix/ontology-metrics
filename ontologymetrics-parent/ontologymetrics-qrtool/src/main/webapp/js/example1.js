var labelType, useGradients, nativeTextSupport, animate;

(function() {
	var ua = navigator.userAgent, iStuff = ua.match(/iPhone/i)
			|| ua.match(/iPad/i), typeOfCanvas = typeof HTMLCanvasElement, nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'), textSupport = nativeCanvasSupport
			&& (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
	// I'm setting this based on the fact that ExCanvas provides text support
	// for IE
	// and that as of today iPhone/iPad current text support is lame
	labelType = (!nativeCanvasSupport || (textSupport && !iStuff)) ? 'Native'
			: 'HTML';
	nativeTextSupport = labelType == 'Native';
	useGradients = nativeCanvasSupport;
	animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
	elem : false,
	write : function(text) {
		if (!this.elem)
			this.elem = document.getElementById('log');
		this.elem.innerHTML = text;
		this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
	}
};

function init(uri) {

	var infovis = document.getElementById('infovis');
	var w = infovis.offsetWidth - 50, h = infovis.offsetHeight - 50;
	var incoming;
	var outgoing

	// init Hypertree
	var ht = new $jit.Hypertree(
			{
				// id of the visualization container
				injectInto : 'infovis',
				// canvas width and height
				width : w,
				height : h,
				// Change node and edge styles such as
				// color, width and dimensions.
				Node : {
					overridable : true,
					dim : 9
				},
				Edge : {
					lineWidth : 2,
					color : "#088"
				},
				onBeforeCompute : function(node) {
					Log.write("centering");
				},
				// Attach event handlers and add text to the
				// labels. This method is only triggered on label
				// creation
				onCreateLabel : function(domElement, node) {
					domElement.innerHTML = node.name;
					$jit.util.addEvent(domElement, 'click', function() {
						ht.onClick(node.id, {
							onComplete : function() {
								ht.controller.onComplete();
							}
						});
					});
				},
				// Change node styles when labels are placed
				// or moved.
				onPlaceLabel : function(domElement, node) {
					var style = domElement.style;
					style.display = '';
					style.cursor = 'pointer';
					if (node._depth <= 1) {
						style.fontSize = "0.8em";
						style.color = "#330088";

					} else if (node._depth == 2) {
						style.fontSize = "0.7em";
						style.color = "#555";

					} else {
						style.display = 'none';
					}

					var left = parseInt(style.left);
					var w = domElement.offsetWidth;
					style.left = (left - w / 2) + 'px';
				},

				onComplete : function() {
					Log.write("done");

					// Build the right column relations list.
					// This is done by collecting the information (stored in the
					// data
					// property)
					// for all the nodes adjacent to the centered node.
					var node = ht.graph.getClosestNodeToOrigin("current");
					var html = "<h4>" + node.name + "</h4><b>Connections:</b>";
					html += "<ul>";
					node
							.eachAdjacency(function(adj) {
								var child = adj.nodeTo;
								if (child.data) {
									var rel = (child.data.band == node.name) ? child.data.relation
											: node.data.relation;
									html += "<li>"
											+ child.name
											+ " "
											+ "<div class=\"relation\">(relation: "
											+ rel + ")</div></li>";
								}
							});
					html += "</ul>";
					$jit.id('inner-details').innerHTML = html;
				},
				Tips : {
					enable : true,
					type : 'Native',
					// add positioning offsets
					offsetX : 20,
					offsetY : 20,
					// implement the onShow method to
					// add content to the tooltip when a node
					// is hovered
					onShow : function(tip, node) {
						var html = "<div class=\"tip-title\">" + node.name
								+ "</div><div class=\"tip-text\">";
						var data = node.data;
						if (data.value) {
							html += "Value: " + data.value + "<br />";
						}
						tip.innerHTML = html;
					}
				}
			});
	// load JSON data.
	$.getJSON('datajson?uri='+uri,
			function(data) {
				var json = data.vizNode;
				incoming = data.incoming;
				outgoing = data.outgoing;
				ht.loadJSON(json);

				ht.refresh();
				// end
				ht.controller.onComplete();
			});
	// Global Options
	// Define a function that returns the selected duration
	function getDuration() {
		var sduration = document.getElementById('select-duration');
		var sdindex = sduration.selectedIndex;
		return parseInt(sduration.options[sdindex].text);
	}
	;
	// Define a function that returns the selected fps
	function getFPS() {
		var fpstype = document.getElementById('select-fps');
		var fpsindex = fpstype.selectedIndex;
		return parseInt(fpstype.options[fpsindex].text);
	}
	;
	// Define a function that returns whether you have to
	// hide labels during the animation or not.
	function hideLabels() {
		return document.getElementById('hide-labels').checked;
	}
	;

	// Remove Nodes
	var button = $jit.id('remove-nodes');
	button.onclick = function() {
		// get animation type.
		var stype = $jit.id('select-type-remove-nodes');
		var sindex = stype.selectedIndex;
		var type = stype.options[sindex].text;
		if (sindex == 0) {
				ht.op.removeNode(incoming, {
					type : 'replot',
					duration : 1000,
					hideLabels : true,
					transition : $jit.Trans.Quart.easeOut
				});
		} else {
				ht.op.removeNode(outgoing, {
					type : 'replot',
					duration : 1000,
					hideLabels : true,
					transition : $jit.Trans.Quart.easeOut
				});
		}
	};
}