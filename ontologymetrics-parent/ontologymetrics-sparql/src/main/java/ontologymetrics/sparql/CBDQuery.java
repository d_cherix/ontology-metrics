package ontologymetrics.sparql;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import ontologymetrics.core.sparql.QueryExecutor;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.openrdf.model.Graph;
import org.openrdf.model.Model;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class CBDQuery {

    private static final Logger logger = LoggerFactory.getLogger(CBDQuery.class);
    @Autowired
    protected SparqlPrefixes prefixes;
    @Autowired
    protected QueryExecutor queryExecutor;
    protected String graph = "";
    protected String listInstancesQuery;

    @Autowired
    public void init() {
        listInstancesQuery =
                        "SELECT ?instance WHERE { { SELECT DISTINCT ?instance WHERE { ?instance a <%s> } ORDER BY ASC(?instance) }} LIMIT %d OFFSET %d ";
    }

    public List<String> queryInstancesOf(String owlClass, int limit, int offset) {
        String queryString = String.format(listInstancesQuery, owlClass,limit,offset);
        TupleQueryResult resultSet = queryExecutor.executeSelectQuery(queryString, graph);
        List<String> instances = new LinkedList<String>();
        try{
        while (resultSet.hasNext()) {
            instances.add(resultSet.next().getBinding("instance").getValue().stringValue());
        }
        } catch (QueryEvaluationException e){
            logger.error("error qhile evaluating query",e);
        }
        return instances;

    }

    public Model CBDquery(String instance, int depth) {
        StringBuilder builder = new StringBuilder();
        builder.append("CONSTRUCT {");
        for (int i = 0; i < depth; i++) {
            if (i == 0) {
                builder.append("<");builder.append(instance);builder.append(">");
            } else {
                builder.append(" ?o");
                builder.append(i - 1);
            }

            builder.append(" ?p");
            builder.append(i);
            builder.append(" ?o");
            builder.append(i);
            builder.append(" .");
        }
        builder.append(" ?o");
        builder.append(depth - 1);
        builder.append("?x ?z .");
        builder.append("} WHERE {");
        for (int i = 0; i < depth; i++) {
            if (i == 0) {
                builder.append(" <");builder.append(instance);builder.append(">");
            } else {
                builder.append("OPTIONAL { ");

                builder.append("?o");
                builder.append(i - 1);
            }
            builder.append(" ?p");
            builder.append(i);
            builder.append(" ?o");
            builder.append(i);
            if (i > 0) {
                builder.append("} ");
            } else {
                builder.append(" .");
            }
        }
        builder.append(" OPTIONAL { ?o");
        builder.append(depth - 1);
        builder.append("?x ?z . FILTER(isBlank(?o");
        builder.append(depth - 1);
        builder.append(") && !isBlank(?z)) }");
        builder.append("}");
        return queryExecutor.executeConstructQuery(builder.toString(), graph);
    }

    public static void main(String[] args) {

        QueryExecutorImpl qExec = new QueryExecutorImpl();
        qExec.setEndpoint("http://localhost:8890/sparql");

        CBDQuery q = new CBDQuery();
        q.setPrefixes(new SparqlPrefixes(new HashMap<String, String>()));
        q.setQueryExecutor(qExec);
        q.init();
        List<String> instances = q.queryInstancesOf("http://swat.cse.lehigh.edu/onto/univ-bench.owl#FullProfessor",
                100, 0);
        Graph m = q.CBDquery(instances.get(0), 2);
        
    }

    public SparqlPrefixes getPrefixes() {
        return prefixes;
    }

    public void setPrefixes(SparqlPrefixes prefixes) {
        this.prefixes = prefixes;
    }

    public String getGraph() {
        return graph;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }

    public void setQueryExecutor(QueryExecutor queryExecutor) {
        this.queryExecutor = queryExecutor;
    }

}
