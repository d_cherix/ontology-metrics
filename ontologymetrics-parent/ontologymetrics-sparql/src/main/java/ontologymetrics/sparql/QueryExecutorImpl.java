package ontologymetrics.sparql;

import ontologymetrics.core.sparql.QueryExecutor;

import org.openrdf.model.Graph;
import org.openrdf.model.Model;
import org.openrdf.model.impl.LinkedHashModel;
import org.openrdf.query.BooleanQuery;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.QueryResults;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class QueryExecutorImpl.
 */
public class QueryExecutorImpl implements QueryExecutor {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(QueryExecutorImpl.class);

    /** The endpoint. */
    private String endpoint;

    private HTTPRepository repository;

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.sparql.QueryExecutor#executeConstructQuery(java.lang.String,
     * java.lang.String)
     */
    @Override
    public Model executeConstructQuery(String queryString, String graphName) {
        RepositoryConnection con = null;
        GraphQuery query = null;
        GraphQueryResult result = null;
        Model model=null;
        try {
            con = repository.getConnection();
            query = con.prepareGraphQuery(QueryLanguage.SPARQL, queryString);
            result = query.evaluate();
             model = QueryResults.asModel(result);
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedQueryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (QueryEvaluationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (RepositoryException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return model;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.sparql.QueryExecutor#executeSelectQuery(java.lang.String,
     * java.lang.String)
     */
    @Override
    public TupleQueryResult executeSelectQuery(String queryString, String graph) {
        RepositoryConnection con = null;
        TupleQuery query = null;
        TupleQueryResult result = null;
        try {
            con = repository.getConnection();
            query = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
            result = query.evaluate();
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedQueryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (QueryEvaluationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (RepositoryException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public boolean executeAskQuery(String queryString, String graph) {
        RepositoryConnection con = null;
        BooleanQuery query = null;
        boolean result=false;
        try {
            con = repository.getConnection();
            query = con.prepareBooleanQuery(QueryLanguage.SPARQL, queryString);
            result = query.evaluate();
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedQueryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (QueryEvaluationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (RepositoryException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * @return the endpoint
     */
    public String getEndpoint() {
        return endpoint;
    }

    /**
     * @param endpoint
     *            the endpoint to set
     */
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
        this.repository = new HTTPRepository(endpoint);
    }

}
