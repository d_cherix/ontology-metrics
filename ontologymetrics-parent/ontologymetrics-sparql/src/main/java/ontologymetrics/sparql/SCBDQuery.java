package ontologymetrics.sparql;

import java.util.HashMap;
import java.util.List;

import ontologymetrics.core.sparql.SparqlPrefixes;

import org.openrdf.model.Graph;
import org.openrdf.model.Model;

public class SCBDQuery extends CBDQuery {

    public SCBDQuery() {
        super();
    }

    @Override
    public Model CBDquery(String instance, int depth) {
        Model m = super.CBDquery(instance, depth);
        StringBuilder builder = new StringBuilder();
        builder.append("CONSTRUCT {");
        for (int i = 0; i < depth; i++) {
            builder.append("?o");
            builder.append(i);
            builder.append(" ?p");
            builder.append(i);
            if (i == 0) {
                builder.append(" <");
                builder.append(instance);
                builder.append(">");
            } else {
                builder.append(" ?o");
                builder.append(i - 1);
            }
            builder.append(" .");
            builder.append(" ?o");
            builder.append(i);
            builder.append(" ?x");
            builder.append(i);
            builder.append(" ?z");
            builder.append(i);
            builder.append(" . ");
        }
        builder.append("} WHERE {");
        for (int i = 0; i < depth; i++) {
            if (i == 0) {
                builder.append(" ?o0");
            } else {
                builder.append("OPTIONAL { ");
                builder.append("?o");
                builder.append(i);
            }
            builder.append(" ?p");
            builder.append(i);
            if (i == 0) {
                builder.append(" <");
                builder.append(instance);
                builder.append(">");
            } else {
                builder.append(" ?o");
                builder.append(i - 1);
            }
            if (i > 0) {
                builder.append("} ");
            } else {
                builder.append(" .");
            }
            builder.append(" OPTIONAL { ?o");
            builder.append(i);
            builder.append("?x");
            builder.append(i);
            builder.append("?z");
            builder.append(i);
            builder.append(" . FILTER(isBlank(?o");
            builder.append(i);
            builder.append(") && !isBlank(?z");
            builder.append(i);
            builder.append(")) }");
        }

        builder.append("}");
        Graph result = queryExecutor.executeConstructQuery(builder.toString(), "");
        m.addAll(result);
        return m;
    }

    public static void main(String[] args) {

        QueryExecutorImpl qExec = new QueryExecutorImpl();
        qExec.setEndpoint("http://localhost:8890/sparql");

        CBDQuery q = new SCBDQuery();
        q.setPrefixes(new SparqlPrefixes(new HashMap<String, String>()));
        q.setQueryExecutor(qExec);
        q.init();
        List<String> instances = q.queryInstancesOf("http://swat.cse.lehigh.edu/onto/univ-bench.owl#FullProfessor", 1,
                0);
        Graph m = q.CBDquery(instances.get(0), 2);

    }

}
