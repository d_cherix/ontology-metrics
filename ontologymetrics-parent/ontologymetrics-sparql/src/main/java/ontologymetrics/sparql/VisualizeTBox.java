package ontologymetrics.sparql;

import java.util.List;

import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public class VisualizeTBox extends AbstractPipelineStage {

    
    private static final Logger logger = LoggerFactory.getLogger(VisualizeTBox.class);
    
    private String pathToSchema = null;

    private String classPattern;
    
    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    protected void process() {
//        for(String uriClass: this.listClasses()){
//            this.computeDistinctProperties(uriClass);
//        }
//        
    }

    private void computeDistinctProperties(String uriClass) {
//        ParameterizedSparqlString queryString = new ParameterizedSparqlString();
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub
        
    }
    
    private List<String> listClasses() {
//        Pattern classPat = Pattern.compile(classPattern);
//        Matcher m;
//        Monitor mon = MonitorFactory.start("list classes");
//        InputStream inputStream = FileManager.get().open(pathToSchema);
//        OntModel model = ModelFactory.createOntologyModel();
//        model.read(inputStream, null);
//        LinkedList<String> classes = new LinkedList<String>();
//        for (ExtendedIterator<OntClass> iter = model.listNamedClasses(); iter
//                .hasNext();) {
//            String uri = iter.next().getURI();
//            m = classPat.matcher(uri);
//            if (m.matches()) {
//                classes.add(uri);
//            }
//        }
//        mon.stop();
//        logger.debug("Classes: {}", classes);
//        return classes;
        return null;
    }

    public String getPathToSchema() {
        return pathToSchema;
    }

    @Required
    public void setPathToSchema(String pathToSchema) {
        this.pathToSchema = pathToSchema;
    }

    public String getClassPattern() {
        return classPattern;
    }

    @Required
    public void setClassPattern(String classPattern) {
        this.classPattern = classPattern;
    }

}
