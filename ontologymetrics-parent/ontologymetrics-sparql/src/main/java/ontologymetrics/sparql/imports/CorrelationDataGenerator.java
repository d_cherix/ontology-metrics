package ontologymetrics.sparql.imports;

import java.util.Random;

import lombok.Setter;
import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CorrelationDataGenerator extends AbstractPipelineStage {

    private static final Logger logger = LoggerFactory.getLogger(CorrelationDataGenerator.class);

    @Setter
    private GraphDBDao graphDb;
    @Setter
    private int trustedInstances;
    @Setter
    private int untrustedInstances;
    @Setter
    private int degree;
    private double[] factors = null;
    @Setter
    private boolean writeRdf = false;
    @Setter
    private String filePath;

    @Override
    protected boolean prerequisitesFulfilled() throws StageInitException {
        if (writeRdf) {
            if (filePath == null) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void process() {
//        OntModel m = ModelFactory.createOntologyModel();
//        OntClass ontClass = m.createClass("http://example.org/ontology#Object");
//        ontClass.addSuperClass(OWL.Thing);
//        DatatypeProperty propX = m.createDatatypeProperty("http://example.org/ontology#x");
//        Resource resDouble = m.createResource("http://www.w3.org/2001/XMLSchema#double");
//        propX.addRange(resDouble);
//        DatatypeProperty propY = m.createDatatypeProperty("http://example.org/ontology#y");
//        propY.setRange(resDouble);
//        Random random = new Random();
//        for (int i = 0; i < trustedInstances; i++) {
//            Resource res = m.createIndividual("http://example.org/resource/" + i, ontClass);
//            double x = random.nextDouble() * (random.nextInt(1000) - 1000);
//            res.addLiteral(propX, x);
//            double deviation = 0;
//            while (deviation > 5 || deviation < -5) {
//                deviation = random.nextDouble() * (random.nextInt(200) - 100);
//            }
//            res.addLiteral(propY, this.function(x) + deviation);
//        }
//        for (int i = 0; i < untrustedInstances; i++) {
//            Resource res = m.createIndividual("http://example.org/resource/u" + i, ontClass);
//            double x = random.nextDouble() * random.nextInt(1000);
//            res.addLiteral(propX, x);
//            double deviation = 0;
//            while (deviation < 5 && deviation > -5) {
//                deviation = random.nextDouble() * (random.nextInt(200) - 100);
//            }
//            res.addLiteral(propY, this.function(x) + deviation);
//        }
//        if (writeRdf) {
//            try {
//                m.write(new FileWriter(filePath));
//            } catch (IOException e) {
//                logger.error("Error onw riting model: ", e);
//            }
//        }
//        graphDb.save(m, "http://example.org");
    }

    private double function(double x) {
        if (factors == null) {
            factors = new double[degree + 1];
            Random random = new Random();
            for (int i = 0; i < factors.length; i++) {
                factors[i] = random.nextDouble();
            }
        }
        double y = 0;
        for (int i = 0; i < factors.length; i++) {
            y += factors[i] * (Math.pow(x, i));
        }
        return y;
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

}
