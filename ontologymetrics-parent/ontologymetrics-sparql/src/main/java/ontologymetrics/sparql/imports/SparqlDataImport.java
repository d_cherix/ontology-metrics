package ontologymetrics.sparql.imports;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ontologymetrics.core.data.GraphDBDao;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.core.sparql.QueryExecutor;
import ontologymetrics.core.sparql.SparqlPrefixes;
import ontologymetrics.sparql.CBDQuery;

import org.openrdf.model.Model;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.QueryResultHandlerException;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.query.TupleQueryResultHandler;
import org.openrdf.query.TupleQueryResultHandlerException;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

/**
 * The Class DataImport.
 * This class is for the scdb import into neo4j responsible.
 * The class create an index for the uri of instances.
 * Indexes for DatatypeProperties and ObjectProperties are created too.
 * 
 * @author d.cherix
 */
public class SparqlDataImport extends AbstractPipelineStage {

    /** The Constant LOGGER. */
    private static final Logger logger = LoggerFactory
            .getLogger(SparqlDataImport.class);

    /**
     * The SPARQL prefixes
     */
    @Autowired
    private SparqlPrefixes prefixes;

    /** The query executor. */
    private QueryExecutor queryExecutor = null;
    /**
     * The path to the schema of the ontology to use
     */
    private String pathToSchema = null;

    private String classPattern;

    private GraphDBDao graphDb;
    @Autowired
    private CBDQuery cbdQuery;

    private int offset = 0;
    private int counter = 0;


    private List<String> classes;

    private int depth;

    /**
     * Check if a {@link QueryExecutor} was specified in the configuration
     */
    @Override
    public boolean prerequisitesFulfilled() throws StageInitException {
        if (this.prefixes == null) {
            throw new StageInitException(
                    "No SparqlPrefixes SparqlDataImport component");
        }
        if ((pathToSchema == null || classPattern == null) && classes == null) {
            throw new StageInitException("To use the sparql import you need to set " +
                    "the path to the owl schema and a pattern to filter the classes to " +
                    "select or provide a list of classes to use");
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.core.pipeline.
     * AbstractPipelineStage#process()
     */
    @Override
    protected void process() {
        List<String> classes = this.listClasses();
        this.queryInstances(classes);
    }

    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    private List<String> listClasses() {
        if (this.classes != null) {
            return this.classes;
        }
        Pattern classPat = Pattern.compile(classPattern);
        Matcher m;
        Monitor mon = MonitorFactory.start("list classes");
        Repository repo = new SailRepository( new MemoryStore(new File(pathToSchema)) );
        RepositoryConnection con=null;
        try {
            repo.initialize();
            con=repo.getConnection();
            TupleQuery query = con.prepareTupleQuery(QueryLanguage.SPARQL, "select distinct ?Concept where {[] a ?Concept}");
           TupleQueryResult result = query.evaluate();
           while(result.hasNext()){
               String uri = result.next().getBinding("Concept").getValue().stringValue();
               m=classPat.matcher(uri);
               if(m.matches()){
                   classes.add(uri);
               }
           }
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedQueryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (QueryEvaluationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        mon.stop();
        logger.debug("Classes: {}", classes);
        return classes;
    }

    private void queryInstances(List<String> classes) {
        for (String owlClass : classes) {
            logger.debug("processing instances of {}", owlClass);
            int numberOfInstances = 1000;
            for (int i = offset; i < Integer.MAX_VALUE; i++) {
                List<String> instances = cbdQuery.queryInstancesOf(owlClass, numberOfInstances, i*numberOfInstances);
                if (instances.isEmpty()) {
                    break;
                } else {
                    for (String instance : instances) {
                        try{
                        Model model = cbdQuery.CBDquery(instance, this.depth);
                        graphDb.save(model,instance);
                        } catch(Exception e){
                            logger.error("query error ",e);
                            String label=e.getClass().getName();
                            MonitorFactory.add(label, "Excpetion",1);
                        }
                        
                    }
                }
                logger.debug("{} instances processed",(i+1)*numberOfInstances);
            }
        }
    }

    public void setPathToSchema(String pathToSchema) {
        this.pathToSchema = pathToSchema;
    }

    /**
     * @param queryExecutor
     *            the queryExecutor to set
     */
    public void setQueryExecutor(QueryExecutor queryExecutor) {
        this.queryExecutor = queryExecutor;
    }

    /**
     * @param prefixes
     *            the prefixes to set
     */
    public void setPrefixes(SparqlPrefixes prefixes) {
        this.prefixes = prefixes;
    }

    /**
     * @param classPattern
     *            the classPattern to set
     */
    public void setClassPattern(String classPattern) {
        this.classPattern = classPattern;
    }

    /**
     * @param graphDb
     *            the graphDb to set
     */
    public void setGraphDb(GraphDBDao graphDb) {
        this.graphDb = graphDb;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Required
    public void setDepth(int depth) {
        this.depth = depth;
    }

}
