package ontologymetrics.sparql.imports;

import java.util.List;

import ontologymetrics.core.data.GraphDBImpl;
import ontologymetrics.core.exceptions.StageInitException;
import ontologymetrics.core.pipeline.AbstractPipelineStage;
import ontologymetrics.core.sparql.QueryExecutor;
import ontologymetrics.core.sparql.SparqlPrefixes;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

/**
 * The Class DataImport.
 * This class is for the scdb import into neo4j responsible.
 * The class create an index for the uri of instances.
 * Indexes for DatatypeProperties and ObjectProperties are created too.
 * 
 * @author d.cherix
 */
public class SparqlDataImportModular extends AbstractPipelineStage {

    /** The Constant LOGGER. */
    private static final Logger logger = LoggerFactory
            .getLogger(SparqlDataImportModular.class);

    /**
     * The SPARQL prefixes
     */
    private SparqlPrefixes prefixes;

    /** The query executor. */
    private QueryExecutor queryExecutor = null;
    /**
     * The path to the scheman of the ontology to use
     */
    private String pathToSchema = null;

    private String classPattern;

    private GraphDBImpl graphDb;

    private GraphDatabaseService db;

    private Index<org.neo4j.graphdb.Node> indexNodes;
    private Index<Relationship> datatypeProperties;
    private Index<Relationship> objectProperties;
    private int offset = 0;
    private int counter = 0;

    private Transaction tx;

    private String instancesQuery;

    /**
     * Check if a {@link QueryExecutor} was specified in the configuration
     */
    @Override
    public boolean prerequisitesFulfilled() throws StageInitException {
        if (this.queryExecutor == null) {
            throw new StageInitException(
                    "No query executor in SparqlDataImport component");
        }
        if (this.prefixes == null) {
            throw new StageInitException(
                    "No SparqlPrefixes SparqlDataImport component");
        }
        db = graphDb.getGraphDb();
        indexNodes = db.index().forNodes("uri");
        datatypeProperties = db.index().forRelationships("datatypeProperties");
        objectProperties = db.index().forRelationships("objectProperties");
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.core.pipeline.
     * AbstractPipelineStage#process()
     */
    @Override
    protected void process() {
        this.queryInstances();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.unister.semweb.quality.ontologymetrics.core.pipeline.
     * AbstractPipelineStage#finalise()
     */
    @Override
    protected void finalise() {
        // TODO Auto-generated method stub

    }

    private void queryInstances() {
//        StringBuilder builder = new StringBuilder();
//        builder.append(this.instancesQuery);
//        builder.append(" ORDER BY ASC(?instance)  LIMIT ?limit OFFSET ?offset ");
//
//        ParameterizedSparqlString queryString = new ParameterizedSparqlString(
//                builder.toString(), this.prefixes.listPrefixes());
//
//        ResultSet resultSet;
//        QuerySolution solution;
//
//        int numberOfInstances = 1000;
//        for (int i = offset; i < Integer.MAX_VALUE; i++) {
//            List<String> instances = new LinkedList<String>();
//            queryString.setLiteral("offset", i * numberOfInstances);
//            queryString.setLiteral("limit", numberOfInstances);
//            logger.debug("Query string: {}", queryString.asQuery());
//            Monitor mon = MonitorFactory.start("Instance query");
//            resultSet = this.queryExecutor.executeSelectQuery(
//                    queryString.toString(), "");
//            mon.stop();
//            if (!resultSet.hasNext()) {
//                break;
//            } else {
//                while (resultSet.hasNext()) {
//                    solution = resultSet.next();
//                    if (solution.getResource("instance") != null) {
//                        instances
//                                .add(solution.getResource("instance").getURI());
//                    } else {
//                        this.querySCBD(instances);
//                    }
//                }
//            }
//            this.querySCBD(instances);
//            logger.debug("{} instances: ", instances.size());
//        }

    }

    private void querySCBD(List<String> uris) {
        Monitor mon = MonitorFactory.start("SCDB All");

//        ParameterizedSparqlString queryString = new ParameterizedSparqlString(
//                "CONSTRUCT  { ?instance ?predicate ?object ."
//                        + " ?subject ?pp ?instance } "
//                        + "WHERE { { ?instance ?predicate ?object} "
//                        + "UNION { ?subject ?pp ?instance } } ",
//                this.prefixes.listPrefixes());
//
//        tx = null;
//        tx = db.beginTx();
//        try {
//            for (String uri : uris) {
//                queryString.setIri("instance", uri);
//                Model model = null;
//                int i=1;
//                        Monitor mon2 = MonitorFactory.start("SCDB Query");
//                        try {
//                            model = queryExecutor.executeConstructQuery(
//                                    queryString.asQuery().toString(), "");
//                        } catch (Exception e) {
//                           logger.error("Exception on query {}",queryString.asQuery(),e);
//                           continue;
//                        }
//                        mon2.stop();
//                        i=1;
//                Statement statement;
//                Relationship rel;
//                for (StmtIterator iter = model.listStatements(); iter.hasNext();) {
//                    statement = iter.next();
//                    Node subject = statement.asTriple().getSubject();
//                    String predicate = URIref.decode(statement.asTriple().getPredicate()
//                            .getURI());
//                    Node object = statement.asTriple().getObject();
//                    if (subject.isURI()) {
//                        if (subject.getURI().equals(uri)) {
//                            if (object.isURI()) {
//                                rel = (getNode(uri).createRelationshipTo(
//                                        getNode(object.getURI()),
//                                        DynamicRelationshipType
//                                                .withName(predicate)));
//                                objectProperties.add(rel, "node", getNode(uri));
//                                logger.trace("objectProperties node {}", uri);
//                            } else {
//
//                                org.neo4j.graphdb.Node literalNode = db.createNode();
//                                if (object.getLiteralDatatypeURI() != null) {
//                                    literalNode.setProperty("datatype", object.getLiteralDatatypeURI());
//                                }
//                                if (object.getLiteralLanguage() != null) {
//                                    literalNode.setProperty("language", object.getLiteralLanguage());
//                                }
//                                Object value = "parse error";
//                                try {
//                                    if (object.getLiteralValue() instanceof BaseDatatype.TypedValue) {
//                                        value = ((BaseDatatype.TypedValue) object.getLiteralValue()).lexicalValue;
//                                    } else {
//                                        value = object.getLiteralValue();
//                                        if (!(value instanceof Boolean) &&
//                                                !(value instanceof boolean[]) &&
//                                                !(value instanceof Byte) &&
//                                                !(value instanceof byte[]) &&
//                                                !(value instanceof Short) &&
//                                                !(value instanceof short[]) &&
//                                                !(value instanceof Integer) &&
//                                                !(value instanceof int[]) &&
//                                                !(value instanceof Long) &&
//                                                !(value instanceof long[]) &&
//                                                !(value instanceof Float) &&
//                                                !(value instanceof float[]) &&
//                                                !(value instanceof Double) &&
//                                                !(value instanceof double[]) &&
//                                                !(value instanceof Character) &&
//                                                !(value instanceof char[]) &&
//                                                !(value instanceof String) &&
//                                                !(value instanceof String[])) {
//                                            value = value.toString();
//                                        }
//                                    }
//                                } catch (Exception e) {
//
//                                    logger.warn("Parsing error {}", e.getMessage());
//                                }
//
//                                literalNode.setProperty("value", value);
//
//                                logger.trace("datatypeProperties node {} {} {}", new Object[] { uri, predicate,
//                                        literalNode.getProperty("value") });
//                                rel = (getNode(uri).createRelationshipTo(literalNode,
//                                        DynamicRelationshipType.withName(predicate)));
//                                datatypeProperties.add(rel, "node", getNode(uri));
//
//                            }
//                        } else {
//                            if (object.isURI()) {
//                                getNode(object.getURI()).createRelationshipTo(
//                                        getNode(uri),
//                                        DynamicRelationshipType
//                                                .withName(predicate));
//                            }
//                        }
//                    } else {
//                        logger.warn("literal or blanknode as subject: {}",
//                                statement);
//                    }
//
//                    counter++;
//                    tx.success();
//                }
//                tx.success();
//            }
//        } finally {
//            tx.success();
//            tx.finish();
//        }
//        mon.stop();
//        logger.debug("{} instances saved", counter);
    }

//    private org.neo4j.graphdb.Node getNode(String uri) {
//        org.neo4j.graphdb.Node node;
//        uri = URIref.decode(uri);
//        if (indexNodes.get("uri", uri).hasNext()) {
//            node = indexNodes.get("uri", uri).next();
//        } else {
//            node = db.createNode();
//            node.setProperty("uri", uri);
//            logger.trace("index Nodes uri {}", uri);
//            indexNodes.add(node, "uri", uri);
//            tx.success();
//            tx.finish();
//            tx = db.beginTx();
//        }
//        return node;
//    }

    public void setPathToSchema(String pathToSchema) {
        this.pathToSchema = pathToSchema;
    }

    /**
     * @param queryExecutor
     *            the queryExecutor to set
     */
    public void setQueryExecutor(QueryExecutor queryExecutor) {
        this.queryExecutor = queryExecutor;
    }

    /**
     * @param prefixes
     *            the prefixes to set
     */
    public void setPrefixes(SparqlPrefixes prefixes) {
        this.prefixes = prefixes;
    }

    /**
     * @param classPattern
     *            the classPattern to set
     */
    public void setClassPattern(String classPattern) {
        this.classPattern = classPattern;
    }

    /**
     * @param graphDb
     *            the graphDb to set
     */
    public void setGraphDb(GraphDBImpl graphDb) {
        this.graphDb = graphDb;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Required
    public void setInstancesQuery(String instancesQuery) {
        this.instancesQuery = instancesQuery;
    }

}
