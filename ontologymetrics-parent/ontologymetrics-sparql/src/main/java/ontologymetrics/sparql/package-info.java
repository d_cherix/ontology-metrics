/**
 * 
 * This package contains the sparql execution classes.
 * @author d.cherix
 *
 */
package ontologymetrics.sparql;